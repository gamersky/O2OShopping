//
//  SureOrderCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/21.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： SureOrderCell
 Created_Date： 20151107
 Created_People：
 Function_description： 确认订单cell
 ***************************************/

#import <UIKit/UIKit.h>

@interface SureOrderCell : UITableViewCell
- (void)updateData:(NSDictionary*)dataDic hoemMakeType:(NSString *)homeMakeType;
- (void)updateGoodsData:(NSDictionary*)dataDic;
@end
