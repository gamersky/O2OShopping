//
//  SureOrderHeaderView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： SureOrderHeaderView
 Created_Date： 20151107
 Created_People：
 Function_description： 确认订单头部视图
 ***************************************/

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, SureOrderHeaderType) {
    kproductSureOrderHeader = 0,//商品订单确认头部
    kserviceSureorderHeader //服务订单确认头部
};
@protocol SureOrderHeaderViewDelegate<NSObject>
- (void)selectXiaoqu;
- (void)selectDetailAddress;
- (void)modifiedAddress;
- (void)addMarkInfo;
- (void)selectServiceTime;
@end
@interface SureOrderHeaderView : UIView
@property(nonatomic,unsafe_unretained)id<SureOrderHeaderViewDelegate>delegate;
@property(nonatomic, strong)NSString *payType;//支付方式
@property(nonatomic, strong)NSString *sendProductType;//收货方式
@property(nonatomic, strong)NSString *sexType;//性别类型
@property(nonatomic, strong)UITextField *userNameTextField;//用户名输入框
@property(nonatomic, strong)UITextField *userTelePhoneTextField;//用户联系电话输入框
@property(nonatomic, strong)UITextField *userAddressTextField;//用户详细地址
@property(nonatomic, strong)UIButton *detailAddressBtn;//选择楼号，单元，门牌号

- (void)resetMarkContent:(NSString*)markContent;//设置备注信息
- (void)resetServiceDate:(NSString*)serviceDate;//设置服务日期
- (void)setDetailAddressTitle:(NSString*)title;//设置详细地址buton标题
- (instancetype)initWithFrame:(CGRect)frame hadOrdersAddressStatus:(BOOL)isHadOrdersAddress withSureOrderType:(SureOrderHeaderType)orderType;
- (instancetype)initWithFrame:(CGRect)frame hadOrdersAddressStatus:(BOOL)isHadOrdersAddress;
- (void)updateViewWithReceivedAddressStatus:(BOOL)isHadReceivedAddress;
- (void)updataWithName:(NSString*)name telNumber:(NSString*)telNumber address:(NSString*)address;
@end
