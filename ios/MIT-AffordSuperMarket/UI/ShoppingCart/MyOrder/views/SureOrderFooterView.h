//
//  SureOrderFooterView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/21.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： SureOrderFooterView
 Created_Date： 20151107
 Created_People：
 Function_description： 确认订单底部视图
 ***************************************/

#import <UIKit/UIKit.h>
#import "SureOrderFooterViewModel.h"
@interface SureOrderFooterView : UIView
- (instancetype)initWithFrame:(CGRect)frame isNewUser:(BOOL)isNewUser isHadYouHui:(BOOL)isHadYouHui isHideYouHuiShuoMing:(BOOL)isHideYouHuiShuoMing;
- (void)updateDataWithDataModel:(SureOrderFooterViewModel*)model;
@end
