//
//  MyGoodsOrderController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/3.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsOrderController.h"
#import "AppDelegate.h"
#import "RDVTabBarController.h"
#import "MyGoodsOrderDetailController.h"
#import "WeiXinPayController.h"
#import "MyGoodsOderTrackingController.h"
#import "MyGoodsTradeFinishedController.h"
#import "Evaluate_Store_Controller.h"
#import "Home_Root_Controller.h"
//view
#import "MyGoodsOrderCell.h"
#import "EmptyPageView.h"
//untils
#import "UIColor+Hex.h"
#import "MenuScrollView.h"
//untils
#import "Global.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
//vendor
#import "MTA.h"
#import "MJRefresh.h"
@interface MyGoodsOrderController()<EmptyPageViewDelegate>
{
    MenuScrollView *menu;
}
//空视图
@property (nonatomic,strong)EmptyPageView *emptyView;
//存储table视图
@property (nonatomic, strong)NSMutableArray *tableArr;
//数据源
@property (nonatomic, strong)NSMutableArray *dataSource;
@property (nonatomic, strong)NSMutableArray *wholeListDataSource;//全部订单数据
@property (nonatomic, strong)NSMutableArray *noPayListDataSource;//未付款数据
@property (nonatomic, strong)NSMutableArray *noReceivingListDataSource;//待收货数据
@property (nonatomic, strong)NSMutableArray *noEvaluateListDataSource;//待评价数据
@property (nonatomic, strong)NSMutableArray *orderFinishListDataSource;//交易完成数据
@property (nonatomic,assign)NSInteger tag;
//当前页
@property (nonatomic)NSInteger currentPage;
@property (nonatomic)NSInteger wholeListCurrentPage;
@property (nonatomic)NSInteger noPayListCurrentPage;
@property (nonatomic)NSInteger noReceivingListCurrentPage;
@property (nonatomic)NSInteger noEvaluateListCurrentPage;
@property (nonatomic)NSInteger orderFinishListCurrentPage;
//总条数
@property (nonatomic)NSInteger totalCount;
@property (nonatomic)NSInteger wholeListTotalCount;
@property (nonatomic)NSInteger noPayListTotalCount;
@property (nonatomic)NSInteger noReceivingListTotalCount;
@property (nonatomic)NSInteger noEvaluateListTotalCount;
@property (nonatomic)NSInteger orderFinishListTotalCount;
@end
@implementation MyGoodsOrderController
@synthesize dataSource = _dataSource;
@synthesize currentPage = _currentPage;
@synthesize totalCount = _totalCount;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单列表";
    [self addBackNavItem];
    self.currentPage = 1;
    _tableArr = [[NSMutableArray alloc]init];
    _dataSource =[[NSMutableArray alloc]init];
    [self setupSubView];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    if ([self.PopType isEqualToString:@"evaluateType"]) {
        menu.selectIndex = self.selectedIndex;
    }
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
            [self loadTeHuiDataWithType:[self tehuiIdWithMyGoodOrderType:self.myGoodOrderType] requestPage:self.currentPage requestCount:kRequestLimit];
    }
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"商品订单"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"商品订单"];
    self.PopType=@"";
}
#pragma mark -- request Data
-(void)loadTeHuiDataWithType:(NSString*)type requestPage:(NSInteger)requestPage requestCount:(NSInteger)requestCount
{
    
    //  请求参数说明：
    //  CATEGORYCODE  类别代码    1：蔬菜水果，2：酒水饮料，3：生活百货，4：粮油调料，5：营养早餐
    //  TYPE          优惠类型  1：普通，2：推荐，3：限量，4：一元购
    //  PAGENUM		页码
    //  PAGESIZE		数量
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    UserInfoModel *userInfoMode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    XiaoQuAndStoreInfoModel *xiaoQuAndStoreModel = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:userInfoMode.token forKey:@"TOKEN"];
    [parameter setObject:serviceVersionNumber forKey:@"APIVERSION"];
    [parameter setObject:xiaoQuAndStoreModel.storeID forKey:@"STOREID"];
    [parameter setObject:[NSNumber numberWithInteger:requestPage] forKey:@"PAGENUM"];
    [parameter setObject:[NSNumber numberWithInteger:requestCount] forKey:@"PAGESIZE"];
    [parameter setObject:type forKey:@"FLAG"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"order/list" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            
            if ([state isEqualToString:@"0"]) {//请求成功
                _totalCount = [[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"TOTALROW"]] integerValue];
                if (weakSelf.currentPage == 1) {//请求第一页清空数据
                    [weakSelf.dataSource removeAllObjects];
                }
                NSMutableArray *dataArr = [[NSMutableArray alloc]init];
                [dataArr addObjectsFromArray:[weakSelf.dataSource copy]];
                [dataArr addObjectsFromArray:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"LIST"]];
                
                weakSelf.dataSource = dataArr;
                UITableView *tableView = (UITableView*)[weakSelf.tableArr objectAtIndex:weakSelf.myGoodOrderType - 100];
                [weakSelf noDataViewIsShowWithTableView:tableView];
                [tableView reloadData];
                
            } else {
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }

        }
        [weakSelf endRefresh];
    } failure:^(bool isFailure) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        [weakSelf endRefresh];
    }];
    
    
}
//删除订单

-(void)deleteOrderWithTag:(NSInteger )tag
{
    /*
     SIGNATURE			设备识别码
     TOKEN			    用户签名
     ORDERNUM			订单号
     **/
    NSDictionary *dataDic = [self.dataSource objectAtIndex:tag];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    NSMutableDictionary *paramerters=[[NSMutableDictionary alloc]init];
    [paramerters setObject:mode.token forKey:@"TOKEN"];
    [paramerters setObject:[NSString stringTransformObject:[dataDic objectForKey:@"ORDERNUM"]] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:paramerters customPOST:@"order/delete" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dataDic = (NSDictionary*)responseObject;
        NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
//        NSLog(@"%@",[dataDic objectForKey:@"MSG"]);
        if ([state isEqualToString:@"0"]) {//请求成功
//            [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"删除成功" inView:weakSelf.view];
            [weakSelf loadTeHuiDataWithType:[weakSelf tehuiIdWithMyGoodOrderType:weakSelf.myGoodOrderType] requestPage:weakSelf.currentPage requestCount:kRequestLimit];
        }
        
    } failure:^(bool isFailure) {
        
    }];
    
}
//请求催单数据
- (void)loadCuiOrderDataWithOrderTag:(NSInteger )tag {
    
    NSDictionary *dataDic = [self.dataSource objectAtIndex:tag];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:mode.token forKey:@"TOKEN"];
    [parameter setObject:[NSString stringTransformObject:[dataDic objectForKey:@"ORDERNUM"]] forKey:@"ORDERNUM"];;
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"order/urge" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                msg = @"催单成功";
//                _otherCuiDanTime = [NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"DATETIME"]];
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            } else {
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
            
        }
    } failure:^(bool isFailure) {
        
        
    }];
}
//请求签收
- (void)loadOrderQianShouDataTag:(NSInteger )tag  {
    //请求参数说明
    //SIGNATURE			设备识别码
    //TOKEN			用户签名
    //ORDERNUM			订单号
    NSDictionary *dataDic = [self.dataSource objectAtIndex:tag];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringISNull:mode.token] forKey:@"TOKEN"];
    [parameter setObject:[NSString stringTransformObject:[dataDic objectForKey:@"ORDERNUM"]] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"order/receive" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *data = (NSDictionary*)responseObject;
            NSString *msg = [data objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[data objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
//                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"确认成功" inView:weakSelf.view];
                MyGoodsTradeFinishedController *tradeFinishedVC = [[MyGoodsTradeFinishedController alloc]init];
                tradeFinishedVC.goodsOrderInfoDic = dataDic ;
                [self.navigationController pushViewController:tradeFinishedVC animated:YES];
                
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
    } failure:^(bool isFailure) {
        
    }];
}


// 删除订单 // 催单 //确认收货

-(void)selectedSecondItemWithTag:(NSInteger)tag title:(NSString *)title
{
    _tag =tag;
    if ([title isEqualToString:@"删除订单"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"是否删除订单?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=1;
        [alert show];
        
    }else if ([title isEqualToString:@"催单"]) {//催单
        [self loadCuiOrderDataWithOrderTag:tag];
        
    }else if ([title isEqualToString:@"确认收货"]||[title isEqualToString:@"确认取货"]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:[NSString stringWithFormat:@"是否%@？",title] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=2;
        _tag =tag;
        [alert show];
        
    }
}

//去支付 或者 订单跟踪 // 去评价
-(void)selectedFirstItemWithTag:(NSInteger)tag title:(NSString *)title
{
    NSDictionary *dataDic = [self.dataSource objectAtIndex:tag];
    if ([title isEqualToString:@"去支付"]) {
        WeiXinPayController *weixinpay=[[WeiXinPayController alloc]init];
        weixinpay.orderNum=[NSString stringTransformObject:[dataDic objectForKey:@"ORDERNUM"]];
        weixinpay.totalPrice=[NSString stringWithFormat:@"%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"AMOUNT"]] floatValue]];
        weixinpay.requestDataType =@"1";
        weixinpay.goodIDs=@"";
        [self.navigationController pushViewController:weixinpay animated:YES];
    }else if([title isEqualToString:@"去评价"])
    {
        Evaluate_Store_Controller *evalVc =[[Evaluate_Store_Controller alloc]init];
        evalVc.orderNum =[NSString stringTransformObject:[dataDic objectForKey:@"ORDERNUM"]];
        [self.navigationController pushViewController:evalVc animated:YES];
    }else if([title isEqualToString:@"订单跟踪"]) {
        MyGoodsOderTrackingController *orderTrackingVC = [[MyGoodsOderTrackingController alloc]init];
        NSDictionary *dataDic = [self.dataSource objectAtIndex:tag];
        orderTrackingVC.orderNumber = [NSString stringTransformObject:[dataDic objectForKey:@"ORDERNUM"]];
        orderTrackingVC.payType = [NSString transitionPayType:[NSString stringTransformObject:[dataDic objectForKey:@"PAYTYPE"]]];
        orderTrackingVC.peiSongType = [NSString transitionPeiSongType:[NSString stringTransformObject:[dataDic objectForKey:@"TAKETYPE"]]];
        [self.navigationController pushViewController:orderTrackingVC animated:YES];
    }
    
}
#pragma mark--UIAlertViewdelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1)//删除订单
    {
        if (buttonIndex == 1){
            [self deleteOrderWithTag:_tag];
        }
    }else if(alertView.tag ==2){//确认签收
        if (buttonIndex == 1){
            [self loadOrderQianShouDataTag:_tag];
        }
    }
    
    
}
#pragma mark -- //立即逛逛 跳转
-(void)buttonClick
{
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(action) userInfo:nil repeats:NO];
}
-(void)action
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    Home_Root_Controller *homeVC = [[[appDelegate.tabBarController.viewControllers firstObject] childViewControllers] firstObject];
    [homeVC.collectionView setContentOffset:CGPointMake(0, 0) animated:YES];
    [appDelegate selectedControllerAtTabBarIndex:0];
        
}
#pragma mark -- UITableViewDataSource
////黑条效果
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//        return HightScalar(12);
//    
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *views = [[UIView alloc]init];
//    views.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
//    return views;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.dataSource.count !=0)
    {
        return [self.dataSource count];
    }else {
        return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"TableViewCellIdentifier";
    
     MyGoodsOrderCell*cell = (MyGoodsOrderCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[MyGoodsOrderCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        cell.backgroundColor = [UIColor whiteColor];
        cell.delegate = (id<MyGoodsOrderCellDelegate>)self;
       
        
    }
    cell.goPay.tag=indexPath.row;
    cell.deleteOrder.tag=indexPath.row;
    if (indexPath.row !=0) {
        [ cell.views.layer setBorderWidth:0.5];
        [ cell.views.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];

    }
    if(self.dataSource.count !=0){
      [cell updateViewwithData:[self.dataSource objectAtIndex:indexPath.row]];
    }
   
    return cell;
}
#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return HightScalar(228);
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MyGoodsOrderDetailController *detailVc=[[MyGoodsOrderDetailController alloc]init];
  NSDictionary *dic =[self.dataSource objectAtIndex:indexPath.row];
    detailVc.orderNum =[NSString stringTransformObject:[dic objectForKey:@"ORDERNUM"]];
    [self.navigationController pushViewController:detailVc animated:YES];
}
#pragma mark -- MenuScrollViewDelegate
- (void)selectedMenuItemAtIndex:(NSInteger)index {
    
    if (index == 0) {
        self.myGoodOrderType = kGoodWholeType;
    } else if (index == 1) {
        self.myGoodOrderType = kGoodNoPayMoneyType;
    } else if (index == 2){
        self.myGoodOrderType = kGoodNoReceivingType;
    }else if (index == 3){
        self.myGoodOrderType = kGoodNoEvaluateType;
    }
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        self.currentPage = 1;
        [self loadTeHuiDataWithType:[self tehuiIdWithMyGoodOrderType:self.myGoodOrderType] requestPage:self.currentPage requestCount:kGoodWholeType];
    }
    
}

#pragma mark - 上拉/下拉  更新数据
-(void)headerRereshing {
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        self.currentPage = 1;
        [self loadTeHuiDataWithType:[self tehuiIdWithMyGoodOrderType:self.myGoodOrderType] requestPage:self.currentPage requestCount:kRequestLimit];
    }else{
        [self endRefresh];
    }
    
}

-(void)footerRereshing
{
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        if (self.currentPage*kRequestLimit < self.totalCount) {
            self.currentPage++;
            [self loadTeHuiDataWithType:[self tehuiIdWithMyGoodOrderType:self.myGoodOrderType] requestPage:self.currentPage requestCount:kRequestLimit];
        } else {
             UITableView *tableView = (UITableView*)[self.tableArr objectAtIndex:self.myGoodOrderType - 100];
              [tableView setFooterRefreshingText:@"已加载完全部订单~"];
            [self endRefresh];
        }
    }else{
        [self endRefresh];
    }
}
- (void)endRefresh
{
    UITableView *tableView = (UITableView*)[self.tableArr objectAtIndex:self.myGoodOrderType - 100];
    [tableView headerEndRefreshing];
    [tableView footerEndRefreshing];
}

#pragma mark -- button Action
- (void)backBtnAction:(id)sender {
     AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if (appDelegate.tabBarController.selectedIndex == 3) {//从购物车调到订单页面
        [self.navigationController setViewControllers:@[[self.navigationController.childViewControllers firstObject]]];
        [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
         [appDelegate selectedControllerAtTabBarIndex:4];
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
   
    
   
   
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
#pragma mark -- setter and getter method
- (void)setMyGoodOrderType:(MyGoodOrderType)myGoodOrderType {
    
    _myGoodOrderType = myGoodOrderType;
}
- (void)setDataSource:(NSMutableArray *)dataSource {
    switch (self.myGoodOrderType) {
        case kGoodWholeType:
            _wholeListDataSource = dataSource;
            break;
        case kGoodNoPayMoneyType:
            _noPayListDataSource = dataSource;
            break;
        case kGoodNoReceivingType:
            _noReceivingListDataSource = dataSource;
            break;
        case kGoodNoEvaluateType:
            _noEvaluateListDataSource = dataSource;
            break;
        default:
            break;
    }
}

- (void)setCurrentPage:(NSInteger)currentPage {
    
    switch (self.myGoodOrderType) {
        case kGoodWholeType:
            _wholeListCurrentPage = currentPage;
            break;
        case kGoodNoPayMoneyType:
            _noPayListCurrentPage = currentPage;
            break;
        case kGoodNoReceivingType:
            _noReceivingListCurrentPage =currentPage;
            break;
        case kGoodNoEvaluateType:
            _noEvaluateListCurrentPage =currentPage;
            break;
        default:
            break;
       
    }
    
}

- (void)setTotalCount:(NSInteger)totalCount {
    switch (self.myGoodOrderType) {
        case kGoodWholeType:
            _wholeListTotalCount = totalCount;
            break;
        case kGoodNoPayMoneyType:
            _noPayListTotalCount = totalCount;
            break;
        case kGoodNoReceivingType:
            _noReceivingListTotalCount =totalCount;
            break;
        case kGoodNoEvaluateType:
            _noEvaluateListTotalCount =totalCount;
            break;
        default:
            break;
            
    }
    
}

- (NSMutableArray*)dataSource {
    switch (self.myGoodOrderType) {
        case kGoodWholeType:
            return self.wholeListDataSource;
            break;
        case kGoodNoPayMoneyType:
            return self.noPayListDataSource;
            break;
        case kGoodNoReceivingType:
            return self.noReceivingListDataSource;
            break;
        case kGoodNoEvaluateType:
            return self.noEvaluateListDataSource;
            break;
        default:
            break;
    }
}

- (NSInteger)currentPage {
    switch (self.myGoodOrderType) {
        case kGoodWholeType:
            return self.wholeListCurrentPage;
            break;
        case kGoodNoPayMoneyType:
            return self.noPayListCurrentPage;
            break;
        case kGoodNoReceivingType:
            return self.noReceivingListCurrentPage;
            break;
        case kGoodNoEvaluateType:
            return self.noEvaluateListCurrentPage;
            break;
        default:
            break;
    }
}

- (NSInteger)totalCount {
    switch (self.myGoodOrderType) {
        case kGoodWholeType:
            return self.wholeListTotalCount;
            break;
        case kGoodNoPayMoneyType:
            return self.noPayListTotalCount;
            break;
        case kGoodNoReceivingType:
            return self.noReceivingListTotalCount;
            break;
        case kGoodNoEvaluateType:
            return self.noEvaluateListTotalCount;
            break;
        default:
            break;
    }}
#pragma mark -- 根据特惠类型标题返回特惠类型ID
- (NSString*)tehuiIdWithMyGoodOrderType:(MyGoodOrderType)myGoodOrderType {
    if (myGoodOrderType == kGoodWholeType) {
        return @"1";
    }else if (myGoodOrderType == kGoodNoPayMoneyType) {
        return @"2";
    } else if (myGoodOrderType == kGoodNoReceivingType) {
        return @"3";
    }else if (myGoodOrderType == kGoodNoEvaluateType) {
        return @"4";
    }
    return @"";
}
#pragma mark -- setupSubView
- (void)setupSubView {
    NSArray *titles = @[@"全部",@"待付款",@"待收货",@"待评价"];
    //初始化scroll容器
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0
                                                                              , 44
                                                                              , self.view.frame.size.width
                                                                              , self.view.bounds.size.height - 64 -44 )];
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width * titles.count
                                        , self.view.bounds.size.height - 64 -44 );
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.bounces = NO;
    scrollView.tag = 1000;
    scrollView.pagingEnabled = YES;
    [self.view addSubview:scrollView];
    //初始化子视图
    for (NSInteger i = 0; i < [titles count]; i++) {
        CGRect frame = CGRectMake(i*CGRectGetWidth(scrollView.bounds)
                                  , 0, CGRectGetWidth(self.view.bounds)
                                  , CGRectGetHeight(scrollView.bounds));
        UITableView *tableView = [[UITableView alloc]initWithFrame:frame style:UITableViewStylePlain];
        tableView.delegate = (id<UITableViewDelegate>)self;
        tableView.dataSource = (id<UITableViewDataSource>)self;
        //添加下拉刷新
        [tableView addHeaderWithTarget:self action:@selector(headerRereshing)];
        //添加上拉加载更多
        [tableView addFooterWithTarget:self action:@selector(footerRereshing)];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.backgroundColor =[UIColor colorWithHexString:@"#efefef"];
        tableView.tag = 100 + i;
        [scrollView addSubview:tableView];
        UIView *footView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 1)];
        tableView.tableFooterView=footView;
        [self.tableArr addObject:tableView];
    }
    //初始化menuScrollView
    CGRect menuScrollViewFrame = CGRectMake(0, 0, self.view.bounds.size.width, 44);
    menu = [[MenuScrollView alloc]initWithFrame:menuScrollViewFrame titles:titles dataScrollView:scrollView];
    menu.backgroundColor = [UIColor whiteColor];
    menu.selectIndex = self.selectedIndex;
    menu.showBottomLine = YES;
    menu.showVerticalLine = NO;
    menu.menuScrollDelegate = (id<MenuScrollViewDelegate>)self;
    menu.bottomLineColor =[UIColor colorWithHexString:@"#db3b34"];
    menu.tabBackgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    menu.tabSelectedBackgroundColor = [UIColor colorWithHexString:@"#fffff"];
    menu.tabSelectedTitleColor  = [UIColor colorWithHexString:@"#db3b34"];
    menu.tabTitleColor = [UIColor colorWithHexString:@"#555555"];
    [menu.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    [menu.layer setBorderWidth:0.7];
    [self.view addSubview:menu];
    
    //数据为空时显示的View
    _emptyView =[[EmptyPageView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, scrollView.bounds.size.height) EmptyPageViewType:kGoodsOrderRootView];
    _emptyView.backgroundColor=[UIColor whiteColor];
    //_emptyView.hidden=YES;
    _emptyView.delegate =self;
   
}

//空白页面是否展示
- (void)noDataViewIsShowWithTableView:(UITableView *)tableView{
    self.emptyView.frame = tableView.frame;
    UIScrollView *rootScrollView = (UIScrollView*)[self.view viewWithTag:1000];
    if (self.dataSource.count==0)
    {
//        self.emptyView.hidden=NO;
        [rootScrollView insertSubview:self.emptyView aboveSubview:tableView];
        
    }else{
        [rootScrollView insertSubview:tableView aboveSubview:self.emptyView];
//        self.emptyView.hidden=YES;
    
    }
}

@end
