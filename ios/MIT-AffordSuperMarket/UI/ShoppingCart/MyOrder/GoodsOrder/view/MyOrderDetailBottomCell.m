//
//  MyOrderDetailBottomCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/6.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
#define KleftSpace 15
#define KcellHight  HightScalar(110)
#import "MyOrderDetailBottomCell.h"
#import "UIColor+Hex.h"
#import "Global.h"
#import "NSString+TextSize.h"
#import "NSString+Conversion.h"
@interface MyOrderDetailBottomCell()
//商品总计
@property (nonatomic,strong)UILabel *goodsTotal;
//商品总计金额
@property (nonatomic,strong)UILabel *goodsTotalPrice;
//实惠币
@property (nonatomic,strong)UILabel *shiHuiBi;
//实惠币金额
@property (nonatomic,strong)UILabel *shiHuiBiSum;
//实付款
@property (nonatomic,strong)UILabel *realPayment;
//实付款金额
@property (nonatomic,strong)UILabel *realPaymentNum;
@end
@implementation MyOrderDetailBottomCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSuberView];
    }
    return self;
}
#pragma mark---初始化视图
-(void)setUpSuberView
{
    //实付款
    _goodsTotal = [self setLabelWithTitle:@"" fontSize:FontSize(17) textColor:@"#555555"];
    _goodsTotal.frame = CGRectZero;
    _goodsTotal.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_goodsTotal];
    //实付款金额
    _goodsTotalPrice = [self setLabelWithTitle:@"" fontSize:FontSize(16) textColor:@"#555555"];
    _goodsTotalPrice.frame = CGRectZero;
    _goodsTotalPrice.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_goodsTotalPrice];
    //实惠币
    _shiHuiBi = [self setLabelWithTitle:@"" fontSize:FontSize(17) textColor:@"#555555"];
    _shiHuiBi.frame = CGRectZero;
    _shiHuiBi.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_shiHuiBi];
    //实惠币金额
    _shiHuiBiSum = [self setLabelWithTitle:@"" fontSize:FontSize(16) textColor:@"#555555"];
    _shiHuiBiSum.frame = CGRectZero;
    _shiHuiBiSum.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_shiHuiBiSum];
    //实付款
    _realPayment = [self setLabelWithTitle:@"" fontSize:FontSize(17) textColor:@"#555555"];
    _realPayment.frame = CGRectZero;
    _realPayment.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_realPayment];
    //实付款金额
    _realPaymentNum = [self setLabelWithTitle:@"" fontSize:FontSize(17) textColor:@"#555555"];
    _realPaymentNum.frame = CGRectZero;
    _realPaymentNum.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_realPaymentNum];
    
}
//requestDataType  商品订单 是1 服务订单是 0
-(void)upSubViewWithData:(NSMutableDictionary *)dictionary
{
    //总计
    NSString *goodsTotalText =@"商品总计";
    CGSize goodsTotalSize = [goodsTotalText sizeWithFont:[UIFont systemFontOfSize:FontSize(17)]
                                      maxSize:CGSizeMake(VIEW_WIDTH/2, HightScalar(30))];
    _goodsTotal.frame = CGRectMake(KleftSpace, HightScalar(20), goodsTotalSize.width , goodsTotalSize.height);
    _goodsTotal.text= goodsTotalText;
    //总计金额
    NSString *goodsTotalPriceText =[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dictionary objectForKey:@"AMOUNT"]]floatValue]];
    CGSize goodsTotalPriceSize = [goodsTotalPriceText sizeWithFont:[UIFont systemFontOfSize:FontSize(16)]
                                                   maxSize:CGSizeMake(VIEW_WIDTH/2, HightScalar(30))];
    _goodsTotalPrice.frame = CGRectMake(VIEW_WIDTH-KleftSpace-goodsTotalPriceSize.width, CGRectGetMinY(self.goodsTotal.frame), goodsTotalPriceSize.width , CGRectGetHeight(self.goodsTotal.bounds));
    _goodsTotalPrice.text =goodsTotalPriceText;
    //实惠币
    NSString *shiHuiBiText =@"- 实惠币";
       CGSize shiHuiBiSize = [shiHuiBiText sizeWithFont:[UIFont systemFontOfSize:FontSize(17)]
                                    maxSize:CGSizeMake(VIEW_WIDTH/2, HightScalar(30))];
    _shiHuiBi.frame = CGRectMake(KleftSpace, CGRectGetMaxY(self.goodsTotal.frame)+HightScalar(13), shiHuiBiSize.width , shiHuiBiSize.height);
    _shiHuiBi.text =shiHuiBiText;

   //实惠币金额
    CGFloat shihuiBiMoney = [[NSString stringTransformObject:[dictionary objectForKey:@"REDEEMMONEY"]]floatValue];    NSString *shiHuiBiSumText =[NSString stringWithFormat:@"￥%.2f",shihuiBiMoney];
    CGSize shiHuiBiSumSize = [shiHuiBiSumText sizeWithFont:[UIFont systemFontOfSize:FontSize(16)]
                                                   maxSize:CGSizeMake(VIEW_WIDTH/2, HightScalar(30))];
    _shiHuiBiSum.frame = CGRectMake(VIEW_WIDTH-KleftSpace-shiHuiBiSumSize.width, CGRectGetMinY(self.shiHuiBi.frame), shiHuiBiSumSize.width , CGRectGetHeight(self.shiHuiBi.bounds));
    _shiHuiBiSum.text =shiHuiBiSumText;
    
   //实付款
    NSString *realPaymentText =@"实付款";
    CGSize realPaymentSize = [realPaymentText sizeWithFont:[UIFont systemFontOfSize:FontSize(17)]
                                                   maxSize:CGSizeMake(VIEW_WIDTH/2, HightScalar(30))];
    _realPayment.frame = CGRectMake(KleftSpace, CGRectGetMaxY(self.shiHuiBi.frame)+HightScalar(13), realPaymentSize.width , realPaymentSize.height);
    _realPayment.text =realPaymentText;
    //实付款金额
    NSString  *realPaymentNumText =[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dictionary objectForKey:@"REALPAY"]]floatValue]];
    CGSize realPaymentNumSize = [realPaymentNumText sizeWithFont:[UIFont systemFontOfSize:FontSize(17)]
                                                   maxSize:CGSizeMake(VIEW_WIDTH/2, HightScalar(30))];
    _realPaymentNum.frame = CGRectMake(VIEW_WIDTH-KleftSpace-realPaymentNumSize.width, CGRectGetMinY(self.realPayment.frame), realPaymentNumSize.width , CGRectGetHeight(self.realPayment.bounds));
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:realPaymentNumText];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#c52720"] range:NSMakeRange(0,realPaymentNumText.length)];
    _realPaymentNum.attributedText=str;
}

- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHexString:textColor];
    label.minimumScaleFactor = 0.8;
    label.adjustsFontSizeToFitWidth = YES;
    return label;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
