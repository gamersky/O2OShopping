//
//  MyGoodsDetailAddressCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/14.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#define kLeftSpace 15
#define kRightSpace 15

#import "MyGoodsDetailAddressCell.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+Conversion.h"
@interface MyGoodsDetailAddressCell()
@property (nonatomic,strong)UILabel *userName;
@property (nonatomic,strong)UILabel *userPhone;
@property (nonatomic,strong)UILabel *userAddress;
@end
@implementation MyGoodsDetailAddressCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //初始化视图
        [self setSubView];
    }
    return self;
}
-(void)setSubViewDateWithDictionary:(NSDictionary *)dataDic
{
    self.userName.text =[NSString stringTransformObject:[dataDic objectForKey:@"LINKNAME"]];
    self.userName.frame =CGRectMake(kLeftSpace+15+10,0, [self setLabelWidthWithNSString:self.userName.text Font:FontSize(17.5)].width, HightScalar(53));
   

    self.userPhone.text =[NSString stringTransformObject:[dataDic objectForKey:@"TEL"]];
    self.userPhone.frame =CGRectMake(CGRectGetMaxX(self.userName.frame)+50, CGRectGetMinY(self.userName.frame), [self setLabelWidthWithNSString:self.userPhone.text Font:FontSize(17.5)].width, CGRectGetHeight(self.userName.bounds));

    UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(self.userName.frame)+CGRectGetHeight(self.userName.bounds)/2-9-10, 15, 18)];
    imageView.image =[UIImage imageNamed:@"orderdetail_adress_img_map"];
    [self.contentView addSubview:imageView];
    self.userAddress.text =[NSString stringTransformObject:[dataDic objectForKey:@"ADDRESS"]];
    self.userAddress.frame =CGRectMake(CGRectGetMinX(self.userName.frame), CGRectGetMaxY(self.userName.frame)-10, VIEW_WIDTH-15-15-15, CGRectGetHeight(self.userName.bounds));
   

}
#pragma mark --初始化控件
-(void)setSubView
{
    self.userName =[[UILabel alloc]initWithFrame:CGRectZero];
    self.userName.font =[UIFont systemFontOfSize:FontSize(17.5)];
    self.userName.textColor =[UIColor colorWithHexString:@"#000000"];
    [self.contentView addSubview:self.userName];
    
    
    self.userPhone =[[UILabel alloc]initWithFrame:CGRectZero];
    self.userPhone.font =[UIFont systemFontOfSize:FontSize(17.5)];
    self.userPhone.textColor =[UIColor colorWithHexString:@"#000000"];
    [self.contentView addSubview:self.userPhone];
    
    
    self.userAddress =[[UILabel alloc]initWithFrame:CGRectZero];
    self.userAddress.font =[UIFont systemFontOfSize:FontSize(16)];
    self.userAddress.numberOfLines =0;
    self.userAddress.textColor =[UIColor colorWithHexString:@"#555555"];
    [self.contentView addSubview:self.userAddress];
}
-(CGSize)setLabelWidthWithNSString:(NSString*)lableText Font:(CGFloat)font
{
    CGSize maxSize = CGSizeMake(VIEW_WIDTH, HightScalar(53));
    CGSize curSize = [lableText boundingRectWithSize:maxSize
                                         options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                      attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:font] }
                                         context:nil].size;
    return curSize;

}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
