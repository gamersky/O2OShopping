//
//  MyGoodsTradeFinishedHeaderView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyGoodsTradeFinishedHeaderView
 Created_Date： 20160317
 Created_People：GT
 Function_description： 交易完成头部视图
 ***************************************/

#import <UIKit/UIKit.h>
@protocol MyGoodsTradeFinishedHeaderViewDelegate<NSObject>
@optional
- (void)goEvaluate;
@end
@interface MyGoodsTradeFinishedHeaderView : UIView
@property(nonatomic, unsafe_unretained)id<MyGoodsTradeFinishedHeaderViewDelegate>delegate;
@end
