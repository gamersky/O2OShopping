//
//  MyGoodsSureOredrShiHuiBiCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/5.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsSureOredrShiHuiBiCell.h"

//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "NSString+TextSize.h"
#define kleftSpace 15
#define krightSpace 15
#define kcellHight HightScalar(47)
#define ksepLineHight 0.5
#define khSpace 10
@interface MyGoodsSureOredrShiHuiBiCell()
@property(nonatomic, strong)UILabel *titleLabel;
@property(nonatomic, strong)UISwitch* mySwitch;
@end
@implementation MyGoodsSureOredrShiHuiBiCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //初始化视图
        [self setSubView];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setSubView {
    _titleLabel = [self setLabelWithTitle:@"" fontSize:FontSize(15) textColor:@"#555555"];
    _titleLabel.frame = CGRectZero;
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_titleLabel];
    
    _mySwitch = [[ UISwitch alloc]initWithFrame:CGRectMake(VIEW_WIDTH -krightSpace -50,HightScalar(5),0,kcellHight-HightScalar(10))];
    [_mySwitch addTarget: self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
     [_mySwitch setOn:YES animated:YES];
     _mySwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
   [self.contentView addSubview:_mySwitch];//添加到父视图
   
 }
- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHexString:textColor];
    label.minimumScaleFactor = 0.8;
    label.adjustsFontSizeToFitWidth = YES;
    return label;
}
- (void) switchValueChanged:(id)sender{
    UISwitch* control = (UISwitch*)sender;
    if(control == _mySwitch){
        BOOL on = control.on;
        if (_delegate &&[_delegate respondsToSelector:@selector(setSHiHuiBiSwitchState:)]) {
            [_delegate setSHiHuiBiSwitchState:on];
        }
    }
}
-(void)setUpViewWithShiHuiBi:(NSString *)shiHuiBi
{
     shiHuiBi =[NSString stringWithFormat:@"可以%@实惠币抵用%@元",shiHuiBi,shiHuiBi];
    CGSize titleSize = [shiHuiBi sizeWithFont:[UIFont systemFontOfSize:FontSize(15)]
                                      maxSize:CGSizeMake(VIEW_WIDTH-kleftSpace*2-70, kcellHight)];
    _titleLabel.frame = CGRectMake(kleftSpace, 0, titleSize.width , kcellHight - ksepLineHight);
    _titleLabel.text =shiHuiBi;
   
}
-(void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
