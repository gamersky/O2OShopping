//
//  MyGoodsTradeFinishedHeaderView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsTradeFinishedHeaderView.h"
//untils
#import "UIColor+Hex.h"
#import "Global.h"
#import "UIImage+ColorToImage.h"
#define ktopSpace HightScalar(15)
#define kvSpace HightScalar(9)
#define kLabelHight  HightScalar(28)
#define kbuttonHihgt HightScalar(36)
#define kbuttonWidht  HightScalar(102)
#define kimageWidth  HightScalar(56)

@implementation MyGoodsTradeFinishedHeaderView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
        [self setupSubView];
    }
    return self;
}
- (void)setupSubView {
    //image
   UIImage *image = [UIImage imageNamed:@"pay_failed"];
   UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake((VIEW_WIDTH - kimageWidth)/2
                                                                            , ktopSpace
                                                                            , kimageWidth
                                                                             , kimageWidth)];
    logoImageView.image = image;
    logoImageView.backgroundColor = [UIColor clearColor];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:logoImageView];
    
    //标题
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0
                                                                  , CGRectGetMaxY(logoImageView.frame) + kvSpace
                                                                  , VIEW_WIDTH
                                                                   , kLabelHight)];
    titleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    titleLabel.text = @"感谢您在大实惠购物";
    [self addSubview:titleLabel];
    
    UILabel *subTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(titleLabel.frame)
                                                                     , CGRectGetMaxY(titleLabel.frame)
                                                                     , CGRectGetWidth(titleLabel.frame)
                                                                      , CGRectGetHeight(titleLabel.frame))];
    subTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    subTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    subTitleLabel.text = @"欢迎您再次光临";
    [self addSubview:subTitleLabel];
    
    //去评价
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#ffffff"] frame:CGRectMake(0, 0, 1, 1)]
                      forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#efefef"] frame:CGRectMake(0, 0, 1, 1)]
//                      forState:UIControlStateHighlighted];
    button.frame = CGRectMake((VIEW_WIDTH - kbuttonWidht)/2
                              ,CGRectGetMaxY(subTitleLabel.frame) + ktopSpace
                              , kbuttonWidht
                              , kbuttonHihgt);
    [button setTitle:@"去评价" forState:UIControlStateNormal];
    [button.layer setBorderColor:[UIColor colorWithHexString:@"#db3b34"].CGColor];
    [button.layer setBorderWidth:1];
    [button.layer setMasksToBounds:YES];
    [button.layer setCornerRadius:3];
    [button setTitleColor:[UIColor colorWithHexString:@"#db3b34"] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    [button addTarget:self action:@selector(goEvaluateEvent) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
    
}

#pragma mark -- button Action
- (void)goEvaluateEvent {
    if (_delegate && [_delegate respondsToSelector:@selector(goEvaluate)]) {
        [_delegate goEvaluate];
    }
}
@end
