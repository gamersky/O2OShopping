//
//  WeiXinPayResultController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/8.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： WeiXinPayResultController
 Created_Date： 20151208
 Created_People：
 Function_description： 微信支付结果页面
 ***************************************/

#import "BaseViewController.h"

@interface WeiXinPayResultController : BaseViewController
@property(nonatomic, strong)NSString *state;
@property(nonatomic, strong)NSString *orderNum;
@property(nonatomic, strong)NSString *homeMakeType;
@property(nonatomic,strong)NSString *requestDataType;//requestDataType  商品订单 是1 服务订单是 0
@end
