//
//  MyOrderDetailController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyOrderDetailController.h"
//untils
#import "Global.h"
#import "UIImage+ColorToImage.h"
#import "UIColor+Hex.h"
#import "Global.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
#import "UserInfoModel.h"
#import "AppDelegate.h"
#import "RDVTabBarController.h"
//view
#import "MyOrderDetailCell.h"
#import "MTA.h"
@interface MyOrderDetailController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *orderDetailsTableView;
@property (nonatomic,strong)NSMutableArray *orderList;
@property (nonatomic,strong)NSMutableArray *goodsList;

//新用户
@property (nonatomic,strong)UILabel *xinUser;
//新用户优惠
@property (nonatomic,strong)UILabel *xinUserDiscount;
//总价格
@property (nonatomic,strong)UILabel *totalPrice;
@end

@implementation MyOrderDetailController

-(void)viewDidLoad {
    [super viewDidLoad];
    _orderList=[[NSMutableArray alloc]init];
    _goodsList=[[NSMutableArray alloc]init];
    [self setUpSuberView];
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
         [self initOrderDetailArray];
    }
  
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"订单详情"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"订单详情"];
}
//数据源
-(void)initOrderDetailArray
{
    //请求参数说明
    //SIGNATURE			设备识别码
    //TOKEN			用户签名
    //ORDERNUM			订单号
    NSString * requertKey=[self.requestDataType isEqualToString:@"1"]?@"order/detail":@"service/order/detail";
    if (self.homeMakeType.length !=0) {
        requertKey =@"ser/order/detail";
    }
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..." inView:self.view];
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringISNull:mode.token] forKey:@"TOKEN"];
    [parameter setObject:[NSString stringISNull:self.orderNum] forKey:@"ORDERNUM"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:requertKey success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            
            
            if ([state isEqualToString:@"0"]) {//请求成功
                [_orderList removeAllObjects];
                [_goodsList removeAllObjects];
                [weakSelf.orderList addObject:[dataDic objectForKey:@"OBJECT"]];
                if ([weakSelf.requestDataType isEqualToString:@"1"]) { //requestDataType  商品订单 是1 服务订单是 0
                    [weakSelf.goodsList addObjectsFromArray:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"GOODSLIST"]];
                }else if([weakSelf.requestDataType isEqualToString:@"0"]) //requestDataType  商品订单 是1 服务订单是 0
                {
                    if (weakSelf.homeMakeType.length!=0) {
                        [weakSelf.goodsList addObject:[dataDic objectForKey:@"OBJECT"]];
                        
                    }else{
                        [weakSelf.goodsList addObjectsFromArray:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"SERVICELIST"]];
                    }
                    
                }
                [_orderDetailsTableView reloadData];
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }
        }
        
        
    } failure:^(bool isFailure) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        
    }];
    
}

#pragma mark--UITableViewdelegate
//cell的行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section==0) {
        if([self.requestDataType isEqualToString:@"1"]) //requestDataType  商品订单 是1 服务订单是 0
        {
            return  _goodsList.count+2;
        }else
        {
            return  _goodsList.count+1;
        }
        
    }else{
        
//        if([self.requestDataType isEqualToString:@"1"])
//        {
//            return  8;
//        }else
//        {
//            return  8;
//        }
        return 8;
    }
}
//高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0) {
        if (indexPath.row==_goodsList.count+1) {
//            return 80;
            return HightScalar(40);
        }else{
            return HightScalar(50);
        }
    }else{
        return HightScalar(50);
    }
}

#pragma mark --UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData =[_orderList firstObject];
    
    if (indexPath.section==0) {
        if (indexPath.row==0) {
            NSString *ID =@"title";
            UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:ID];
            if (cell==nil) {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
                 cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
          
            cell.textLabel.text=@"大实惠云超市";
            cell.textLabel.textColor=[UIColor colorWithHexString:@"#000000"];
            cell.textLabel.font=[UIFont boldSystemFontOfSize:HightScalar(18)];
            return cell;
        }else if(indexPath.row ==_goodsList.count+1){

            NSString *ID =@"total";
            UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:ID];
            if (cell==nil) {
                cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
//                //新用户
//                _xinUser=[[UILabel alloc]initWithFrame:CGRectMake(15,10,75,20)];
//                
//                [cell.contentView addSubview:_xinUser];
//                //新用户 减图标
//                UIImageView *xinjian =[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_xinUser.frame), CGRectGetMinY(_xinUser.frame),CGRectGetHeight(_xinUser.bounds), CGRectGetHeight(_xinUser.bounds))];
//                xinjian.image=[UIImage imageNamed:@"newUserLiJian_Image"];
//                [cell.contentView addSubview:xinjian];
                
                //新用户 优惠额度
//                _xinUserDiscount=[[UILabel alloc]initWithFrame:CGRectMake(VIEW_WIDTH-65, CGRectGetMinY(_xinUser.frame), 50, CGRectGetHeight(_xinUser.bounds))];
                
//                [cell.contentView addSubview:_xinUserDiscount];
                //总价格
//                _totalPrice =[[UILabel alloc]initWithFrame:CGRectMake(VIEW_WIDTH-115, CGRectGetMaxY(self.xinUser.frame)+5, 100, CGRectGetHeight(self.xinUser.bounds))];
                
                _totalPrice =[[UILabel alloc]initWithFrame:CGRectMake(15,0, VIEW_WIDTH-30, HightScalar(40))];
                [cell.contentView addSubview:self.totalPrice];
                
            }
            _xinUser.font=[UIFont systemFontOfSize:FontSize(16)];
            _xinUser.textColor=[UIColor colorWithHexString:@"#555555"];
            _xinUserDiscount.textAlignment=NSTextAlignmentRight;
            _xinUserDiscount.textColor=[UIColor colorWithHexString:@"#555555"];
            _xinUserDiscount.font =[UIFont systemFontOfSize:FontSize(16)];
            _totalPrice.font=[UIFont systemFontOfSize:FontSize(16)];
            _totalPrice.textAlignment=NSTextAlignmentRight;
            _totalPrice.textColor=[UIColor colorWithHexString:@"#ff0000"];
            self.xinUser.text=@"新用户立减";
            self.xinUserDiscount.text=[NSString stringWithFormat:@"-￥%@",[NSString stringTransformObject:[dicData objectForKey:@""]]];
            self.totalPrice.text=[NSString stringWithFormat:@"总计：￥%.2f",[[NSString stringTransformObject:[dicData objectForKey:@"AMOUNT"]] floatValue]];
            return cell;
        }else {
            
            NSString *ID = [NSString stringWithFormat:@"cell%ld",(long)indexPath.row];
            MyOrderDetailCell*cell=[tableView dequeueReusableCellWithIdentifier:ID];
            if(!cell) {
                cell=[[MyOrderDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
                 cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            [cell retSubViewWithData:[_goodsList objectAtIndex:indexPath.row-1] requestDataType:self.requestDataType homeMakeType:self.homeMakeType];
            
            return cell;
        }
    }else{
        if (indexPath.row==0) {
            static NSString *identfy = @"dingdanhao";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
                
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.textLabel.text =[NSString stringWithFormat:@"订单号: %@",[NSString stringTransformObject:[dicData objectForKey:@"ORDERNUM"]]];
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            
            return cell;

        }else if(indexPath.row ==1){
            static NSString *identfy = @"xiadantime";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
                
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.textLabel.text =[NSString stringWithFormat:@"下单时间: %@",[NSString stringTransformObject:[dicData objectForKey:@"STARTDATE"]]];
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            
            return cell;
        }else if(indexPath.row ==2){
            static NSString *identfy = @"paytype";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            NSString *str=[NSString stringTransformObject:[dicData objectForKey:@"PAYTYPE"]];
            NSString *paytype=@"";
            if (str.integerValue==1) {
               
                paytype=@"在线支付";
            }else
            {
                if ([self.requestDataType isEqualToString:@"1"]) { //requestDataType  商品订单 是1 服务订单是 0
                paytype=@"货到付款";
                 }else if([self.requestDataType isEqualToString:@"0"]) //requestDataType  商品订单 是1 服务订单是 0
                 {
                 paytype=@"服务后付款";
            
                 }
            }
            cell.textLabel.text =[NSString stringWithFormat:@"支付方式: %@",paytype];
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            
            return cell;
        }else if(indexPath.row ==3){
            static NSString *identfy = @"peisongtype";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
                
            }
            NSString *str=@"";
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            if ([self.requestDataType isEqualToString:@"1"]) { //requestDataType  商品订单 是1 服务订单是 0
                str=[NSString stringTransformObject:[dicData objectForKey:@"TAKETYPE"]];
                NSString *taketype=@"";
                if (str.integerValue==1) {
                    taketype=@"送货";
                }else
                {
                    taketype=@"自取";
                }
                cell.textLabel.text =[NSString stringWithFormat:@"配送方式: %@",taketype];
            }else if([self.requestDataType isEqualToString:@"0"]){ //requestDataType  商品订单 是1 服务订单是 0
                str=[NSString stringTransformObject:[dicData objectForKey:@"SERTIME"]];
                cell.textLabel.text =[NSString stringWithFormat:@"服务时间: %@",str];
            }
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            
            return cell;

        }else if(indexPath.row ==4){
            static NSString *identfy = @"TEL";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
                
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.textLabel.text =[NSString stringWithFormat:@"手机号: %@",[NSString stringTransformObject:[dicData objectForKey:@"TEL"]]];
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            
            return cell;
                    }else if(indexPath.row ==5){
            static NSString *identfy = @"LINKNAME";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
                
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.textLabel.text =[NSString stringWithFormat:@"联系人: %@",[NSString stringTransformObject:[dicData objectForKey:@"LINKNAME"]]];
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            
            return cell;

        }else if(indexPath.row ==6){
          
            static NSString *identfy = @"ADDRESS";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
                
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            NSString *address=@"";
            if ([self.requestDataType isEqualToString:@"1"]) { //requestDataType  商品订单 是1 服务订单是 0
                address=[NSString stringWithFormat:@"收货地址: %@",[NSString stringTransformObject:[dicData objectForKey:@"ADDRESS"]]];
            }else if([self.requestDataType isEqualToString:@"0"]) //requestDataType  商品订单 是1 服务订单是 0
            {
                address=[NSString stringWithFormat:@"服务地点: %@",[NSString stringTransformObject:[dicData objectForKey:@"ADDRESS"]]];
                
            }
            cell.textLabel.text =address;
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            
            return cell;

        }else {
           
            static NSString *identfy = @"peisongbeizhu";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identfy];
            if (cell == nil) {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identfy];
                
            }
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            NSString *str=[NSString stringTransformObject:[dicData objectForKey:@"DESCRIBE"]];
            NSString *takebeizhu=@"";
            if (str.length==0) {
                takebeizhu =@"无";
            }else
            {
                takebeizhu=str;
            }
            NSString *beizhu=@"";
            if ([self.requestDataType isEqualToString:@"1"]) { //requestDataType  商品订单 是1 服务订单是 0
                beizhu=[NSString stringWithFormat:@"配送备注: %@",takebeizhu];
            }else if([self.requestDataType isEqualToString:@"0"]) //requestDataType  商品订单 是1 服务订单是 0
            {
                beizhu=[NSString stringWithFormat:@"预计时长: %@",[NSString stringTransformObject:[dicData objectForKey:@"TOTALTIME"]]];
                if ([[NSString stringTransformObject:[dicData objectForKey:@"TOTALTIME"]] length]==0) {
                    beizhu = @"预计时长: 以实际情况为准";
                }
                
            }
            cell.textLabel.text =beizhu;
            cell.textLabel.font =[UIFont systemFontOfSize:FontSize(16)];
            
            return cell;


        }
        
    }
}
//黑条效果
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HightScalar(12);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *views = [[UIView alloc]init];
    views.backgroundColor = [UIColor colorWithRed:238/250.0 green:238/250.0 blue:238/250.0 alpha:1];
    return views;
}

#pragma mark-- 初始化控件
-(void)setUpSuberView
{
    self.orderDetailsTableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0,VIEW_WIDTH
                                                                             , VIEW_HEIGHT - HightScalar(40)-5 - 64)
                                                            style:UITableViewStylePlain];
    [self.view addSubview:self.orderDetailsTableView];
    self.orderDetailsTableView.delegate=self;
    self.orderDetailsTableView.dataSource=self;
    _orderDetailsTableView.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH,  HightScalar(40))];
    view.backgroundColor = [UIColor colorWithRed:238/250.0 green:238/250.0 blue:238/250.0 alpha:1];
     //_orderDetailsTableView.tableFooterView = view;
    
    UIButton * AgainBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    AgainBtn.backgroundColor=[UIColor redColor];
    AgainBtn.frame=CGRectMake(15, (CGRectGetHeight(view.bounds)- HightScalar(40))/2, VIEW_WIDTH-30, HightScalar(40));
    
    UIImage *againBackgroundImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#0093dd"]
                                                            frame:AgainBtn.bounds];
    [AgainBtn setBackgroundImage:againBackgroundImage forState:UIControlStateNormal];
    [AgainBtn setTitle:@"再来一单" forState:UIControlStateNormal];
    AgainBtn.titleLabel.textColor=[UIColor whiteColor];
    [AgainBtn.layer setMasksToBounds:YES];
    [AgainBtn.layer setCornerRadius:4.0];
   
    
    AgainBtn.titleLabel.font=[UIFont systemFontOfSize:14.0f];
    AgainBtn.hidden=YES;
    
    [view addSubview:AgainBtn];
   
}

@end
