//
//  SureOrderController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： SureOrderController
 Created_Date： 20151107
 Created_People：gt
 Function_description：确认订单页面(现请求地址数据，然后加载视图)
 ***************************************/

#import "BaseViewController.h"
#import "ReceivedAddressOBJECT.h"
typedef NS_ENUM(NSInteger, SureOrderType) {
    kproductSureOrder = 0,//商品订单确认
    kserviceSureorder //服务订单确认
};
@interface SureOrderController : BaseViewController
@property(nonatomic)SureOrderType orderType;
@property(nonatomic, strong)NSString *markContent;
@property(nonatomic,strong)NSString *serviceTime;
@property(nonatomic, strong)ReceivedAddressOBJECT *addressData;
@property(nonatomic, strong)NSMutableArray *serviceDataSource;//服务数据源
@property(nonatomic, strong)NSString * homeMakeType; //1日常保洁 2深度保洁
@end
