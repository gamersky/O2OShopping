//
//  ServerDetailServerTypeView.h
//  ServerDetailDemo
//
//  Created by apple on 15/12/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： ServerDetailHeaderView
 Created_Date： 20151221
 Created_People： GT
 Function_description： 
 
 ***************************************/

#import <UIKit/UIKit.h>

@interface ServerDetailServerTypeView : UIView
@property(nonatomic, strong)NSString *currentPrice;//当前价格
@property(nonatomic, strong)NSString *oldPrice;//优惠前价格
@property(nonatomic, strong)NSString *btntitle;//标题
@property(nonatomic, strong)NSString *typeID;//类型ID

@end
