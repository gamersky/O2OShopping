//
//  RegisterOneStepViewController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/10.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "BaseViewController.h"
#import "Global.h"
//判断第一步是注册还是修改密码（页面判断）
typedef NS_ENUM(NSInteger, OneStepIsRegisterOrModifiedPassword) {
    OneStepIsRegister = 0,//注册
    OneStepIsModifiedPassword = 1,//修改密码
    OneStepIsFindTheSecret = 2,//找回密码
    OneStepIsModifiedPhoneNum //变更手机号
};

@interface RegisterOneStepViewController : BaseViewController
@property (nonatomic)OneStepIsRegisterOrModifiedPassword typeOfView;//页面判断
@end
