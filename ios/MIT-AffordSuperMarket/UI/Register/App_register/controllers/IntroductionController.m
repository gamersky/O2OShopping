//
//  IntroductionController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/3.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "IntroductionController.h"
//vendor

//controllers
//views
#import "SMPageControl.h"
//untils
#import "Global.h"
#import "UIImage+ColorToImage.h"
#import "UIColor+Hex.h"
#import "ManagerHttpBase.h"
#import "ManagerGlobeUntil.h"
#import "NSString+Conversion.h"
//model
#import "XiaoQuAndStoreInfoModel.h"
@interface IntroductionController ()
@property (strong, nonatomic) SMPageControl *pageControl;
@property (strong, nonatomic) UIButton *registerBtn;
@property (strong, nonatomic) NSArray *imageNames;
@end

@implementation IntroductionController

- (NSUInteger)numberOfPages
{
    return 3;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    _imageNames = @[@"introduction_First",@"introduction_second",@"introduction_thrid"];

    
    [self configureViews];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark Super
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    NSInteger nearestPage = scrollView.contentOffset.x/VIEW_WIDTH + 0.5;
    self.pageControl.currentPage = nearestPage;
}

#pragma Views
- (void)configureViews{
    [self configurePageControlView];
    [self configureButtonView];
    [self configureImageView];
}
//配置images
- (void)configureImageView {
    for (NSInteger i = 0; i < self.imageNames.count; i++) {
        UIImageView *imageView = [[UIImageView alloc]init];
        UIImage *image = [UIImage imageNamed:[self.imageNames objectAtIndex:i]];
        imageView.image = image;
        CGFloat y = -20;
        if (VIEW_HEIGHT != 480) {
            y = 0;
            imageView.contentMode = UIViewContentModeScaleAspectFit;
        }
       
        imageView.frame = CGRectMake(i*VIEW_WIDTH, y, VIEW_WIDTH, VIEW_HEIGHT);
        if (i == 2) {
            imageView.userInteractionEnabled = YES;
            [imageView addSubview:self.registerBtn];
        }
        [self.contentView addSubview:imageView];
    }
}
//配置pageControl
- (void)configurePageControlView {
    _pageControl = [[SMPageControl alloc] init];
    _pageControl.frame = CGRectMake((VIEW_WIDTH - 160)/2, VIEW_HEIGHT - 10, 160, 10);
    _pageControl.numberOfPages = self.numberOfPages;
    _pageControl.userInteractionEnabled = NO;
    _pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"#c52720"];
    _pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"#dededd"];
    _pageControl.currentPage = 0;
    [self.view addSubview:_pageControl];
}
//配置button
- (void)configureButtonView {
    CGFloat y = -20;
    if (VIEW_HEIGHT != 480) {
        y = 0;
    }
    _registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _registerBtn.frame = CGRectMake(65
                                   , CGRectGetHeight(self.view.bounds) - 44 - 20 - y
                                   , CGRectGetWidth(self.view.bounds) - 2*65
                                   , 44);
    _registerBtn.backgroundColor = [UIColor blueColor];
    [_registerBtn setTitle:@"立即体验" forState:UIControlStateNormal];
    [_registerBtn setTitleColor:[UIColor colorWithHexString:@"#f09032"] forState:UIControlStateNormal];
    UIImage *backImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#ffffff"] frame:CGRectMake(0, 0, 180, 44)];
    [_registerBtn setBackgroundImage:backImage forState:UIControlStateNormal];
    [_registerBtn.layer setMasksToBounds:YES];
    [_registerBtn.layer setBorderColor:[UIColor colorWithHexString:@"#f09032"].CGColor];
    [_registerBtn.layer setBorderWidth:2];
    [_registerBtn addTarget:self action:@selector(registerBtnClicked) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark -- 请求注册
//注册协议
- (void)loadRigisterData {
    //增加网络监听
//    [[ManagerHttpBase sharedManager] managerReachability];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    [manager rigisterAppSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dataDic = (NSDictionary*)responseObject;
       // NSString *msg = [dataDic objectForKey:@"MSG"];
        NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
        if ([state isEqualToString:@"0"]) {//请求成功
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:[NSString stringISNull:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"SIGNATURE"]]  forKey: Phone_Signature];
            [userDefaults synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:End_IntroductionPage object:nil];
        }else {//请求失败
            
        }
       
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        
    }];
   
    
}

////获取默认社区的百度位置主键(针对是否跳转到定位页面，如果返回百度主键就跳转到首页，如果未返回就跳转到定位页面)
//- (void)loadBaiDuDefaultKeyword {
//    //请求参数说明： SIGNATURE  设备识别码
//     ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
//    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
//   __weak typeof(self)weakSelf = self;
//    [manager parameters:parameter customPOST:@"common/defLocation" success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSDictionary *dataDic = (NSDictionary*)responseObject;
//        // NSString *msg = [dataDic objectForKey:@"MSG"];
//         NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
//        if ([state isEqualToString:@"0"]) {//请求成功
//            NSString *baiduUID = [NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"BAIDUKEY"]];
//            if (baiduUID.length != 0) {//直接跳转到首页面（针对苹果审核）
//                [weakSelf LoadXiaoQuInfoDataWithBaiDuUID:baiduUID];
//            } else {//未返回百度主键，跳转到定位页面
//                
//                
//            }
//            
//        }else {//请求失败
//            
//        }
//
//        
//    } failure:^(bool isFailure) {
//        
//        
//    }];
//}
////根据百度id获取小区和商店信息
//- (void)LoadXiaoQuInfoDataWithBaiDuUID:(NSString*)uid {
//    
//    //请求参数说明： SIGNATURE  设备识别码
//    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
//    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
//    [parameter setObject:uid forKey:@"BAIDUKEY"];
//  __weak typeof(self)weakSelf = self;
//    [manager parameters:parameter customPOST:@"common/location" success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSDictionary *dataDic = (NSDictionary*)responseObject;
//        // NSString *msg = [dataDic objectForKey:@"MSG"];
//        NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
//        if ([state isEqualToString:@"0"]) {//请求成功
//            
//            if (dataDic.count != 0) {
//                XiaoQuAndStoreInfoModel *model = [[XiaoQuAndStoreInfoModel alloc]init];
//                model.xiaoQuID = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"COMMUNITY"] objectForKey:@"ID"]];
//                model.xiaoQuName = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"COMMUNITY"] objectForKey:@"TITLE"]];
//                model.xiaoQuAddress = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"COMMUNITY"] objectForKey:@"ADDRESS"]];
//                model.storeID = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"STORE"] objectForKey:@"ID"]];
//                model.storeName = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"STORE"] objectForKey:@"ADDRESS"]];
//                model.storeAddress = [NSString stringTransformObject:[[[dataDic objectForKey:@"OBJECT"] objectForKey:@"STORE"] objectForKey:@"TITLE"]];
//                [[ManagerGlobeUntil sharedManager] updataXiaoquAndStoreInfoWithModel:model];
//                 [[NSNotificationCenter defaultCenter] postNotificationName:End_IntroductionPage object:nil];
//                
//            }
//            
//        }else {//请求失败
//            
//        }
//        
//        
//    } failure:^(bool isFailure) {
//        
//       
//    }];
//}

#pragma mark Action
- (void)registerBtnClicked{
    [self loadRigisterData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
