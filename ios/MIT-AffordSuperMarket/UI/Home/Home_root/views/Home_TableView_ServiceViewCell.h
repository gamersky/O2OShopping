//
//  Home_TableView_ServiceViewCell.h
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Home_TableView_ServiceViewCell
 Created_Date： 20160217
 Created_People： GT
 Function_description： 服务大类展示视图
 ***************************************/

#import <UIKit/UIKit.h>
@protocol Home_TableView_ServiceViewCellDelegate <NSObject>
- (void)selectedItemAtIndexPath:(NSIndexPath *)indexPath;
@end
@interface Home_TableView_ServiceViewCell : UITableViewCell
- (void)updateViewWithData:(NSArray*)dataArr;
@property(assign,nonatomic)id<Home_TableView_ServiceViewCellDelegate>delegate;
@end


