//
//  Home_Root_HeaderView.m
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.

//高度是根据苹果6屏幕进行比例缩放
#define kloopScrollViewHight HightScalar(141) //轮播图高度
#define kcategoryViewHight HightScalar(136) //分类（大类）高度
#define krecommandViewHight HightScalar(262) //推荐视图高度
#define kserviceViewHight HightScalar(196) //服务视图高度
#define ksectionFooterViewHight 10 //section底部视图
#define ksectionHeaderViewHight HightScalar(44)


#import "Home_Root_HeaderView.h"
//view
#import "Home_TableView_BigKindVIewCell.h"
#import "Home_TableView_SubjectViewCell.h"
#import "Home_TableView_ServiceViewCell.h"
#import "Home_TableView_recommendStyle.h"
//vendor
#import "HYBLoopScrollView.h"
//untils
#import "UIColor+Hex.h"
#import "Global.h"
@interface Home_Root_HeaderView ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic, strong)UITableView *tableview;
@property(nonatomic, strong)HYBLoopScrollView *loopScrollView;//轮播图
@property(nonatomic, strong)NSMutableArray *dataSource;
@property(nonatomic, strong)NSMutableArray *loopImagesArr;
@property(nonatomic, weak)id delegate;
@end

@implementation Home_Root_HeaderView
- (id)initWithFrame:(CGRect)frame delegate:(id)delegate {
    self = [super initWithFrame:frame];
    if (self) {
        _delegate = delegate;
        _dataSource = [[NSMutableArray alloc]init];
        _loopImagesArr =[[NSMutableArray alloc]init];
        [self setupSubView];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame delegate:nil];
}

#pragma mark -- 更新视图
- (void)updateTableViewWithData:(NSDictionary*)dataDic {
    
    [self.dataSource removeAllObjects];
    
    [_loopImagesArr addObjectsFromArray:[dataDic objectForKey:@"loopData"]];
    _loopScrollView.imageUrls =_loopImagesArr;
    [self.dataSource addObjectsFromArray:[dataDic objectForKey:@"otherData"]];
    CGFloat factHight = [self calculationFactHight];
    self.frame = CGRectMake(0, 0, VIEW_WIDTH, factHight);
    self.tableview.frame = CGRectMake(0, 0, VIEW_WIDTH , factHight);
    [self.tableview reloadData];
}
//计算头部高度
-(CGFloat)calculationFactHight
{
    
    return kloopScrollViewHight +kcategoryViewHight + (self.dataSource.count - 3)*krecommandViewHight + kserviceViewHight + (self.dataSource.count-1)*ksectionFooterViewHight + ksectionHeaderViewHight;
    
}
#pragma mark -- 初始化子视图
- (void)setupSubView {
    [self addSubview:self.tableview];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.dataSource count];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary * dataDic =[_dataSource objectAtIndex:indexPath.section];
    
    if ([[dataDic objectForKey:@"viewType"]isEqualToString:@"bigKind"]) {//大类视图
        static NSString *bigClassCellIdentifier = @"bigClassCellIdentifier";
        Home_TableView_BigKindVIewCell *bigClassCell = [tableView dequeueReusableCellWithIdentifier:bigClassCellIdentifier];
        if (!bigClassCell) {
            bigClassCell = [[Home_TableView_BigKindVIewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:bigClassCellIdentifier];
            bigClassCell.selectionStyle =UITableViewCellSelectionStyleNone;
        }
        [bigClassCell.contentView.layer setBorderWidth:0.7];
        [bigClassCell.contentView.layer setBorderColor:[UIColor colorWithHexString:@"#dedede"].CGColor];
        [bigClassCell updateViewWithData:[dataDic objectForKey:@"Data"] sectionTitle:[dataDic objectForKey:@"sectionTitle"]];
        return bigClassCell;
    } else if ([[dataDic objectForKey:@"viewType"]isEqualToString:@"headerTag"]) {
        static NSString *limitCountCellIdentifier = @"limitCountCellIdentifier";
        Home_TableView_SubjectViewCell *limtCountCell = [tableView dequeueReusableCellWithIdentifier:limitCountCellIdentifier];
        if (!limtCountCell) {
            limtCountCell = [[Home_TableView_SubjectViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:limitCountCellIdentifier];
            limtCountCell.selectionStyle =UITableViewCellSelectionStyleNone;
        }
        limtCountCell.delegate = _delegate;
        [limtCountCell.contentView.layer setBorderWidth:0.7];
        [limtCountCell.contentView.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
        [limtCountCell updateViewWithData:[dataDic objectForKey:@"Data"]];
        return limtCountCell;
    }else if ([[dataDic objectForKey:@"viewType"]isEqualToString:@"service"]) {
        static NSString *serviceCellIdentifier = @"serviceCellIdentifier";
        Home_TableView_ServiceViewCell *servieceCountCell = [tableView dequeueReusableCellWithIdentifier:serviceCellIdentifier];
        if (!servieceCountCell) {
            servieceCountCell = [[Home_TableView_ServiceViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:serviceCellIdentifier];
            servieceCountCell.selectionStyle =UITableViewCellSelectionStyleNone;
        }
        [servieceCountCell.contentView.layer setBorderWidth:0.7];
        [servieceCountCell.contentView.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
        servieceCountCell.delegate =_delegate;
        [servieceCountCell updateViewWithData:[dataDic objectForKey:@"Data"]];
        return servieceCountCell;
    }
    static NSString *cellIdentifier = @"cellIdentifier";
    Home_TableView_recommendStyle *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[Home_TableView_recommendStyle alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    [cell updateViewWithData:[dataDic objectForKey:@"Data"]];
    return cell;
    return nil;
}


#pragma mark -- UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"selectedIndex = %ld",(long)indexPath.row);
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary * dataDic =[_dataSource objectAtIndex:indexPath.section];
    
    if ([[dataDic objectForKey:@"viewType"]isEqualToString:@"bigKind"]) {
        return kcategoryViewHight;
    }else if ([[dataDic objectForKey:@"viewType"]isEqualToString:@"headerTag"]) {
        return krecommandViewHight;
    }else if([[dataDic objectForKey:@"viewType"]isEqualToString:@"service"]) {
        return kserviceViewHight;
    }
    return HightScalar(44);
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == _dataSource.count-1) {
        return 0;
    }else{
        return ksectionFooterViewHight;
    }
}
#pragma mark -- 懒加载
- (UITableView*)tableview {
    if (!_tableview) {
        UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = [UIColor whiteColor];
        tableView.tableHeaderView = self.loopScrollView;
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _tableview = tableView;
    }
    return _tableview;
}




//设置pagecontroll样式

- (HYBLoopScrollView*)loopScrollView {
    if (!_loopScrollView) {
        CGRect loopScrollViewFrame = CGRectMake(0, 0, VIEW_WIDTH,kloopScrollViewHight);
        
        HYBLoopScrollView *loopView = [HYBLoopScrollView loopScrollViewWithFrame:loopScrollViewFrame
                                                                       imageUrls:@[@"mystore_img_figure"]];
        loopView.placeholder = [UIImage imageNamed:@"productDetail_Default_loopImage"];
        //设置pagecontroll样式
        loopView.pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"#c52720"];
        loopView.pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"#dededd"];
        loopView.timeInterval = 3;//设置间隔时间
        loopView.alignment = kPageControlAlignRight;//设置pagecontroll对齐方式
        loopView.backgroundColor = [UIColor clearColor];
        _loopScrollView = loopView;
    }
    return _loopScrollView;
}
@end
