//
//  Home_TableView_SubjectViewCell.h
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Home_TableView_SubjectViewCell
 Created_Date： 20160217
 Created_People： GT
 Function_description： 专题商品展示视图
 ***************************************/

#import <UIKit/UIKit.h>
@protocol Home_TableView_SubjectViewCellDelegate <NSObject>
- (void)selectedItemAtIndexPath:(NSIndexPath *)indexPath goodID:(NSString *)goodID isSelf:(NSString *)isSelf;
- (void)moreButtonSelectedWithListByTag:(NSString *)listTag listName:(NSString *)listName;
@end
@interface Home_TableView_SubjectViewCell : UITableViewCell
@property(assign,nonatomic)id<Home_TableView_SubjectViewCellDelegate>delegate;
- (void)updateViewWithData:(NSDictionary*)dataDic;
@end


