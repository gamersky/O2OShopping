//
//  ServerDetailPriceIntroducedCell.h
//  ServerDetailDemo
//
//  Created by apple on 15/12/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServerDetailPriceIntroducedCell : UITableViewCell
@property(nonatomic, strong)NSString *content;
@property(nonatomic, strong)NSString *title;
- (CGFloat)cellFactHeight;
@end
