//
//  Strore_TeHui_Cell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/7.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Strore_TeHui_Cell.h"
#import "Strore_TeHui_Controller.h"
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#import "NSString+Conversion.h"
//vendor
#import "Masonry.h"
#import "UIImageView+AFNetworking.h"
#import "KZLinkLabel.h"
@interface Strore_TeHui_Cell ()
@property(nonatomic, strong)UIImageView *imageview;//产品图片
@property(nonatomic, strong)KZLinkLabel *titleLabel;//标题
@property(nonatomic, strong)UILabel *currentPricelabel;//优惠价格
@property(nonatomic, strong)UILabel *oldPricelabel;//优惠前价格

@end
@implementation Strore_TeHui_Cell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupSubView];
    }
    return self;
}

#pragma makr - 初始化控件
- (void)setupSubView {
    //图片8
    _imageview = [[UIImageView alloc]initWithFrame:CGRectMake(15, 10, HightScalar(106),  HightScalar(106))];
    _imageview.backgroundColor = [UIColor clearColor];
    _imageview.image = [UIImage imageNamed:@"place_image"];
    [_imageview.layer setBorderWidth:1];
    [_imageview.layer setBorderColor:[UIColor colorWithHexString:@"#efefef"].CGColor];
    [_imageview.layer setMasksToBounds:YES];
    [_imageview.layer setCornerRadius:3];
    [self.contentView addSubview:_imageview];
    //标题
    _titleLabel = [[KZLinkLabel alloc]init];
    _titleLabel.numberOfLines = 0;
    _titleLabel.font = [UIFont systemFontOfSize:FontSize(15)];
    _titleLabel.automaticLinkDetectionEnabled = YES;
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _titleLabel.numberOfLines =2;
    [_titleLabel sizeToFit];
    _titleLabel.textColor = [UIColor colorWithHexString:@"#72706f"];
    _titleLabel.frame=CGRectMake(CGRectGetMaxX(self.imageview.frame)+10
                                 , CGRectGetMinY(self.imageview.frame)+3
                                 , VIEW_WIDTH - CGRectGetMaxX(_imageview.frame) -10 - 10, HightScalar(50));
    [self.contentView addSubview:_titleLabel];
    //优惠价格
    _currentPricelabel = [self setLabelWithTitle:@"￥6.00" fontSize:FontSize(15) textColor:@"#da251d"];
    _currentPricelabel.frame=CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(_titleLabel.frame) , CGRectGetWidth(self.titleLabel.bounds), HightScalar(30));
    [self.contentView addSubview:self.currentPricelabel];
    
   
    //优惠前价格
   

    _oldPricelabel = [self setLabelWithTitle:nil fontSize:FontSize(14) textColor:@"#72706f"];
    _oldPricelabel.frame=CGRectMake(CGRectGetMinX(_currentPricelabel.frame)+2, CGRectGetMaxY(_currentPricelabel.frame)-5, CGRectGetWidth(self.currentPricelabel.bounds), HightScalar(30));
    //中划线
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:@"原价￥12.00" attributes:attribtDic];
    
    _oldPricelabel.attributedText = attribtStr;
    [self.contentView addSubview:_oldPricelabel];
    
    //立即抢购按钮
    _panicbuying =[UIButton buttonWithType:UIButtonTypeCustom];
    _panicbuying.frame=CGRectMake(VIEW_WIDTH - 15 - 44, CGRectGetMidY(_currentPricelabel.frame) - 20,50, 50);
    [_panicbuying setImage:[UIImage imageNamed:@"cvs_btn_addshopcar"] forState:UIControlStateNormal];
    [_panicbuying setImage:[UIImage imageNamed:@"cvs_btn_addshopcar_press"] forState:UIControlStateHighlighted];
    [_panicbuying addTarget:self action:@selector(buttonEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_panicbuying];
}
//生成label对象
- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.textColor = [UIColor colorWithHexString:textColor];
    label.backgroundColor = [UIColor clearColor];
    return label;
}
#pragma mark -- button Action 
- (void)buttonEvent:(id)sender {
//    if (_delegate && [_delegate respondsToSelector:@selector(gotoBuyWithItemTag:)]) {
//        [_delegate gotoBuyWithItemTag:self.indexPath.row];
//    }
    if (_delegate && [_delegate respondsToSelector:@selector(addShoppingCarWithIndex:)]) {
        [_delegate addShoppingCarWithIndex:self.indexPath];
    }
}
#pragma mark -- 数据初始化视图
-(void)updatawithData:(NSDictionary*)dataDic withType:(NSInteger)type{
    if ([dataDic count] != 0) {
        [self.imageview setImageWithURL:[NSURL URLWithString:[NSString appendImageUrlWithServerUrl:[NSString stringTransformObject:[dataDic objectForKey:@"THUMB"]]]]
                       placeholderImage:[UIImage imageNamed:@"place_image"]];
        if (type == 101) {
            UIImage *mergerImage = [self waterImageWithBgImage:self.imageview.image waterImage:[UIImage imageNamed:@"icon_limitcount"] scale:[UIScreen mainScreen].scale];
            self.imageview.image = mergerImage;
        } else if (type == 102) {
             UIImage *mergerImage = [self waterImageWithBgImage:self.imageview.image waterImage:[UIImage imageNamed:@"icon_yiyuango"] scale:[UIScreen mainScreen].scale];
            self.imageview.image = mergerImage;
        }
        NSString *isSelf =[NSString stringTransformObject:[dataDic objectForKey:@"ISSELF"]];
        if ([isSelf isEqualToString:@"1"]) {
            NSString *emojiString = [NSString stringWithFormat:@"[zhiying] %@",[NSString stringISNull:[dataDic objectForKey:@"NAME"]]];
            UIFont *font = [UIFont systemFontOfSize:FontSize(14)];
            NSDictionary *attributes = @{NSFontAttributeName: font};
            NSAttributedString *attributedString = [NSAttributedString emotionAttributedStringFrom:emojiString attributes:attributes];
            
            self.titleLabel.attributedText =attributedString;
        }else{
            self.titleLabel.attributedText = [self labelLineSpaceWithSpace:4 text:[NSString stringISNull:[dataDic objectForKey:@"NAME"]]];
        }
        self.currentPricelabel.text =[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"SELLPRICE"]] floatValue]];
        NSString *oldPriceStr =[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"MARKETPRICE"]] floatValue]];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:oldPriceStr];
        [attrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, oldPriceStr.length)];
        self.oldPricelabel.attributedText = attrString;

        
    }
}

-(UIImage*)waterImageWithBgImage:(UIImage *)bgImage waterImage:(UIImage *)waterImage scale:(CGFloat)scale{
    // 生成一张有水印的图片，一定要获取UIImage对象 然后显示在imageView上
    
    //NSLog(@"bgImage Size: %@",NSStringFromCGSize(bgImage.size));
    // 1.创建一个位图【图片】，开启位图上下文
    // size:位图大小
    // opaque: alpha通道 YES:不透明/ NO透明 使用NO,生成的更清析
    // scale 比例 设置0.0为屏幕的比例
    // scale 是用于获取生成图片大小 比如位图大小：20X20 / 生成一张图片：（20 *scale X 20 *scale)
    //NSLog(@"当前屏幕的比例 %f",[UIScreen mainScreen].scale);
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), NO, scale);
    
    // 2.画背景图
    [bgImage drawInRect:CGRectMake(0, 0, 80, 80)];
    
    // 3.画水印
    // 算水印的位置和大小
    // 一般会通过一个比例来缩小水印图片
    CGFloat waterW = waterImage.size.width ;
    CGFloat waterH = waterImage.size.height;
    CGFloat waterX = 2;
    CGFloat waterY = 2;
    
    
    [waterImage drawInRect:CGRectMake(waterX, waterY, waterW, waterH)];
    
    // 4.从位图上下文获取 当前编辑的图片
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    
    // 5.结束当前位置编辑
    UIGraphicsEndImageContext();
    
    
    return newImage;
}
//设置行间距
- (NSMutableAttributedString *)labelLineSpaceWithSpace:(CGFloat)lineSpace text:(NSString*)text {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:lineSpace];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [text length])];
   return attributedString;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
