//
//  Strort_BreakFast_RightCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/25.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#define KRightSpacing 15
#define KLeftSpacing 15
#define KTopSpacing 10
#define KSpacing 10
#import "Strort_Breakfast_RightCell.h"
#import "UIColor+Hex.h"
#import "Global.h"
#import "NSString+Conversion.h"
#import "UIImage+ColorToImage.h"
#import "FMDBManager.h"
@interface Strort_Breakfast_RightCell ()

//商品图片
@property (retain ,nonatomic) UIImageView *productImage;
//商品名字
@property (retain ,nonatomic) UILabel *productName;
//商品价格
@property (retain ,nonatomic) UILabel *productMoney;
//销售数量
@property (retain ,nonatomic)UILabel *saleNumber;
//左减号
@property (retain,nonatomic)UIButton *leftBtn;
//右加号
@property (retain,nonatomic)UIButton *rightBtn;



@end
@implementation Strort_Breakfast_RightCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        _productImage =[[UIImageView alloc]init];
        [self.contentView addSubview:_productImage];
        self.productImage.frame =CGRectMake(KLeftSpacing, KTopSpacing, 60, 60);
        
        
        
        _productName =[[UILabel alloc]init];
        [self.contentView addSubview:_productName];
        _productName.font=[UIFont systemFontOfSize:14.0];
        self.productName.frame =CGRectMake(CGRectGetMaxX(self.productImage.frame)+KSpacing, CGRectGetMinY(self.productImage.frame), CGRectGetWidth(self.bounds)-80-KLeftSpacing-KRightSpacing, 20);
        _productName.textColor=[UIColor colorWithHexString:@"#4d4948"];
        
        
        _saleNumber =[[UILabel alloc]init];
        [self.contentView addSubview:_saleNumber];
        _saleNumber.font =[UIFont systemFontOfSize:12.0f];
        _saleNumber.textColor=[UIColor grayColor];
         _saleNumber.textColor=[UIColor colorWithHexString:@"#4d4948"];
        self.saleNumber.frame=CGRectMake(CGRectGetMinX(self.productName.frame), CGRectGetMaxY(self.productName.frame)+KSpacing, CGRectGetWidth(self.productName.bounds)/2, CGRectGetHeight(self.productName.bounds));
        
        _productMoney =[[UILabel alloc]init];
        [self.contentView addSubview:_productMoney];
        _productMoney.font=[UIFont systemFontOfSize:14.0f];
        _productMoney.textColor=[UIColor redColor];
        self.productMoney.frame=CGRectMake(CGRectGetMinX(self.saleNumber.frame),CGRectGetMaxY(self.saleNumber.frame)+KSpacing,80,CGRectGetHeight(self.saleNumber.bounds));
       
        _productMoney.textColor=[UIColor colorWithHexString:@"#000000"];
        _productMoney.textAlignment=NSTextAlignmentLeft;
        
        
        //右边加按钮
        _rightBtn =[[UIButton alloc]initWithFrame:CGRectMake(VIEW_WIDTH - 34 -10-75, CGRectGetMaxY(self.productName.frame) + 3, 34, 34)];
        [self.contentView addSubview:_rightBtn];
        [_rightBtn addTarget:self action:@selector(btnClike:) forControlEvents:UIControlEventTouchUpInside];
        [_rightBtn setImage:[UIImage imageNamed:@"btn_cart_normal_jia"] forState:UIControlStateNormal];
        [_rightBtn setImage:[UIImage imageNamed:@"btn_cart_press_jia"] forState:UIControlStateHighlighted];
        _rightBtn.tag=12;
        
        //购买数量
        _productQuantity =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.rightBtn.frame)- 20, CGRectGetMinY(self.rightBtn.frame), 20, CGRectGetHeight(self.rightBtn.bounds))];
        
        [self.contentView addSubview:_productQuantity];
        _productQuantity.textColor=[UIColor colorWithHexString:@"#555555"];
        _productQuantity.text=@"0";
        _productQuantity.textAlignment=NSTextAlignmentCenter;
        _productQuantity.font=[UIFont systemFontOfSize:12.0f];
        //左边减按钮
        _leftBtn =[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.productQuantity.frame) - CGRectGetWidth(self.rightBtn.bounds), CGRectGetMinY(self.productQuantity.frame), CGRectGetWidth(self.rightBtn.bounds), CGRectGetHeight(self.rightBtn.bounds))];
        [self.contentView addSubview:_leftBtn];
        [_leftBtn setImage:[UIImage imageNamed:@"btn_cart_normal_jian"] forState:UIControlStateNormal];
        [_leftBtn setImage:[UIImage imageNamed:@"btn_cart_press_jia"] forState:UIControlStateHighlighted];
        [_leftBtn addTarget:self action:@selector(btnClike:) forControlEvents:UIControlEventTouchUpInside];
        _leftBtn.tag=11;
    }
    return self;
}

#pragma mark ---点击加减按钮事件
-(void)btnClike:(id)sender
{
    UIButton *btn =(UIButton*)sender;
    if (_delegate && [_delegate respondsToSelector:@selector(selectedItemAtIndexPath:tag:)]) {
        [_delegate selectedItemAtIndexPath:self.indexPath  tag:btn.tag];
    }
}

-(void)retSubViewWithData:(NSDictionary *)dictionary
{
    self.productImage.image=[UIImage createImageWithImageUrlString:[NSString stringTransformObject:[dictionary objectForKey:@""]]];
    self.productName.text =[NSString stringTransformObject:[dictionary objectForKey:@""]];
    self.productMoney.text =[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dictionary objectForKey:@""]] floatValue]];
    self.saleNumber.text =[NSString stringWithFormat:@"月售:%@",[NSString stringTransformObject:[dictionary objectForKey:@""]]];
    
}

@end
