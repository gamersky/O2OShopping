//
//  Strore_CollectionReusableView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/5.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Strore_CollectionReusableView
 Created_Date： 20151105
 Created_People： GT
 Function_description：便利店Collection头部视图
 ***************************************/

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TeHuiButtonTag) {
    krecommendButtonTag = 100,//推荐
    klimitCountButtonTag = 101,//限量
    kyiYuanGouButtonTag  //一元购
};
@protocol Strore_CollectionReusableViewDelegate <NSObject>
- (void)selectedTeHuiTypeWithIndex:(TeHuiButtonTag)buttonTag;
- (void)selectedLoopImageIndex:(NSInteger)index;
@end
@interface Strore_CollectionReusableView : UICollectionReusableView
@property(nonatomic)TeHuiButtonTag *buttonTag;
@property(nonatomic, unsafe_unretained)id<Strore_CollectionReusableViewDelegate>delegate;

- (void)resetLoopScrollViewImages:(NSArray*)imageUrls;
- (void)resetKindInfo:(NSDictionary*)dataDic;
@end
