//
//  StoreNavigationTitleView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/22.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "StoreNavigationTitleView.h"
//untils
#import "UIColor+Hex.h"
#import "Global.h"
#define ktopSpace 8
#define khSpace 10
#define kleftSpace 15
@implementation StoreNavigationTitleView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:@"#c52720"];
        [self setupSubView];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setupSubView {
    UIImage *logoImage = [UIImage imageNamed:@"tab_bg_logo_left"];
    UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, ktopSpace , VIEW_WIDTH/3-10 , 28)];
    imageView.backgroundColor= [UIColor clearColor];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = logoImage;
    [self addSubview:imageView];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(CGRectGetMaxX(imageView.frame) + khSpace
                              ,CGRectGetMinY(imageView.frame)
                              , VIEW_WIDTH - CGRectGetWidth(imageView.bounds) - kleftSpace - khSpace
                              , 28);
    [button setTitle:@"天天都实惠" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor whiteColor];
    [button.layer setMasksToBounds:YES];
    [button.layer setCornerRadius:4];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
    [button setImage:[UIImage imageNamed:@"search_logo"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(beatSearchEvent) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
   
}
- (void)beatSearchEvent {
    if (_delegate && [_delegate respondsToSelector:@selector(beatNavigationTitleViewToSearchView)]) {
        [_delegate beatNavigationTitleViewToSearchView];
    }
}

@end
