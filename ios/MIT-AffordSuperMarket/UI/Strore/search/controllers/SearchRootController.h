//
//  SearchRootController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/24.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： SearchRootController
 Created_Date： 20151103
 Created_People： GT
 Function_description：搜索根视图
 ***************************************/
#import "BaseViewController.h"
typedef NS_ENUM(NSInteger, SearchType) {
    ksearchHistoryType = 0,//搜索历史页面
    ksearchResultType  //搜索结果页面
};
@interface SearchRootController : BaseViewController
@property(nonatomic)NSInteger selectedIndex;
@end
