//
//  TopicHotkeyView.m
//  Coding_iOS
//
//  Created by pan Shiyu on 15/7/15.
//  Copyright (c) 2015年 Coding. All rights reserved.
//

#import "TopicHotkeyView.h"
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#define kOther_Hotkey_Text_Color    @"#707070"
#define kOther_HotKey_Border_Color  @"#e1e1e1"

@interface TopicHotkeyView ()
@property (nonatomic,strong) NSMutableArray *keyDatalist;
@property (nonatomic,strong) NSMutableArray *keyViewlist;

@end

@implementation TopicHotkeyView

- (void)setHotkeys:(NSArray *)hotkeys {
    if (!_keyDatalist) {
        _keyDatalist = [@[] mutableCopy];
        _keyViewlist = [@[] mutableCopy];
    }
    
    for (UIView *subView in _keyViewlist) {
        [subView removeFromSuperview];
    }
    [_keyViewlist removeAllObjects];
    [_keyDatalist removeAllObjects];
    
    if ([hotkeys count] <= 0) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, VIEW_WIDTH, 0);
        return;
    }
    
    [_keyDatalist addObjectsFromArray:hotkeys];
    
    
    if(hotkeys.count) {
        
        CGFloat currentWidth = 10.0f;
        CGFloat currentHeight = 0.0f;
        CGSize currentSize = CGSizeZero;
        CGFloat maxWidth = VIEW_WIDTH - 20.0f;
        UIFont *hotkeyFont = [UIFont systemFontOfSize:12.0f];
        
        for (int i = 0; i < _keyDatalist.count; i++) {
            NSString *displayHotkey = [NSString stringWithFormat:@"     %@     ",[[hotkeys objectAtIndex:i] objectForKey:@"KEYWORD"]];
            UIButton *btnHotkey = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnHotkey setTag:i];
            [btnHotkey setTitle:displayHotkey forState:UIControlStateNormal];
            [btnHotkey.titleLabel setFont:hotkeyFont];
            btnHotkey.layer.borderWidth = 1.0f;
            btnHotkey.layer.cornerRadius = 15.0f;
            [btnHotkey addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
            [btnHotkey setTitleColor:[UIColor colorWithHexString:kOther_Hotkey_Text_Color] forState:UIControlStateNormal];
            btnHotkey.layer.borderColor = [[UIColor colorWithHexString:kOther_HotKey_Border_Color] CGColor];
            //实际字体size
            currentSize = [self computeSizeFromString:displayHotkey];
            if (currentSize.width >= maxWidth) {//大于屏幕预留宽度
                
                CGFloat height = ((int)currentSize.width % (int)maxWidth ? currentSize.width / maxWidth : currentSize.width / maxWidth + 1) * currentSize.height;
                btnHotkey.frame = CGRectMake(currentWidth, currentHeight, maxWidth, height);
                currentHeight = currentHeight + height + 10.0f;
                currentWidth = 10.0f;
            }else {
                
                if(currentSize.width + currentWidth < maxWidth) {//宽度小于一行
                    
                    btnHotkey.frame = CGRectMake(currentWidth, currentHeight, currentSize.width, currentSize.height);
                    currentWidth = currentWidth + currentSize.width + 5.0f;
                }else if (currentSize.width + currentWidth == maxWidth) {//刚好一行
                    
                    btnHotkey.frame = CGRectMake(currentWidth, currentHeight, currentSize.width, currentSize.height);
                    currentWidth = 10.0f;
                    currentHeight = currentHeight + currentSize.height + 10.0f;
                }else if(currentSize.width + currentWidth > maxWidth ) {//换行
                    
                    currentWidth = 10.0f;
                    currentHeight = currentHeight + currentSize.height + 10.0f;
                    btnHotkey.frame = CGRectMake(currentWidth, currentHeight, currentSize.width, currentSize.height);
                    currentWidth = currentWidth + currentSize.width + 5.0f;
                }
            }
            
            if(i == hotkeys.count - 1) {//视图最终高度
                
                currentHeight += currentSize.height + 14.0f;
            }
            
            [self addSubview:btnHotkey];
            
            [_keyViewlist addObject:btnHotkey];
        }
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, VIEW_WIDTH, currentHeight);
    }
}

- (CGSize)computeSizeFromString:(NSString *)string {
    
    CGSize maxSize = CGSizeMake(VIEW_WIDTH, 30);
    CGSize curSize = [string boundingRectWithSize:maxSize
                                          options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                       attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:12.0f] }
                                          context:nil].size;
    return CGSizeMake(ceilf(curSize.width), 30);
    
}

- (void)didClickButton:(id)sender {
    NSInteger index = [(UIButton *)sender tag];
    if (_block) {
        _block(_keyDatalist[index]);
    }
}

@end
