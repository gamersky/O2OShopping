//
//  StoreShowModel.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/10.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "StoreShowModel.h"
#define kleftSpace 15
#define ktopSpace 11
#import "Global.h"
@implementation StoreShowModel {
     CGFloat _cellHeight;
}
+ (instancetype)messageWithDic:(NSDictionary *)dic {
    StoreShowModel *message = [[self alloc] init];
    [message setValuesForKeysWithDictionary:dic];
    return message;
}

- (CGFloat)cellHeight {
    if (!_cellHeight) {
         CGFloat contentW = VIEW_WIDTH - 2 * kleftSpace; // 屏幕宽度减去左右间距
        CGFloat contentH = [self.content boundingRectWithSize:CGSizeMake(contentW, MAXFLOAT)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:FontSize(16)]}
                                                      context:nil].size.height;
        _cellHeight = 2*ktopSpace + contentH + 10;
    }
    return _cellHeight;
}
    
@end
