//
//  AddReceiveAddressController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "AddReceiveAddressController.h"
#define ktopSpace HightScalar(12)
#define kleftSpace 15
#define kVSpace 10
#define kitemHeight HightScalar(53)
//untils
#import "Global.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#import "NSString+TextSize.h"
#import "AppDelegate.h"
#import "RDVTabBarController.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
//model
#import "LouCengInfoModel.h"

//view
#import "CheckBoxView.h"
#import "LouCengPickerView.h"
//controllers
#import "SureOrderController.h"
#import "BaiDuLocationViewController.h"
#import "MTA.h"
@interface AddReceiveAddressController ()<UIGestureRecognizerDelegate, UITextFieldDelegate>
{
    NSInteger _sex;
    CheckBoxView *_sexCheckBoxView;
    
}
@property(nonatomic, strong)UITextField *userNameTextField;//用户名输入框
@property(nonatomic, strong)UITextField *userTelePhoneTextField;//用户联系电话输入框
@property(nonatomic, strong)UITextField *userAddressTextField;//用户详细地址
@property(nonatomic, strong)UIButton *selectXiaoQuBtn;//选择小区
@property(nonatomic, strong)UILabel *xiaoQuAddressLabel;//显示小区label
@property(nonatomic, strong)UIButton *detailAddressBtn;//选择楼号，单元，门牌号
@property(nonatomic, strong)UIButton *setDefaultAddressBtn;//是否默认地址
@property(nonatomic, strong)NSMutableArray *louCengInfoArr;//楼层信息名字和ID
@property(nonatomic, strong)NSString *userTel; //用户电话号码

@end

@implementation AddReceiveAddressController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    self.title = @"我的收货地址";
    _sex = 1;
    [self addBackNavItem];
    _louCengInfoArr = [[NSMutableArray alloc]init];
    UserInfoModel *model = [[ManagerGlobeUntil sharedManager] getUserInfo];
    _userTel = model.userName;
    [self setupSubView];
    
    //隐藏键盘手势
    UITapGestureRecognizer *tabGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    tabGesture.delegate = self;
    [self.view addGestureRecognizer:tabGesture];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSString *itemTitle = @"";
    if (self.supperControllerType == kUserCenterPushToAddReceiveAddressController) {
       itemTitle = @"保存";
    } else if (self.supperControllerType == kSureOrderPushToAddReceiveAddressController) {
       itemTitle = @"使用";
    }
    self.xiaoQuAddressLabel.text = [[ManagerGlobeUntil sharedManager] getUserInfo].xiaoquName;
    [self addRightNavItemWithTitle:itemTitle];

    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    
    if (self.editAddressType == kmodifyReceiveAddressType) {
        [self setupViewWithData];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"添加收货地址"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"添加收货地址"];
}
#pragma mark -- 请求之前条件判断
- (BOOL)isFitRequestCase {
    NSString *errorMsg = @"";
    if (self.userNameTextField.text.length == 0) {
        errorMsg = @"联系人姓名不能为空";
        [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
        return NO;
    } else if (self.userTelePhoneTextField.text.length == 0) {
        errorMsg = @"联系人电话不能为空";
        [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
        return NO;
    }else if(self.userAddressTextField.text.length == 0)
    {
        errorMsg = @"详细地址不能为空";
        [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
        return NO;
    }
//    } else if ([self.detailAddressBtn.titleLabel.text isEqualToString:@"请选择"]) {
//        errorMsg = @"请选择楼号-单元-门牌号";
//        [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
//        return NO;
//    }
    return YES;
}
#pragma mark -- request Data
//加载小区信息（单元，楼号，门牌号）
- (void)loadXiaoQuDetailInfoData {
  //    SIGNATURE			设备识别码
  //    COMMUNITYID			社区ID
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    XiaoQuAndStoreInfoModel *model = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:model.xiaoQuID forKey:@"COMMUNITYID"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"common/communityDetail" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                if ([[dataDic objectForKey:@"OBJECT"] count]!= 0) {
                    LouCengPickerView *pickView = [[LouCengPickerView alloc]initWithFrame:CGRectMake(0, weakSelf.view.bounds.size.height - HightScalar(272), weakSelf.view.bounds.size.width, HightScalar(272))
                                                                    withComponentContents:[[ManagerGlobeUntil sharedManager] modifiedServerXiaoQuInfoDatasoruce:[dataDic objectForKey:@"OBJECT"]]
                                                                                 delegate:(id<LouCengPickerViewDelegate>)weakSelf];
                    [pickView showInView:weakSelf.view];
                    weakSelf.detailAddressBtn.enabled = NO;
                }
                
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }

        }
    } failure:^(bool isFailure) {
        
    }];
}

//添加收货地址
- (void)loadAddReceivedAddressData {
    /*SIGNATURE:设备识别码  
     TOKEN:用户签名 
     LINKNAME:昵称 
     TEL:联系电话，座机或手机
     SEX:性别1：男，2：女
     COMMUNITYID:社区ID 
     BUILDID: 楼号ID
     UNITID:单元号ID
     ROOMID:房间门牌号ID
     ISDEFAULT:是否默认 1：是，0：否
     */
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
//    LouCengInfoModel *louNumberModel = [self.louCengInfoArr firstObject];
//    LouCengInfoModel *danYunNumberModel = [self.louCengInfoArr objectAtIndex:1];
//    LouCengInfoModel *roomNumberModel = [self.louCengInfoArr lastObject];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    XiaoQuAndStoreInfoModel *model = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    UserInfoModel *userModel =[[ManagerGlobeUntil sharedManager] getUserInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    
    [parameter setObject:userModel.token forKey:@"TOKEN"];
    [parameter setObject:serviceVersionNumber forKey:@"APIVERSION"];
    [parameter setObject:self.userNameTextField.text forKey:@"LINKNAME"];
    [parameter setObject:[NSNumber numberWithInteger:_sex] forKey:@"SEX"];
    [parameter setObject:self.userTelePhoneTextField.text forKey:@"TEL"];
    [parameter setObject:model.storeID forKey:@"STOREID"];
    [parameter setObject:self.userAddressTextField.text forKey:@"ADDRESS"];
//    [parameter setObject:louNumberModel.ID  forKey:@"BUILDID"];
//    [parameter setObject:danYunNumberModel.ID  forKey:@"UNITID"];
//    [parameter setObject:roomNumberModel.ID  forKey:@"ROOMID"];
     __weak typeof(self)weakSelf = self;
    [parameter setObject:self.setDefaultAddressBtn.selected ? [NSNumber numberWithInteger:1] : [NSNumber numberWithInteger:0] forKey:@"ISDEFAULT"];
    [manager parameters:parameter customPOST:@"user/addAddress" success:^(AFHTTPRequestOperation *operation, id responseObject) {
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                
                [weakSelf updateAddressData:[dataDic objectForKey:@"OBJECT"]];
                
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }

        }
        
    } failure:^(bool isFailure) {
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        
    }];
}
//修改收货地址
- (void)loadModifiedReceivedAddressData {
    /*SIGNATURE:设备识别码
     TOKEN:用户签名
     LINKNAME:昵称
     TEL:联系电话，座机或手机
     SEX:性别1：男，2：女
     COMMUNITYID:社区ID
     BUILDID: 楼号ID
     UNITID:单元号ID
     ROOMID:房间门牌号ID
     ISDEFAULT:是否默认 1：是，0：否
     */
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
     XiaoQuAndStoreInfoModel *model = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    UserInfoModel *userModel = [[ManagerGlobeUntil sharedManager] getUserInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
//    if (self.louCengInfoArr.count != 0) {//修改了门牌号
//        LouCengInfoModel *louNumberModel = [self.louCengInfoArr firstObject];
//        LouCengInfoModel *danYunNumberModel = [self.louCengInfoArr objectAtIndex:1];
//        LouCengInfoModel *roomNumberModel = [self.louCengInfoArr lastObject];
//        [parameter setObject:louNumberModel.ID  forKey:@"BUILDID"];
//        [parameter setObject:danYunNumberModel.ID  forKey:@"UNITID"];
//        [parameter setObject:roomNumberModel.ID  forKey:@"ROOMID"];
//    } else {//未修改门牌号
//        [parameter setObject:[NSNumber numberWithDouble:self.receivedAddressDatas.bUILDID]  forKey:@"BUILDID"];
//        [parameter setObject:[NSNumber numberWithDouble:self.receivedAddressDatas.uNITID]  forKey:@"UNITID"];
//        [parameter setObject:[NSNumber numberWithDouble:self.receivedAddressDatas.rOOMID]  forKey:@"ROOMID"];
//    }
    [parameter setObject:model.storeID forKey:@"STOREID"];
    [parameter setObject:userModel.token forKey:@"TOKEN"];
    [parameter setObject:[NSNumber numberWithDouble:self.receivedAddressDatas.iDProperty] forKey:@"ADDRESSID"];
    
    [parameter setObject:serviceVersionNumber forKey:@"APIVERSION"];
    [parameter setObject:[NSNumber numberWithInteger:_sex] forKey:@"SEX"];
    [parameter setObject:self.userNameTextField.text forKey:@"LINKNAME"];
    [parameter setObject:self.userTelePhoneTextField.text forKey:@"TEL"];
    [parameter setObject:self.userAddressTextField.text forKey:@"ADDRESS"];
    [parameter setObject:self.setDefaultAddressBtn.selected ? [NSNumber numberWithInteger:1] : [NSNumber numberWithInteger:0] forKey:@"ISDEFAULT"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"user/updateAddress" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
         if ([responseObject isKindOfClass:[NSDictionary class]]) {
             NSDictionary *dataDic = (NSDictionary*)responseObject;
             NSString *msg = [dataDic objectForKey:@"MSG"];
             NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
             if ([state isEqualToString:@"0"]) {//请求成功
                 [weakSelf updateAddressData:[dataDic objectForKey:@"OBJECT"]];
                 
             }else {//请求失败
                 [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
             }
         }
        
        
        
    } failure:^(bool isFailure) {
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        
    }];

}

//删除收货地址
- (void)loadRemoveReceivedAddressDatas{
    /*SIGNATURE:设备识别码
     TOKEN:用户签名
     ADDRESSID:收货地址ID
     */
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..."
                                              inView:self.view];
    ManagerHttpBase *manager = [ManagerHttpBase sharedManager];
    UserInfoModel *model = [[ManagerGlobeUntil sharedManager] getUserInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:model.token forKey:@"TOKEN"];
    [parameter setObject:[NSNumber numberWithDouble:self.receivedAddressDatas.iDProperty] forKey:@"ADDRESSID"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"user/delAddress" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            if ([state isEqualToString:@"0"]) {//请求成功
                [weakSelf.navigationController popViewControllerAnimated:YES];
                
            }else {//请求失败
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:msg inView:weakSelf.view];
            }

        }
        
    } failure:^(bool isFailure) {
        
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
    }];
}
- (void)updateAddressData:(NSDictionary*)dataDic {
    
    NSMutableDictionary *datadic = [[NSMutableDictionary alloc]initWithDictionary:dataDic];
    
//    [datadic setValue:self.xiaoQuAddressLabel.text forKey:@"COMMUNITYTITLE"];
    NSString * address =[NSString stringTransformObject:[dataDic objectForKey:@"ADDRESS"]];
    [datadic setObject:address forKey:@"ADDRESS"];
    ReceivedAddressOBJECT *addressObject = [[ReceivedAddressOBJECT alloc]initWithDictionary:[datadic copy]];
    [self requestSuccessWithAddressObject:addressObject];
}
- (void)requestSuccessWithAddressObject:(ReceivedAddressOBJECT*)addressObject {
    if (self.supperControllerType == kUserCenterPushToAddReceiveAddressController) {
        [self.navigationController popViewControllerAnimated:YES];
    } else if (self.supperControllerType == kSureOrderPushToAddReceiveAddressController) {
            for (UIViewController *VC in self.navigationController.childViewControllers ) {
                if ([VC isKindOfClass:[SureOrderController class]]) {
                    ((SureOrderController*)VC).addressData = addressObject;
                    [self.navigationController popToViewController:VC animated:YES];
                    return ;
                }
            }
        }
        
    }
#pragma mark -- CheckBoxViewDelegate
- (void)selectedItemWithIndex:(NSInteger)buttonTag withCheckBoxView:(CheckBoxView*)checkboxView {
    if (buttonTag == 0) {//男士
        _sex = 1;
    } else if (buttonTag == 1) {//女士
        _sex = 2;
    }
}
//#pragma mark -- LouCengPickerViewDelegate
//- (void)selectedLouNumberModel:(LouCengInfoModel*)louNumberModel danYuanNumberModel:(LouCengInfoModel*)danyuanNumberModel roomNumberModel:(LouCengInfoModel*)roomNumberModel {
//    self.detailAddressBtn.enabled = YES;
//    [self.louCengInfoArr addObject:louNumberModel];
//    [self.louCengInfoArr addObject:danyuanNumberModel];
//    [self.louCengInfoArr addObject:roomNumberModel];
//    NSString *louInfoStr = [NSString stringWithFormat:@"%@%@%@",louNumberModel.name,danyuanNumberModel.name,roomNumberModel.name];
//    [self setDetailAddressTitle:louInfoStr];
//    
//}
#pragma mark -- button Action
//选取小区
- (void)selectXiaoQuEvent {
    BaiDuLocationViewController*userLocation=[[BaiDuLocationViewController alloc]init];
    userLocation.suppperControllerType = kOther_ControllerSupperController;
    UINavigationController *naVc =[[UINavigationController alloc]initWithRootViewController:userLocation];
    [self presentViewController:naVc animated:YES completion:nil];
}
//选取楼号-单元-门牌号
- (void)selectDetailAddressEvent {
    [self loadXiaoQuDetailInfoData];
    
}
//是否设置为默认地址
- (void)setDefaultAddressEvent:(id)sender {
    UIButton *button = (UIButton*)sender;
    button.selected = !button.selected;
}

//删除用户地址
- (void)deleteReceiveAddress {
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        [self loadRemoveReceivedAddressDatas];
    }
    
}
//保存事件
- (void)rigthBtnAction:(id)sender {
    [self hideKeyboard];
    if (self.editAddressType == kaddReceiveAddressType) {//添加收货地址
        if ([self isFitRequestCase]) {
            if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
              [self loadAddReceivedAddressData];
            }
            
        }
    } else if (self.editAddressType == kmodifyReceiveAddressType) {//修改收货地址
        if ([self isFitRequestCase]) {
            if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
                [self loadModifiedReceivedAddressData];
            }
            
        }
    }
    
}
//隐藏键盘
- (void)hideKeyboard {
    if ([self.userNameTextField isFirstResponder])
    {
        [self.userNameTextField resignFirstResponder];
    }
    if ([self.userTelePhoneTextField isFirstResponder])
    {
        [self.userTelePhoneTextField resignFirstResponder];
    }
    if ([self.userAddressTextField isFirstResponder])
    {
        [self.userAddressTextField resignFirstResponder];
    }
    
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.userNameTextField.isFirstResponder || self.userTelePhoneTextField.isFirstResponder || self.userAddressTextField.isFirstResponder) {
        return YES;
    }
    return NO;
}

- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
#pragma mark setter method
- (void)setEditAddressType:(EditReceiveAddressType)editAddressType {
    NSString *title = @"";
    if (editAddressType == kaddReceiveAddressType) {
        title = @"新增收货地址";
    } else if (editAddressType == kmodifyReceiveAddressType) {
        title = @"修改收货地址";
    }
    _editAddressType = editAddressType;
}
- (void)setupViewWithData {
    self.userNameTextField.text = [NSString stringISNull:self.receivedAddressDatas.lINKNAME];
    _sexCheckBoxView.selectedItemIndex = (self.receivedAddressDatas.sEX == 1 ? 0: 1);
    _sex = self.receivedAddressDatas.sEX;
    self.userTelePhoneTextField.text = [NSString stringISNull:self.receivedAddressDatas.tEL];
//    self.xiaoQuAddressLabel.text = [NSString stringISNull:self.receivedAddressDatas.cOMMUNITYTITLE];
    NSString *address = [NSString stringWithFormat:@"%@",[NSString stringISNull:self.receivedAddressDatas.aDDRESS]];
    [self setDetailAddressTitle:address];
    self.setDefaultAddressBtn.selected = self.receivedAddressDatas.iSDEFAULT == 1 ? YES : NO;

}
#pragma mark -- 初始化子视图
- (void)setupSubView {
    UIView *firstView = [[UIView alloc]initWithFrame:CGRectMake(0, ktopSpace, CGRectGetWidth(self.view.bounds), HightScalar(272) - kitemHeight - 1)];
    firstView.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    [self.view addSubview:firstView];
    UIImage *sepImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"] frame:CGRectMake(0, 0, 2, 1)];
    NSString *usreNameTitle = @"联系人：";
    CGSize userNameTitleLabelSize = [usreNameTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(120, kitemHeight)];
    UILabel *userNameTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                     , 0
                                                                    , userNameTitleLabelSize.width
                                                                    , kitemHeight)];
    userNameTitleLabel.text = usreNameTitle;
    userNameTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    userNameTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    [firstView addSubview:userNameTitleLabel];
    
    _userNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(userNameTitleLabel.frame)
                                                                     , CGRectGetMinY(userNameTitleLabel.frame)
                                                                     , VIEW_WIDTH - 2*kleftSpace - CGRectGetWidth(userNameTitleLabel.bounds)
                                                                      , CGRectGetHeight(userNameTitleLabel.bounds))];
    _userNameTextField.font = [UIFont systemFontOfSize:FontSize(16)];
    _userNameTextField.placeholder = @"请输入联系人姓名";
    _userNameTextField.delegate = self;
    _userNameTextField.textColor = [UIColor colorWithHexString:@"555555"];
    [firstView addSubview:_userNameTextField];
    
    UIImageView *firstSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(0
                                                                            , CGRectGetMaxY(_userNameTextField.frame)
                                                                            , CGRectGetWidth(self.view.bounds) 
                                                                             , 1)];
    firstSepLine.image = sepImage;
    [firstView addSubview:firstSepLine];
    
    //性别
   _sexCheckBoxView = [[CheckBoxView alloc]initWithFrame:CGRectMake(CGRectGetMinX(firstSepLine.frame)
                                                                                          , CGRectGetMaxY(firstSepLine.frame)
                                                                                          , CGRectGetWidth(firstSepLine.frame)
                                                                                          , CGRectGetHeight(_userNameTextField.bounds))
                                                                   withiTitles:@[@"男士",@"女士"]
                                                                   normalImage:[UIImage imageNamed:@"checkBoxNormalImage"]
                                                                  slectedImage:[UIImage imageNamed:@"checkBoxSelectdImage"]];
    _sexCheckBoxView.delegate = (id<CheckBoxViewDelegate>)self;
    [firstView addSubview:_sexCheckBoxView];
    
    UIImageView *secondSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(firstSepLine.frame)
                                                                             , CGRectGetMaxY(_sexCheckBoxView.frame)
                                                                             , CGRectGetWidth(firstSepLine.bounds)
                                                                             , CGRectGetHeight(firstSepLine.bounds))];
    secondSepLine.image = sepImage;
    [firstView addSubview:secondSepLine];
    
    //电话
    NSString *userTelePhoneTitle = @"电话：";
    CGSize userTelePhoneTitleSize = [userTelePhoneTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(120, kitemHeight)];
    UILabel *userTelePhoneTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(userNameTitleLabel.frame)
                                                                           , CGRectGetMaxY(secondSepLine.frame)
                                                                           , userTelePhoneTitleSize.width
                                                                           , CGRectGetHeight(userNameTitleLabel.bounds))];
    userTelePhoneTitleLabel.text = userTelePhoneTitle;
    userTelePhoneTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    userTelePhoneTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    [firstView addSubview:userTelePhoneTitleLabel];
    
    _userTelePhoneTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(userNameTitleLabel.frame)
                                                                      , CGRectGetMinY(userTelePhoneTitleLabel.frame)
                                                                      , VIEW_WIDTH - 2*kleftSpace - CGRectGetWidth(userNameTitleLabel.bounds)
                                                                      , CGRectGetHeight(userTelePhoneTitleLabel.bounds))];
    _userTelePhoneTextField.font = [UIFont systemFontOfSize:FontSize(16)];
    _userTelePhoneTextField.placeholder = @"请输入联系人电话";
    _userTelePhoneTextField.text =self.userTel;
    _userTelePhoneTextField.delegate = self;
    _userTelePhoneTextField.textColor = [UIColor colorWithHexString:@"555555"];
    [firstView addSubview:_userTelePhoneTextField];
    
    UIImageView *thridSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(secondSepLine.frame)
                                                                             , CGRectGetMaxY(_userTelePhoneTextField.frame)
                                                                             , CGRectGetWidth(secondSepLine.bounds)
                                                                             , CGRectGetHeight(secondSepLine.bounds))];
    thridSepLine.image = sepImage;
    [firstView addSubview:thridSepLine];
    
//    //小区
//    NSString *xiaoquTitle = @"小区：";
//    CGSize xiaoquTitleSize = [xiaoquTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(120, kitemHeight)];
//    UILabel *xiaoquTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(userTelePhoneTitleLabel.frame)
//                                                                                , CGRectGetMaxY(thridSepLine.frame)
//                                                                                , xiaoquTitleSize.width
//                                                                                , CGRectGetHeight(userTelePhoneTitleLabel.bounds))];
//    xiaoquTitleLabel.text = xiaoquTitle;
//    xiaoquTitleLabel.font = [UIFont systemFontOfSize:15];
//    xiaoquTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
//    [firstView addSubview:xiaoquTitleLabel];
//    UIImage *dingWeiImage = [UIImage imageNamed:@"dingweiImage"];
//    UIImageView *dingWeiImageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(xiaoquTitleLabel.frame) + 8
//                                                                                ,(CGRectGetHeight(xiaoquTitleLabel.bounds) - dingWeiImage.size.height)/2 + CGRectGetMinY(xiaoquTitleLabel.frame)
//                                                                                , dingWeiImage.size.width
//                                                                                 , dingWeiImage.size.height)];
//    dingWeiImageView.image = dingWeiImage;
//    [firstView addSubview:dingWeiImageView];
//    
//    _xiaoQuAddressLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(dingWeiImageView.frame) + 8
//                                                                  , CGRectGetMinY(xiaoquTitleLabel.frame)
//                                                                  , CGRectGetWidth(self.view.bounds) - 2*kleftSpace - xiaoquTitleSize.width - 8*2 - 44 - CGRectGetWidth(dingWeiImageView.bounds)
//                                                                   , CGRectGetHeight(xiaoquTitleLabel.bounds))];
//    
//   // _xiaoQuAddressLabel.attributedText = [self dealWithLableFirstFontSize:15 secondFontSize:13 firstTextColor:@"#555555" secondTextColor:@"#a9a9a9" firstRang:NSMakeRange(0, model.xiaoquName.length) content:model.xiaoquName];
//    _xiaoQuAddressLabel.textAlignment = NSTextAlignmentLeft;
//    _xiaoQuAddressLabel.textColor = [UIColor colorWithHexString:@"#555555"];
//    _xiaoQuAddressLabel.font = [UIFont systemFontOfSize:15];
//    [firstView addSubview:_xiaoQuAddressLabel];
//    if (self.supperControllerType == kUserCenterPushToAddReceiveAddressController) {//只有在个人中心跳过来的，才可以修改小区
//        _selectXiaoQuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_selectXiaoQuBtn addTarget:self action:@selector(selectXiaoQuEvent) forControlEvents:UIControlEventTouchUpInside];
//        [_selectXiaoQuBtn setImage:[UIImage imageNamed:@"changeXiaoQuImage"] forState:UIControlStateNormal];
//        _selectXiaoQuBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//        _selectXiaoQuBtn.frame = CGRectMake(CGRectGetWidth(self.view.bounds) - kleftSpace - 44
//                                            , CGRectGetMinY(_xiaoQuAddressLabel.frame)
//                                            , 44
//                                            , CGRectGetHeight(_xiaoQuAddressLabel.bounds));
//        [firstView addSubview:_selectXiaoQuBtn];
//    }
    
    
//    UIImageView *fourthSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(firstSepLine.frame)
//                                                                              , CGRectGetMaxY(userTelePhoneTitleLabel.frame)
//                                                                              , CGRectGetWidth(firstSepLine.bounds)
//                                                                              , CGRectGetHeight(firstSepLine.bounds))];
//    fourthSepLine.image = sepImage;
//    [firstView addSubview:fourthSepLine];
    //详细地址
    NSString *userAddressTitle = @"详细地址：";
    CGSize userAddressTitleSize = [userAddressTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(120, kitemHeight)];
    UILabel *userAddressTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(userNameTitleLabel.frame)
                                                                                , CGRectGetMaxY(thridSepLine.frame)
                                                                                , userAddressTitleSize.width
                                                                                , CGRectGetHeight(userNameTitleLabel.bounds))];
    userAddressTitleLabel.text = userAddressTitle;
    userAddressTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    userAddressTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    [firstView addSubview:userAddressTitleLabel];
    
    _userAddressTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(userAddressTitleLabel.frame)
                                                                           , CGRectGetMinY(userAddressTitleLabel.frame)
                                                                           , VIEW_WIDTH - 2*kleftSpace - CGRectGetWidth(userAddressTitleLabel.bounds)
                                                                           , CGRectGetHeight(userAddressTitleLabel.bounds))];
    _userAddressTextField.font = [UIFont systemFontOfSize:FontSize(16)];
    _userAddressTextField.placeholder = @"街道/小区/门牌号等详细信息";
//    _userAddressTextField.text =self.userTel;
    _userAddressTextField.delegate = self;
    _userAddressTextField.textColor = [UIColor colorWithHexString:@"555555"];
    [firstView addSubview:_userAddressTextField];
    
//    //楼号-单元-门牌号
//    NSString *detailAddressTitle = @"楼号-单元-门牌号：";
//    CGSize detailAddressTitleSize = [detailAddressTitle sizeWithFont:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(160, kitemHeight)];
//    UILabel *detailAddressTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(xiaoquTitleLabel.frame)
//                                                                               , CGRectGetMaxY(fourthSepLine.frame)
//                                                                               , detailAddressTitleSize.width
//                                                                                , CGRectGetHeight(xiaoquTitleLabel.bounds))];
//    detailAddressTitleLabel.text = detailAddressTitle;
//    detailAddressTitleLabel.font = [UIFont systemFontOfSize:15];
//    detailAddressTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
//    detailAddressTitleLabel.textAlignment = NSTextAlignmentLeft;
//    [firstView addSubview:detailAddressTitleLabel];
//    
//    _detailAddressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_detailAddressBtn addTarget:self action:@selector(selectDetailAddressEvent) forControlEvents:UIControlEventTouchUpInside];
//    _detailAddressBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//   
//    _detailAddressBtn.titleLabel.font = [UIFont systemFontOfSize:15];
//    [_detailAddressBtn setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
//    _detailAddressBtn.frame = CGRectMake(CGRectGetMaxX(detailAddressTitleLabel.frame)
//                                        , CGRectGetMinY(detailAddressTitleLabel.frame)
//                                        , CGRectGetWidth(self.view.bounds) - 2*kleftSpace - detailAddressTitleSize.width
//                                        , CGRectGetHeight(detailAddressTitleLabel.bounds));
//    [firstView addSubview:_detailAddressBtn];
//    [self setDetailAddressTitle:@"请选择"];
    
    //是否设置默认地址
    
    UIView *secondView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMinX(firstView.frame)
                                                                , CGRectGetMaxY(firstView.frame) + ktopSpace
                                                                , CGRectGetWidth(firstView.bounds)
                                                                 , HightScalar(54))];
    secondView.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
    [self.view addSubview:secondView];
    
    NSString *setDefaultAddressTitle = @"设为默认收货地址";
    UILabel *setDefaultAddressTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                                   , 0
                                                                                   , CGRectGetWidth(secondView.bounds) - 2*kleftSpace - 44
                                                                                    , CGRectGetHeight(secondView.bounds))];
    setDefaultAddressTitleLabel.textAlignment = NSTextAlignmentLeft;
    setDefaultAddressTitleLabel.text = setDefaultAddressTitle;
    setDefaultAddressTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    setDefaultAddressTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    setDefaultAddressTitleLabel.textAlignment = NSTextAlignmentLeft;
    [secondView addSubview:setDefaultAddressTitleLabel];
    
    _setDefaultAddressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_setDefaultAddressBtn addTarget:self action:@selector(setDefaultAddressEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    _setDefaultAddressBtn.titleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    [_setDefaultAddressBtn setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
    _setDefaultAddressBtn.frame = CGRectMake(CGRectGetWidth(secondView.bounds)  - 44
                                         , CGRectGetMinY(setDefaultAddressTitleLabel.frame)
                                         , 44
                                         , 44);
    [_setDefaultAddressBtn setImage:[UIImage imageNamed:@"checkBoxNormalImage"] forState:UIControlStateNormal];
    [_setDefaultAddressBtn setImage:[UIImage imageNamed:@"checkBoxSelectdImage"] forState:UIControlStateSelected];
    [secondView addSubview:_setDefaultAddressBtn];
    
    //删除按钮
    if (self.editAddressType == kmodifyReceiveAddressType ) {
        UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteBtn addTarget:self action:@selector(deleteReceiveAddress) forControlEvents:UIControlEventTouchUpInside];
        [deleteBtn.layer setMasksToBounds:YES];
        [deleteBtn.layer setCornerRadius:4];
        deleteBtn.titleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
        [deleteBtn setTitle:@"删除收货地址" forState:UIControlStateNormal];
        deleteBtn.frame = CGRectMake(kleftSpace
                                     , CGRectGetMaxY(secondView.frame) + 30
                                     , CGRectGetWidth(self.view.bounds) - 2*kleftSpace
                                     , HightScalar(48));
        UIImage *loginImage = [UIImage imageNamed:@"login_Btn"];
        loginImage = [loginImage stretchableImageWithLeftCapWidth:6 topCapHeight:6];
        [deleteBtn setBackgroundImage:loginImage forState:UIControlStateNormal];;
        [self.view addSubview:deleteBtn];
    }
    
    
}

- (void)setDetailAddressTitle:(NSString*)title {

    self.userAddressTextField.text = title;
    
}

- (NSMutableAttributedString *)dealWithLableFirstFontSize:(CGFloat)firstFontSize secondFontSize:(CGFloat)secondFontSize firstTextColor:(NSString*)firstTextColor secondTextColor:(NSString*)secondTextColor firstRang:(NSRange)range content:(NSString*)content{
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:content];
    //设置：在0-3个单位长度内的内容显示成红色
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:firstTextColor] range:range];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:secondTextColor] range:NSMakeRange(range.length, content.length -range.length)];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:firstFontSize] range:range];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:secondFontSize] range:NSMakeRange(range.length, content.length - range.length)];
    return str;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
