//
//  MyMarkAndHistory_Cell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/17.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： TitleRImageMoreCell
 Created_Date： 20151118
 Created_People： GT
 Function_description： 我的账号里面更换头像cell
 ***************************************/


#define kCellIdentifier_TitleRImageMore @"TitleRImageMoreCell"

#import <UIKit/UIKit.h>

@interface TitleRImageMoreCell : UITableViewCell
@property(nonatomic, strong)UIImage *userImage;
@property(nonatomic, strong)NSString *title;

@end
