//
//  MyMarkAndHistory_Cell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/17.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyMarkAndHistory_Cell
 Created_Date： 20151118
 Created_People： GT
 Function_description： 我的关注和浏览历史cell
 ***************************************/

#import <UIKit/UIKit.h>

@interface MyMarkAndHistory_Cell : UITableViewCell
- (void)updataWithDataDic:(NSDictionary*)dataDic;
- (void)updataCollectionDataDic:(NSDictionary*)dataDic;
@end
