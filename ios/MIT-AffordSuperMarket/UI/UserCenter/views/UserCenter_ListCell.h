//
//  UserCenter_ListCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/9.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： UserCenter_ListCell
 Created_Date： 20151108
 Created_People： GT
 Function_description： 个人中心列表视图
 ***************************************/

#import <UIKit/UIKit.h>

@interface UserCenter_ListCell : UITableViewCell
@property (nonatomic,strong)UILabel *contentTextLabel;//标题
@property (nonatomic,strong)UIImageView *iconImgView;//图片
@property (nonatomic,strong)UIImageView *backImgView;//背景图片
@property (nonatomic,strong)UIImageView *lineImgView;//分割线
@property (nonatomic,strong)UIImageView *jiantouImgView;//指示箭头
@end
