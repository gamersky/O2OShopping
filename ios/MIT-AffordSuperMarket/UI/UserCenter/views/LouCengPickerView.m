//
//  LouCengPickerView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "LouCengPickerView.h"
//untils
#import "UIImage+ColorToImage.h"
#import "UIColor+Hex.h"
//model
#import "LouCengInfoModel.h"
@interface LouCengPickerView ()<UIPickerViewDelegate, UIPickerViewDataSource> {
    LouCengInfoModel *_louNumModel;
    LouCengInfoModel *_danyuanNumModel;
    LouCengInfoModel *_roomNumModel;
}
@property(nonatomic, strong)UIPickerView *pickView;
@property(nonatomic, strong)UIView *headerView;
@property(nonatomic, strong)UIButton *footView;
@property(nonatomic, strong)NSMutableArray *conponentArr;//picker数据源
@property(nonatomic, strong)NSMutableArray *louNumberArr;
@property(nonatomic, strong)NSMutableArray *danYuanNumberArr;
@property(nonatomic, strong)NSMutableArray *roomNumberArr;
@end
@implementation LouCengPickerView
- (id)initWithFrame:(CGRect)frame withComponentContents:(NSArray*)conponentContents delegate:(id<LouCengPickerViewDelegate>)delegate {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
        _conponentArr = [[NSMutableArray alloc]init];
        _danYuanNumberArr = [[NSMutableArray alloc]init];
        _roomNumberArr = [[NSMutableArray alloc]init];
        self.delegate = delegate;
        [self setupSubViewWithComponentContents:conponentContents];
        
        
    }
    return self;
}

#pragma mark -- 初始化视图
- (void)setupSubViewWithComponentContents:(NSArray*)conponentContents {
    [self.conponentArr addObjectsFromArray:conponentContents];
    [self.danYuanNumberArr addObjectsFromArray:[[conponentContents objectAtIndex:1] firstObject]];
    [self.roomNumberArr addObjectsFromArray:[[[conponentContents lastObject] firstObject] firstObject]];
    [self addSubview:self.headerView];
    [self addSubview:self.pickView];
    [self addSubview:self.footView];
}

#pragma mark - PickerView lifecycle

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return self.conponentArr.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return [[self.conponentArr firstObject] count];
            break;
        case 1:
            return self.danYuanNumberArr.count;
            break;
        case 2:
            return self.roomNumberArr.count;
            break;
        default:
            return 0;
            break;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 0:
        {
            LouCengInfoModel *model = [[self.conponentArr firstObject] objectAtIndex:row];
            _louNumModel = model;
            return model.name;
        }
            break;
        case 1:
        {
             LouCengInfoModel *model = [self.danYuanNumberArr  objectAtIndex:row];
            _danyuanNumModel =  model;
            return model.name;
        }
            break;
        case 2:
        {
            LouCengInfoModel *model = [self.roomNumberArr objectAtIndex:row];
            _roomNumModel = model;
            return model.name;
        }
            break;
        default:
            break;
    }
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
            switch (component) {
                case 0:
                {   [self.danYuanNumberArr removeAllObjects];
                    [self.danYuanNumberArr addObjectsFromArray:[[self.conponentArr objectAtIndex:1] objectAtIndex:row]];
                    
                     NSInteger selectedSecondCommentRow = [pickerView selectedRowInComponent:1];
                    [self.roomNumberArr removeAllObjects];
                    [self.roomNumberArr addObjectsFromArray:[[[self.conponentArr lastObject] objectAtIndex:row] objectAtIndex:selectedSecondCommentRow]];
                    [pickerView reloadAllComponents];
                }
                    break;
                case 1:
                {
                    [self.roomNumberArr removeAllObjects];
                    NSInteger selectedFirstCommentRow = [pickerView selectedRowInComponent:0];
                    [self.roomNumberArr addObjectsFromArray:[[[self.conponentArr lastObject] objectAtIndex:selectedFirstCommentRow] objectAtIndex:row]];
                    [pickerView reloadAllComponents];
                }
                    break;
                case 2:
                {
                    [self.roomNumberArr removeAllObjects];
                    NSInteger selectedFirstCommentRow = [pickerView selectedRowInComponent:0];
                    NSInteger selectedSecondCommentRow = [pickerView selectedRowInComponent:1];
                    [self.roomNumberArr addObjectsFromArray:[[[self.conponentArr lastObject] objectAtIndex:selectedFirstCommentRow] objectAtIndex:selectedSecondCommentRow]];
                    [pickerView reloadAllComponents];
                }
                    break;
                default:
                    break;
            }
    
}


- (UIPickerView*)pickView {
    if (!_pickView) {
        CGRect rect = CGRectMake(0
                                 , 44
                                 , CGRectGetWidth(self.bounds)
                                 , CGRectGetHeight(self.bounds) - 44*2);
        _pickView = [[UIPickerView alloc]initWithFrame:rect];
        _pickView.delegate = self;
        _pickView.backgroundColor = [UIColor colorWithHexString:@"#ffffff"];
        _pickView.dataSource = self;
        
    }
    return _pickView;
}

- (UIView*)headerView {
    if (!_headerView) {
        _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), 44)];
        _headerView.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
        CGFloat width = CGRectGetWidth(self.bounds)/self.conponentArr.count;
        NSArray *titles = @[@"楼号",@"单元",@"门牌号"];
        for (NSInteger i = 0; i < [self.conponentArr count]; i++) {
            
            UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(width*i, 0, width, CGRectGetHeight(_headerView.bounds))];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.text = [titles objectAtIndex:i];
            [_headerView addSubview:titleLabel];
            
        }
        
    }
    return _headerView;
}

- (UIButton*)footView {
    if (!_footView) {
        UIImage *bgImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#efefef"]
                                                   frame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), 44)];
        _footView = [UIButton buttonWithType:UIButtonTypeCustom];
        _footView.backgroundColor = [UIColor grayColor];
        _footView.frame = CGRectMake(0, CGRectGetHeight(self.bounds) - 44
                                     , CGRectGetWidth(self.bounds)
                                     , 44);
        [_footView setBackgroundImage:bgImage forState:UIControlStateNormal];
        [_footView setTitleColor:[UIColor colorWithHexString:@"#555555"] forState:UIControlStateNormal];
        [_footView setTitle:@"完成" forState:UIControlStateNormal];
        [_footView addTarget:self action:@selector(finishSelected) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _footView;
}
#pragma mark -- 控制视图显示与隐藏
- (void)showInView:(UIView *)view {
    self.frame = CGRectMake(0, view.frame.size.height, self.frame.size.width, self.frame.size.height);
    [view addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0, view.frame.size.height - self.frame.size.height, self.frame.size.width, self.frame.size.height);
    }];
    
}

- (void)cancelPicker {
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.frame = CGRectMake(0, self.frame.origin.y+self.frame.size.height, self.frame.size.width, self.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                         
                     }];
}

#pragma mark -- button Action
- (void)finishSelected {
    [self cancelPicker];
    if (_delegate && [_delegate respondsToSelector:@selector(selectedLouNumberModel:danYuanNumberModel:roomNumberModel:)]) {
        [_delegate selectedLouNumberModel:_louNumModel danYuanNumberModel:_danyuanNumModel roomNumberModel:_roomNumModel];
    }
}
@end
