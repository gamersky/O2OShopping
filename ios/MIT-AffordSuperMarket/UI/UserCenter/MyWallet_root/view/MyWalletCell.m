//
//  MyWalletCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/1.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#define kLeftSpace 15
#define ktopSpace 10
#import "MyWalletCell.h"

#import "Global.h"
#import "NSString+TextSize.h"
#import "UIColor+Hex.h"
@interface MyWalletCell()
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *badyLable;
@property(nonatomic,strong)UILabel *shiHuiBiLabel;
@end
@implementation MyWalletCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSubView];
    }
    return self;
}
-(void)setSubViewWithTitleText:(NSString *)titleText BadyText:(NSString *)badyText ShiHuiBiText:(NSString *)shiHuiBiText{
    
    CGSize titelSize =[titleText sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(VIEW_WIDTH-30, HightScalar(30))];
    _titleLabel.frame =CGRectMake(kLeftSpace,15, titelSize.width, titelSize.height);
    _titleLabel.text =titleText;
  
    CGSize badySize =[badyText sizeWithFont:[UIFont systemFontOfSize:FontSize(15)] maxSize:CGSizeMake(VIEW_WIDTH-30, HightScalar(30))];
    _badyLable.frame =CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.titleLabel.frame)+5, badySize.width, badySize.height);
    _badyLable.text =badyText;
    
    NSString *shihuibi =[NSString stringWithFormat:@"+%@ 实惠币",shiHuiBiText];
     CGSize shiHuiBiSize =[shihuibi sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(VIEW_WIDTH-30, HightScalar(30))];
    _shiHuiBiLabel.frame = CGRectMake(VIEW_WIDTH-kLeftSpace-shiHuiBiSize.width, (HightScalar(90)-shiHuiBiSize.height)/2, shiHuiBiSize.width, shiHuiBiSize.height);
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:shihuibi];
     [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize(16)] range:NSMakeRange(0, shiHuiBiText.length+1)];
    _shiHuiBiLabel.attributedText = str;

    
}
#pragma mark - 初始化视图
-(void)setSubView
{
    //标题
    _titleLabel =[[UILabel alloc]initWithFrame:CGRectZero];
    _titleLabel.font =[UIFont systemFontOfSize:FontSize(16)];
    _titleLabel.textColor =[UIColor colorWithHexString:@"#555555"];
    [self.contentView addSubview:self.titleLabel];
    
    
    _badyLable =[[UILabel alloc]initWithFrame:CGRectZero];
    _badyLable.font =[UIFont systemFontOfSize:FontSize(15)];
    _badyLable.textColor =[UIColor colorWithHexString:@"#999999"];
    _badyLable.numberOfLines =0;
    [self.contentView addSubview:self.badyLable];

    _shiHuiBiLabel =[[UILabel alloc]initWithFrame:CGRectZero];
    _shiHuiBiLabel.font =[UIFont systemFontOfSize:FontSize(14)];
//    _shiHuiBiLabel.textColor =[UIColor colorWithHexString:@"#e63f17"];
    _shiHuiBiLabel.textColor =[UIColor redColor];
    _shiHuiBiLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.shiHuiBiLabel];
   
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
