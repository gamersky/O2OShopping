//
//  MyWalletHeadView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/1.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MyWalletHeadViewDelegate <NSObject>
- (void)shiHuiBiBackGroundBtnJump;

- (void)firendBackGroundbtnJump;
@end
@interface MyWalletHeadView : UIView

-(void)setSubViewDataWithMoneyMun:(NSString *)money FriendNum:(NSString *)friendNum;
@property (nonatomic,assign)id<MyWalletHeadViewDelegate>delegate;
@end
