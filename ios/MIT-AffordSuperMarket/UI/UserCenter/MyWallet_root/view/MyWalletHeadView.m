//
//  MyWalletHeadView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/1.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyWalletHeadView.h"

#import "UIColor+Hex.h"
#import "NSString+TextSize.h"
#import "UIImage+ColorToImage.h"
#import "Global.h"
@interface MyWalletHeadView()
@property (nonatomic, strong)UILabel *shiHuiMoney;//实惠币数量
@property (nonatomic, strong)UILabel *myShiHuiBi;//我的实惠币
@property (nonatomic, strong)UIButton *shiHuiBiBackGroundBtn;//实惠币背景按钮
@property (nonatomic, strong)UILabel *firendNum;//好友数量
@property (nonatomic, strong)UILabel *recommendedFriend;//推荐的好友
@property (nonatomic, strong)UIButton *firendBackGroundBtn;//推荐好友背景按钮
@end
@implementation MyWalletHeadView
-(instancetype)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if(self){
        [self setSubViewinitWithFrame:frame];
        self.backgroundColor =[UIColor colorWithHexString:@"#dddddd"];
    }
    return self;
}
-(void)shiHuiBiBackGroundBtnClick
{
    if(_delegate &&  [_delegate respondsToSelector:@selector(shiHuiBiBackGroundBtnJump)]){
        [_delegate shiHuiBiBackGroundBtnJump];
    }
}
-(void)firendBackGroundbtnClick
{
    if(_delegate &&  [_delegate respondsToSelector:@selector(firendBackGroundbtnJump)]){
        [_delegate firendBackGroundbtnJump];
    }
}
-(void)setSubViewDataWithMoneyMun:(NSString *)money FriendNum:(NSString *)friendNum
{
    //金额
    money = [NSString stringWithFormat:@"%.2f",[money floatValue]];
    _shiHuiMoney.frame = CGRectMake(0, 25, CGRectGetWidth(self.shiHuiBiBackGroundBtn.bounds),  HightScalar(30));
    _shiHuiMoney.text = money;
    //我的实惠币
    NSMutableAttributedString *myShiHuiBiText = [[NSMutableAttributedString alloc] initWithString:@"我的实惠币"];
    NSRange myShiHuiBiRange = {0, [myShiHuiBiText length]};
    [myShiHuiBiText addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:myShiHuiBiRange];
    _myShiHuiBi.attributedText = myShiHuiBiText;
    _myShiHuiBi.frame =CGRectMake(CGRectGetMinX(self.shiHuiMoney.frame), CGRectGetMaxY(self.shiHuiMoney.frame)+5, CGRectGetWidth(self.shiHuiMoney.bounds), CGRectGetHeight(self.shiHuiMoney.bounds));

    //好友数量
    _firendNum.frame = CGRectMake(0, 25, CGRectGetWidth(self.shiHuiBiBackGroundBtn.bounds),  HightScalar(30));
    _firendNum.text = friendNum;
    //推荐的好友
    NSMutableAttributedString *recommendedFriendText = [[NSMutableAttributedString alloc] initWithString:@"推荐的好友"];
    NSRange recommendedFriendRange = {0, [recommendedFriendText length]};
    [recommendedFriendText addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:recommendedFriendRange];
    _recommendedFriend.attributedText = recommendedFriendText;
    _recommendedFriend.frame =CGRectMake(CGRectGetMinX(self.firendNum.frame), CGRectGetMaxY(self.firendNum.frame)+5, CGRectGetWidth(self.firendNum.bounds), CGRectGetHeight(self.firendNum.bounds));

}
#pragma mark --  初始化视图
-(void)setSubViewinitWithFrame:(CGRect)frame
{
    //实惠币背景按钮
    _shiHuiBiBackGroundBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _shiHuiBiBackGroundBtn.frame = CGRectMake(0, 0, VIEW_WIDTH/2, frame.size.height-HightScalar(12));
    _shiHuiBiBackGroundBtn.backgroundColor = [UIColor colorWithHexString:@"#343333"];
    [_shiHuiBiBackGroundBtn addTarget:self action:@selector(shiHuiBiBackGroundBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_shiHuiBiBackGroundBtn];
    //实惠币金额
    _shiHuiMoney = [[UILabel alloc]initWithFrame:CGRectZero];
    _shiHuiMoney.font = [UIFont systemFontOfSize:FontSize(30)];
    _shiHuiMoney.textColor = [UIColor whiteColor];
    _shiHuiMoney.text=@"0";
    [_shiHuiBiBackGroundBtn addSubview:_shiHuiMoney];
    _shiHuiMoney.textAlignment = NSTextAlignmentCenter;
    //我的实惠币
    _myShiHuiBi = [[UILabel alloc]initWithFrame:CGRectZero];
    _myShiHuiBi.font = [UIFont systemFontOfSize:FontSize(16)];
    _myShiHuiBi.textColor = [UIColor whiteColor];
    _myShiHuiBi.textAlignment = NSTextAlignmentCenter;
    [_shiHuiBiBackGroundBtn addSubview:_myShiHuiBi];

    //分割线
    UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_shiHuiBiBackGroundBtn.frame)-1,(frame.size.height-HightScalar(12))/4 , 1,(frame.size.height-HightScalar(12))/2)];
    imageView.image =[UIImage createImageWithColor:[UIColor colorWithHexString:@"#000000"] frame:CGRectMake(0, 0, 0.7, frame.size.height/2)];
    [_shiHuiBiBackGroundBtn addSubview:imageView];

    //好友背景按钮
    _firendBackGroundBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _firendBackGroundBtn.frame = CGRectMake(CGRectGetMaxX(_shiHuiBiBackGroundBtn.frame), 0, VIEW_WIDTH/2, frame.size.height-HightScalar(12));
    _firendBackGroundBtn.backgroundColor = [UIColor colorWithHexString:@"#343333"];
     [_firendBackGroundBtn addTarget:self action:@selector(firendBackGroundbtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_firendBackGroundBtn];
    //实惠币金额
    _firendNum = [[UILabel alloc]initWithFrame:CGRectZero];
    _firendNum.font = [UIFont systemFontOfSize:FontSize(30)];
    _firendNum.textColor = [UIColor whiteColor];
    _firendNum.text=@"0";
    _firendNum.textAlignment = NSTextAlignmentCenter;
    [_firendBackGroundBtn addSubview:_firendNum];
    //我的实惠币
    _recommendedFriend = [[UILabel alloc]initWithFrame:CGRectZero];
    _recommendedFriend.font = [UIFont systemFontOfSize:FontSize(16)];
    _recommendedFriend.textColor = [UIColor whiteColor];
    _recommendedFriend.textAlignment = NSTextAlignmentCenter;
    [_firendBackGroundBtn addSubview:_recommendedFriend];

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
