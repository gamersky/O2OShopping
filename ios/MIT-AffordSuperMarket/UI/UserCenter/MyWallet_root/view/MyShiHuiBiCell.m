//
//  MyShiHuiBiCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/4.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
#define kLeftSpace 15
#import "MyShiHuiBiCell.h"

#import "Global.h"
#import "NSString+TextSize.h"
#import "UIColor+Hex.h"
@interface MyShiHuiBiCell()
@property(nonatomic,strong)UILabel *timeLabel;
@property(nonatomic,strong)UILabel *moenyLable;
@end
@implementation MyShiHuiBiCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSubView];
    }
    return self;
}
-(void)setSubViewWithTimeText:(NSString *)timeText moneyText:(NSString *)moneyText
{
    _timeLabel.text =timeText;
    
    NSString *shihuibi =[NSString stringWithFormat:@"-%@ 实惠币",moneyText];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:shihuibi];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize(16)] range:NSMakeRange(0, moneyText.length+1)];
    _moenyLable.attributedText = str;
}
#pragma mark - 初始化视图
-(void)setSubView
{
    //标题
    _timeLabel =[[UILabel alloc]initWithFrame:CGRectMake(kLeftSpace,0, VIEW_WIDTH-2*kLeftSpace,HightScalar(67))];
    _timeLabel.font =[UIFont systemFontOfSize:FontSize(16)];
    _timeLabel.textColor =[UIColor colorWithHexString:@"#999999"];
    _timeLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.timeLabel];
    
    
    _moenyLable =[[UILabel alloc]initWithFrame:CGRectMake(kLeftSpace,0, VIEW_WIDTH-2*kLeftSpace,HightScalar(67))];
    _moenyLable.font =[UIFont systemFontOfSize:FontSize(16)];
    _moenyLable.textColor =[UIColor colorWithHexString:@"#999999"];
    _moenyLable.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.moenyLable];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
