//
//  MyWalletViewModel.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/4.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyWalletViewModel.h"
//untils
#import "Global.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
#import "UIImage+ColorToImage.h"

@interface MyWalletViewModel ()
@end
@implementation MyWalletViewModel
- (id)init {
    self = [super init];
    if (self) {
        [self resetDatas];
    }
    return self;
}

- (void)resetDatas {
   self.shiHuiBiMoney = @"";
   self.recommendFriend = @"";
   self.searchType = @"";
   self.totalCount = 0;
}
-(void)requestMyWalletDetailDatasuccess:(void (^)(NSString *))success failure:(void (^)(NSString *))failureMessage
{
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    UserInfoModel *model = [[ManagerGlobeUntil sharedManager] getUserInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:model.token forKey:@"TOKEN"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"user/money" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            NSString *msg = [dataDic objectForKey:@"MSG"];
            if ([state isEqualToString:@"0"]) {//请求成功
                if ([[dataDic objectForKey:@"OBJECT"] count] != 0) {
                [weakSelf handelMyWalletData:[dataDic objectForKey:@"OBJECT"]];
                    success(@"请求成功");
                } else {
                    success(@"请求成功");
                }
            }else{
                if (msg.length != 0) {
                    failureMessage(msg);
                } else {
                    failureMessage(@"请求超时");
                }
            }
            
        }
        
    } failure:^(bool isFailure) {
        failureMessage(@"请求超时");
    }];

}
-(void)requestProfitWithPageNum:(NSInteger )pageNum PageSize:(NSInteger )pageSize SearchType:(NSString *)searchType success:(void (^)(NSArray *))success failure:(void (^)(NSString *))failureMessage
{
    _searchType =searchType;
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    UserInfoModel *model = [[ManagerGlobeUntil sharedManager] getUserInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:model.token forKey:@"TOKEN"];
    [parameter setObject:[NSNumber numberWithInteger:pageNum] forKey:@"PAGENUM"];
    [parameter setObject:[NSNumber numberWithInteger:pageSize] forKey:@"PAGESIZE"];
    [parameter setObject:searchType forKey:@"SEARCHTYPE"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"user/expenseRecord" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            NSString *msg = [dataDic objectForKey:@"MSG"];
            if ([state isEqualToString:@"0"]) {//请求成功
                if ([[dataDic objectForKey:@"OBJECT"] count] != 0) {
                    success( [weakSelf handelProfitData:[dataDic objectForKey:@"OBJECT"]]);
                } else {
                    success(@[]);
                }
            }else{
                if (msg.length != 0) {
                    failureMessage(msg);
                } else {
                    failureMessage(@"请求超时");
                }
            }
            
        }
        
    } failure:^(bool isFailure) {
        failureMessage(@"请求超时");
    }];

}
- (void)handelMyWalletData:(NSDictionary*)dataDic  {
    self.shiHuiBiMoney = [NSString stringTransformObject:[dataDic objectForKey:@"MONEY"]];
     self.recommendFriend = [NSString stringTransformObject:[dataDic objectForKey:@"INVITECOUNT"]];
    
}
- (NSMutableArray *)handelProfitData:(NSDictionary*)dataDic  {
    NSMutableArray * dataSouce =[[NSMutableArray alloc]init];
    self.totalCount = [[NSString stringTransformObject:[dataDic objectForKey:@"TOTALROW"]] integerValue];
    NSArray *listArray =[dataDic objectForKey:@"LIST"];
    if ([_searchType isEqualToString:@"1"]) {
        for (NSInteger i =0; i<listArray.count; i++) {
            NSDictionary *dataDic =[listArray objectAtIndex:i];
            NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
            [dic setObject:[NSString stringWithFormat:@"订单号:%@收益",[NSString stringTransformObject:[dataDic objectForKey:@"FROMORDERNUM"]]] forKey:@"titleText"];
            [dic setObject:[NSString stringTransformObject:[dataDic objectForKey:@"CREATEDATE"]] forKey:@"badyText"];
            [dic setObject:[NSString stringWithFormat:@"%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"AMOUNT"]] floatValue]] forKey:@"moneyText"];
            [dataSouce addObject:dic];
        }
    }else if([_searchType isEqualToString:@"2"]){
        for (NSInteger i =0; i<listArray.count; i++) {
            NSDictionary *dataDic =[listArray objectAtIndex:i];
            NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
            [dic setObject:[NSString stringWithFormat:@"获得好友%@收益",[NSString stringTransformObject:[dataDic objectForKey:@"NICKNAME"]]] forKey:@"titleText"];
            [dic setObject:[NSString stringTransformObject:[dataDic objectForKey:@"CREATEDATE"]] forKey:@"badyText"];
            [dic setObject:[NSString stringWithFormat:@"%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"AMOUNT"]] floatValue]] forKey:@"moneyText"];
            [dataSouce addObject:dic];
        }
    }else if([_searchType isEqualToString:@"3"]){
        for (NSInteger i =0; i<listArray.count; i++) {
            NSDictionary *dataDic =[listArray objectAtIndex:i];
            NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
            [dic setObject:[NSString stringTransformObject:[dataDic objectForKey:@"CREATEDATE"]] forKey:@"badyText"];
            [dic setObject:[NSString stringWithFormat:@"%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"AMOUNT"]] floatValue]] forKey:@"moneyText"];
            [dataSouce addObject:dic];
        }
    }else if([_searchType isEqualToString:@"4"]){
        for (NSInteger i =0; i<listArray.count; i++) {
            NSDictionary *dataDic =[listArray objectAtIndex:i];
            NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
            [dic setObject:[NSString stringTransformObject:[dataDic objectForKey:@"NICKNAME"]] forKey:@"titleText"];
            [dic setObject:[NSString stringTransformObject:[dataDic objectForKey:@"CREATEDATE"]] forKey:@"badyText"];
            [dic setObject:[NSString stringWithFormat:@"%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"AMOUNT"]] floatValue]] forKey:@"moneyText"];
            [dataSouce addObject:dic];
        }
    }
    return dataSouce;
}
@end
