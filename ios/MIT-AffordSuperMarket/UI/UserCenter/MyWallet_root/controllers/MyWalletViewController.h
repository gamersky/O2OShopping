//
//  MyWalletViewController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/1.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： MyWalletViewController
 Created_Date： 20160401
 Created_People： JSQ
 Function_description： 我的钱包
 ***************************************/
#import "BaseViewController.h"

@interface MyWalletViewController : BaseViewController
@property (nonatomic, strong)NSString *userLevel;
@end
