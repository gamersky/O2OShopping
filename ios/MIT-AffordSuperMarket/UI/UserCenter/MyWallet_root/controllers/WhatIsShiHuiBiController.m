//
//  WhatIsShiHuiBiController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/4.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
#define kLeftSpace 15
#define ktopSpace 25
//controller
#import "WhatIsShiHuiBiController.h"
#import "MembersShareController.h"
#import "MemberAwardController.h"
//untils
#import "MTA.h"
#import "AppDelegate.h"
#import "RDVTabBarController.h"
#import "Global.h"
#import "NSString+TextSize.h"
#import "UIColor+Hex.h"
@interface WhatIsShiHuiBiController ()
@property(nonatomic,strong)UILabel *whatIsShiHuiBi;
@property(nonatomic,strong)UILabel *shiHuiBiDeatil;
@property(nonatomic,strong)UIButton *touchUpShare;
@property(nonatomic,strong)UIButton *howBeVip;
@end

@implementation WhatIsShiHuiBiController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"什么是实惠币";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    [self addBackNavItem];
    [self setSubView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"什么是实惠币"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"什么是实惠币"];
}
-(void)shareBtnClick{
    if ([self.userLevel isEqualToString:@"2"]) {
        MembersShareController  *shareVc = [[MembersShareController alloc]init];
        [self.navigationController pushViewController:shareVc animated:YES];
    }else{
        MemberAwardController  *awardVc = [[MemberAwardController alloc]init];
        [self.navigationController pushViewController:awardVc animated:YES];
    }
}
-(void)setSubView
{
    UIView *backGroundView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, HightScalar(260))];
    backGroundView.backgroundColor =[UIColor whiteColor];
    [self.view addSubview: backGroundView];
    //标题
    NSString *whatIsShiHuiBiText =@"什么是实惠币？";
    CGSize whatIsShiHuiBiSize =[whatIsShiHuiBiText sizeWithFont:[UIFont systemFontOfSize:FontSize(18)] maxSize:CGSizeMake(VIEW_WIDTH-30, HightScalar(30))];
    
    _whatIsShiHuiBi =[[UILabel alloc]initWithFrame:CGRectMake(kLeftSpace, ktopSpace, whatIsShiHuiBiSize.width, whatIsShiHuiBiSize.height)];
    _whatIsShiHuiBi.font =[UIFont systemFontOfSize:FontSize(18)];
    _whatIsShiHuiBi.textColor =[UIColor colorWithHexString:@"#e63f17"];
    _whatIsShiHuiBi.text= whatIsShiHuiBiText;
    [backGroundView addSubview:_whatIsShiHuiBi];
    
    NSString *shiHuiBiDeatilText =@"实惠币是您升级为白金会员后，您消费的每一笔金额，都将转化成不等额的实惠币，1实惠币等于1元人民币，同时您推荐的朋友每消费一笔，也都将为您存下一笔钱，推荐的朋友越多，您存的钱也就越多，边消费边理财的模式，让您成为持家小能手。";
    CGSize shiHuiBiDeatilSize =[shiHuiBiDeatilText sizeWithFont:[UIFont systemFontOfSize:FontSize(15)] maxSize:CGSizeMake(VIEW_WIDTH-30, HightScalar(100))];
    _shiHuiBiDeatil =[[UILabel alloc]initWithFrame:CGRectMake(kLeftSpace, CGRectGetMaxY(_whatIsShiHuiBi.frame)+10, shiHuiBiDeatilSize.width, shiHuiBiDeatilSize.height)];
    _shiHuiBiDeatil.font =[UIFont systemFontOfSize:FontSize(15)];
    _shiHuiBiDeatil.textColor =[UIColor colorWithHexString:@"#555555"];
    _shiHuiBiDeatil.numberOfLines =0;
    _shiHuiBiDeatil.text= shiHuiBiDeatilText;
    [backGroundView addSubview:_shiHuiBiDeatil];
    
    _touchUpShare = [UIButton buttonWithType:UIButtonTypeCustom];
    CGSize touchUpShareSize = [@"如何成为白金会员" sizeWithFont:[UIFont systemFontOfSize:FontSize(15)] maxSize:CGSizeMake(200, HightScalar(20))];
    _touchUpShare.titleLabel.textColor = [UIColor colorWithHexString:@"#e63f17"];
    NSString *lableText = @"";
    if ([_userLevel isEqualToString:@"2"]) {
        lableText =@"点击分享给好友";
    }else{
        lableText =@"如何成为白金会员";
    }
    NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:lableText];
    NSRange contentRange = {0, [content length]};
    [content addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:contentRange];
    [_touchUpShare setAttributedTitle:content forState:UIControlStateNormal];
    _touchUpShare.titleLabel.font =[UIFont systemFontOfSize:FontSize(15)];
    _touchUpShare.titleLabel.textAlignment =NSTextAlignmentLeft;
    _touchUpShare.frame = CGRectMake(kLeftSpace
                                 , CGRectGetMaxY(_shiHuiBiDeatil.frame)+10
                                 , touchUpShareSize.width
                                 , HightScalar(20));
    [_touchUpShare addTarget:self action:@selector(shareBtnClick) forControlEvents:UIControlEventTouchUpInside];
    _touchUpShare.backgroundColor =[UIColor clearColor];
    [backGroundView addSubview:_touchUpShare];
}
-(void)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
