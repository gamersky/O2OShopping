//
//  ManagerGlobeUntil.m
//  MIT_itrade
//
//  Created by wanc on 14-4-24.
//  Copyright (c) 2014年 ___ZhengDaXinXi___. All rights reserved.
//
/*********************************
 文件名: ManagerGlobeUntil.m
 功能描述: 全局数据单例
 创建人: GT
 修改日期: 2015.10.30
 *********************************/
#import "ManagerGlobeUntil.h"
//vendor
#import "SaveDataInKeychain.h"
//utils
#import "Global.h"
#import "AppDelegate.h"
#import "NSString+Conversion.h"
//model
#import "LouCengInfoModel.h"
@interface ManagerGlobeUntil() {
    
}
@end

@implementation ManagerGlobeUntil

+ (instancetype)sharedManager {
    static ManagerGlobeUntil *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[ManagerGlobeUntil alloc]init];
    });
    
    return _sharedManager;
}

#pragma mark -- init method
-(id)init
{
    if (self = [super init])
    {
        //删除注册signature信息
        [SaveDataInKeychain delete:KEYCHAIN_SIGNATURE_KEY];
        
         /*  交易   */
        
        _singleLogin = NO;
        _isOutOfTimeLimit = NO;
        _transactionLogin = NO;
        _tradeLoginID = @"";
        _tradeLoginPwd = @"";
        _bigClassID = @"";
        _bigClassTitle = @"";
        self.isNetworkReachability = YES;
    }
    
    return self;
}


#pragma mark -- 清空用户信息

- (void)clearUserInfoData {
    UserInfoModel *model = [[UserInfoModel alloc]init];
    [self updataUserInfoWithModel:model];
    self.transactionLogin = NO;
    self.tradeLoginID = @"";
    self.tradeLoginPwd = @"";
}

#pragma mark --  数据存储

/*    对个人信息进行处理         */
- (void)updataUserInfoWithModel:(UserInfoModel*)model {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:model];
    [userDefaults setObject:data forKey:User_INFO];
    [userDefaults synchronize];
}
- (UserInfoModel*)getUserInfo {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefault objectForKey:User_INFO];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

// 对小区和商店信息处理
- (void)updataXiaoquAndStoreInfoWithModel:(XiaoQuAndStoreInfoModel*)model {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:model];
    [userDefaults setObject:data forKey:XIAOQU_AND_STORE__INFO];
    [userDefaults synchronize];
}
- (XiaoQuAndStoreInfoModel*)getxiaoQuAndStoreInfo {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefault objectForKey:XIAOQU_AND_STORE__INFO];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}
//  对服务端返回的楼层信息重新组装
- (NSArray*)modifiedServerXiaoQuInfoDatasoruce:(NSArray*)datasource {
    NSMutableArray *louNumberDatasource = [[NSMutableArray alloc]init];//楼层信息
    NSMutableArray *danYuanNumberDatasource = [[NSMutableArray alloc]init];//单元信息
    NSMutableArray *roomNumberDatasource = [[NSMutableArray alloc]init];//房间信息
    for (NSDictionary *louNumDic in datasource) {//楼号信息
        NSMutableArray *danyuanArr = [[NSMutableArray alloc]init];//存放每个楼几个单元
        NSMutableArray *sectionDanYuanArr = [[NSMutableArray alloc]init];//存放每个单元多少个房间
        LouCengInfoModel *louNumberModel = [[LouCengInfoModel alloc]init];
        louNumberModel.ID = [NSString stringTransformObject:[louNumDic objectForKey:@"ID"]];
        louNumberModel.name = [NSString stringTransformObject:[louNumDic objectForKey:@"NAME"]];
        [louNumberDatasource addObject:louNumberModel];
        for (NSDictionary *danYunNumDic in [louNumDic objectForKey:@"CHILDREN"]) {//单元号信息
            NSMutableArray *roomArr = [[NSMutableArray alloc]init];
            LouCengInfoModel *danYuanNumberModel = [[LouCengInfoModel alloc]init];
            danYuanNumberModel.ID = [NSString stringTransformObject:[danYunNumDic objectForKey:@"ID"]];
            danYuanNumberModel.name = [NSString stringTransformObject:[danYunNumDic objectForKey:@"NAME"]];
            [danyuanArr addObject:danYuanNumberModel];
            for (NSDictionary *roomNumberDic in [danYunNumDic objectForKey:@"CHILDREN"]) {//房间号信息
                LouCengInfoModel *roomNumberModel = [[LouCengInfoModel alloc]init];
                roomNumberModel.ID = [NSString stringTransformObject:[roomNumberDic objectForKey:@"ID"]];
                roomNumberModel.name = [NSString stringTransformObject:[roomNumberDic objectForKey:@"NAME"]];
                [roomArr addObject:roomNumberModel];
            }
            [sectionDanYuanArr addObject:roomArr];
        }
        [roomNumberDatasource addObject:sectionDanYuanArr];
        [danYuanNumberDatasource addObject:danyuanArr];
    }
    return @[louNumberDatasource,danYuanNumberDatasource,roomNumberDatasource];
}


#pragma mark - MBProgressHUD

- (void)showAlertWithMsg:(NSString*)str delegate:(id)delegate
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                   message:str delegate:delegate cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alert show];
    
}

- (void)hideAllHUDFormeView:(id)View
{
    [MBProgressHUD hideAllHUDsForView:(UIView*)View animated:YES];
}

- (void)showTRHUDWithMsg:(NSString*)str inView:(id)View
{
    if (!HUD)
    {
        HUD = [MBProgressHUD showHUDAddedTo:(UIView *)View animated:YES];
        HUD.delegate = (id<MBProgressHUDDelegate> )self;
        HUD.labelText = str;
        HUD.yOffset = -74.0f;
        
    }
}

- (void)showHUDWithMsg:(NSString*)str inView:(id)View
{
//    if (!HUD)
//    {
//        
//    }
    HUD = [MBProgressHUD showHUDAddedTo:(UIView *)View animated:YES];
    HUD.delegate = (id<MBProgressHUDDelegate> )self;
    HUD.labelText = str;
}

- (void)showTextHUDWithMsg:(NSString*)str inView:(id)View
{
    [self hideHUD];
    UIView *v = (UIView*)View;
    if (!v)
    {
        v = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).window;
    }
    HUD = [MBProgressHUD showHUDAddedTo:v animated:YES];
    HUD.mode = MBProgressHUDModeText;
    HUD.delegate = (id<MBProgressHUDDelegate> )self;
    HUD.detailsLabelText = str;
    [HUD hide:YES afterDelay:2.f];
}
- (void)hideHUD
{
    if (HUD)
    {
        
        [HUD hide:YES afterDelay:0.3];
    }
    
}

- (void)hideHUDFromeView:(UIView*)view
{
    [MBProgressHUD hideAllHUDsForView:view animated:YES];
}

#pragma mark -- 字体大小比例
-(CGFloat)setDifferenceScreenFontSizeWithFontOfSize:(CGFloat)size
{
    if (VIEW_WIDTH == 375) {
        return size;
    }else if(VIEW_WIDTH == 320)
    {
        return size*0.85;
    }else if(VIEW_WIDTH == 414) {
        return size *1.05;
    }
    return size;
    
}
-(CGFloat)setCurrentViewHightWithBaseViewHight:(CGFloat)hight
{
    if (VIEW_WIDTH == 375) {
        return hight;
    }else if(VIEW_WIDTH == 320)
    {
        return hight*0.85;
    }else if(VIEW_WIDTH == 414) {
        return hight *1.1;
    }
    return hight;
}
#pragma mark -- lable宽度
-(CGSize)setLabelWidthWithNSString:(NSString*)lableText Font:(CGFloat)font
{
    CGSize maxSize = CGSizeMake(VIEW_WIDTH, HightScalar(53));
    CGSize curSize = [lableText boundingRectWithSize:maxSize
                                             options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                          attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:font] }
                                             context:nil].size;
    return curSize;
    
}

@end
