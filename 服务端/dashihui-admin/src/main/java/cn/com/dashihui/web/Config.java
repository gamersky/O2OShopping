package cn.com.dashihui.web;

import java.io.File;
import java.util.Map;

import com.google.common.collect.Maps;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.ext.plugin.shiro.ShiroInterceptor;
import com.jfinal.ext.plugin.shiro.ShiroPlugin;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;
import com.jfinal.upload.OreillyCos;

import cn.com.dashihui.web.controller.APIController;
import cn.com.dashihui.web.controller.AdController;
import cn.com.dashihui.web.controller.AdminController;
import cn.com.dashihui.web.controller.BrandController;
import cn.com.dashihui.web.controller.CategoryController;
import cn.com.dashihui.web.controller.CityController;
import cn.com.dashihui.web.controller.CommunityController;
import cn.com.dashihui.web.controller.FeedbackController;
import cn.com.dashihui.web.controller.GoodsBaseController;
import cn.com.dashihui.web.controller.GoodsStoreController;
import cn.com.dashihui.web.controller.LogKeywordController;
import cn.com.dashihui.web.controller.OrderController;
import cn.com.dashihui.web.controller.OrderEvalController;
import cn.com.dashihui.web.controller.OrderRefundController;
import cn.com.dashihui.web.controller.OrderReportController;
import cn.com.dashihui.web.controller.ResourceController;
import cn.com.dashihui.web.controller.RoleController;
import cn.com.dashihui.web.controller.StoreController;
import cn.com.dashihui.web.controller.SysConfigController;
import cn.com.dashihui.web.controller.SystemController;
import cn.com.dashihui.web.controller.UserController;
import cn.com.dashihui.web.controller.VersionController;
import cn.com.dashihui.web.controller.WebJoinController;
import cn.com.dashihui.web.controller.WebNoticeController;
import cn.com.dashihui.web.controller.WxArticleCategoryController;
import cn.com.dashihui.web.controller.WxArticleController;
import cn.com.dashihui.web.controller.WxArticleOutputController;
import cn.com.dashihui.web.dao.Ad;
import cn.com.dashihui.web.dao.Admin;
import cn.com.dashihui.web.dao.Brand;
import cn.com.dashihui.web.dao.Category;
import cn.com.dashihui.web.dao.City;
import cn.com.dashihui.web.dao.Community;
import cn.com.dashihui.web.dao.Feedback;
import cn.com.dashihui.web.dao.GoodsBase;
import cn.com.dashihui.web.dao.GoodsBaseImages;
import cn.com.dashihui.web.dao.GoodsStore;
import cn.com.dashihui.web.dao.GoodsStoreImages;
import cn.com.dashihui.web.dao.LogKeyword;
import cn.com.dashihui.web.dao.Logistics;
import cn.com.dashihui.web.dao.Order;
import cn.com.dashihui.web.dao.OrderRefund;
import cn.com.dashihui.web.dao.OrderRefundAPIRecord;
import cn.com.dashihui.web.dao.Resource;
import cn.com.dashihui.web.dao.Role;
import cn.com.dashihui.web.dao.Store;
import cn.com.dashihui.web.dao.StoreAd;
import cn.com.dashihui.web.dao.SysConfig;
import cn.com.dashihui.web.dao.User;
import cn.com.dashihui.web.dao.Version;
import cn.com.dashihui.web.dao.WebJoin;
import cn.com.dashihui.web.dao.WebNotice;
import cn.com.dashihui.web.dao.WxArticle;
import cn.com.dashihui.web.dao.WxArticleCategory;
import cn.com.dashihui.web.handler.ContextParamsHandler;
import cn.com.dashihui.web.handler.SessionHandler;

public class Config extends JFinalConfig {
	
	private Routes routes;
	
	public void configConstant(Constants me) {
		// 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
		loadPropertyFile("config.properties");
		//设置视图根目录
		me.setBaseViewPath("/WEB-INF/page");
	    //设置字符集
	    me.setEncoding("UTF-8");
	    me.setViewType(ViewType.JSP);
	    //调试模式，会打印详细日志
		me.setDevMode(getPropertyToBoolean("constants.devMode", false));
		//上传文件配置
		//注：此处的目录是上级目录，真正在Controller中获取上传的文件时，需要指定（也可以不指定）要将文件移到什么目录下（）相对于此处指定的目录
		//比如在此设置目录为<E:/>，而在Controller中<getFile("fileName","upload")>，则会将文件移至<E:/upload>目录中，如果不指定则移至<E:/>目录中
		OreillyCos.init(PathKit.getWebRootPath()+File.separator+"upload", 10*1024*1024, "UTF-8");
		//出错跳转页面
		me.setError401View("/WEB-INF/page/401.jsp");
		me.setError403View("/WEB-INF/page/403.jsp");
		me.setError404View("/WEB-INF/page/404.jsp");
		me.setError500View("/WEB-INF/page/500.jsp");
	}
	
	public void configRoute(Routes me) {
		this.routes = me;
		//api
		me.add("/api",APIController.class);
		//权限系列
		me.add("/", SystemController.class);
		me.add("/auth/admin", AdminController.class);
		me.add("/auth/role", RoleController.class);
		me.add("/auth/resource", ResourceController.class);
		//字典系列
		me.add("/dict/city", CityController.class);
		me.add("/dict/brand", BrandController.class);
		me.add("/dict/category", CategoryController.class);
		me.add("/dict/community", CommunityController.class);
		me.add("/dict/store", StoreController.class);
		//业务系列
		me.add("/bus/ad", AdController.class);
		me.add("/bus/goods/base",GoodsBaseController.class);
		me.add("/bus/goods/store",GoodsStoreController.class);
		me.add("/bus/user", UserController.class);
		me.add("/bus/order",OrderController.class);
		me.add("/bus/order/report",OrderReportController.class);
		me.add("/bus/order/eval",OrderEvalController.class);
		me.add("/bus/order/refund",OrderRefundController.class);
		//系统设置
		me.add("/sys/version",VersionController.class);
		me.add("/sys/feedback",FeedbackController.class);
		me.add("/sys/config",SysConfigController.class);
		//日志
		me.add("/log/keyword",LogKeywordController.class);
		//WEB业务系列
		me.add("/web/notice",WebNoticeController.class);
		me.add("/web/join",WebJoinController.class);
		//微信文章模块
		me.add("/wx/article/category",WxArticleCategoryController.class);
		me.add("/wx/article",WxArticleController.class);
		me.add("/wx/article/output",WxArticleOutputController.class);
	}
	
	public void configPlugin(Plugins me) {
		//缓存
		EhCachePlugin ecp = new EhCachePlugin();
		me.add(ecp);
		// 配置C3p0数据库连接池插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("db.jdbcUrl"), getProperty("db.username"), getProperty("db.password"), getProperty("db.jdbcDriver"));
		c3p0Plugin.setMaxPoolSize(getPropertyToInt("db.maxPoolSize"));
		c3p0Plugin.setMinPoolSize(getPropertyToInt("db.minPoolSize"));
		c3p0Plugin.setInitialPoolSize(getPropertyToInt("db.initialPoolSize"));
		c3p0Plugin.setMaxIdleTime(getPropertyToInt("db.maxIdleTime"));
		c3p0Plugin.setAcquireIncrement(getPropertyToInt("db.acquireIncrement"));
		me.add(c3p0Plugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.setDialect(new MysqlDialect());
		//忽略大小写
		//arp.setContainerFactory(new CaseInsensitiveContainerFactory());
		arp.setShowSql(true);
		me.add(arp);
		//添加model映射
		arp.addMapping("t_auth_admin", Admin.class);
		arp.addMapping("t_auth_roles", Role.class);
		arp.addMapping("t_auth_resources", Resource.class);
		arp.addMapping("t_dict_city", City.class);
		arp.addMapping("t_dict_category","categoryId", Category.class);
		arp.addMapping("t_dict_community", Community.class);
		arp.addMapping("t_dict_store", Store.class);
		arp.addMapping("t_dict_brand", Brand.class);
		arp.addMapping("t_dict_logistics", Logistics.class);
		arp.addMapping("t_bus_ad", Ad.class);
		arp.addMapping("t_bus_store_ad", StoreAd.class);
		arp.addMapping("t_bus_goods_base", GoodsBase.class);
		arp.addMapping("t_bus_goods_base_images", GoodsBaseImages.class);
		arp.addMapping("t_bus_goods", GoodsStore.class);
		arp.addMapping("t_bus_goods_images", GoodsStoreImages.class);
		arp.addMapping("t_bus_user", User.class);
		arp.addMapping("t_bus_order", Order.class);
		arp.addMapping("t_bus_order_refund", OrderRefund.class);
		arp.addMapping("t_bus_order_refund_apirecord", OrderRefundAPIRecord.class);
		
		arp.addMapping("t_sys_version", Version.class);
		arp.addMapping("t_sys_feedback", Feedback.class);
		arp.addMapping("t_sys_config", SysConfig.class);
		arp.addMapping("t_log_keyword", LogKeyword.class);
		arp.addMapping("t_log_keyword", LogKeyword.class);
		
		arp.addMapping("t_web_notice", WebNotice.class);
		arp.addMapping("t_web_join", WebJoin.class);
		
		arp.addMapping("t_wx_article_category", WxArticleCategory.class);
		arp.addMapping("t_wx_article", WxArticle.class);
		//shiro权限框架，添加到plugin
		ShiroPlugin shiroPlugin = new ShiroPlugin(this.routes);
		shiroPlugin.setLoginUrl("/login");
		shiroPlugin.setSuccessUrl("/index");
		shiroPlugin.setUnauthorizedUrl("/error403");
		me.add(shiroPlugin);
	}
	
	public void configInterceptor(Interceptors me) {
		me.add(new SessionInViewInterceptor());
		me.add(new ShiroInterceptor());
	}
	
	public void configHandler(Handlers me) {
		//可在此设置context_path，解决http://ip:port/context_path的问题
		//因为测试时是在jetty下，所以默认没有context_path，如果部署在tomcat下，会自动加上项目名，所以会用到该配置
		//可自定义context_path，默认下是CONTEXT_PATH，使用如：${CONTEXT_PATH}
		me.add(new ContextPathHandler("BASE_PATH"));
		//需要在页面传递的常量
		Map<String,Object> params = Maps.newHashMap();
		params.put("FTP_PATH", PropKit.get("constants.ftppath"));
		me.add(new ContextParamsHandler(params));
		me.add(new SessionHandler());
	}
}
