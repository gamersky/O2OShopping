package cn.com.dashihui.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresAuthentication;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.City;
import cn.com.dashihui.web.service.CityService;

@RequiresAuthentication
public class CityController extends BaseController{
	private static final Logger logger = Logger.getLogger(CityController.class);
	private CityService dictCityService = new CityService();
    
	//@RequiresPermissions("dict:city:index")
    public void index(){
    	setAttr("cityList", dictCityService.findAllCitys());
        render("index.jsp");
    }
    
    public void doSort(){
    	Map<String,String[]> params = getParaMap();
    	if(params!=null&&params.size()!=0&&params.containsKey("sortKey")){
    		String sortKey = params.get("sortKey")[0];
    		Map<String,String> sortMap = new HashMap<String,String>();
    		for(String key : params.keySet()){
    			if(!key.equals("sortKey")){
    				String id = key.replace(sortKey, "");
    				String no = params.get(key)[0];
    				if(StrKit.isBlank(no)||no.length()>3){
    					no = "0";
    				}
    				sortMap.put(id, no);
    			}
    		}
    		dictCityService.sortCity(sortMap);
    		renderSuccess();
    		return;
    	}
    	renderFailed();
    }
    
	public void toAdd(){
		int parentid = getParaToInt(0,0);
		if(parentid!=0){
			setAttr("parent", dictCityService.findById(parentid));
		}
		keepPara("type");
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：地市名称为空，2：地市名称过长，3：地市代码为空，4：地市代码过长，5：地市代码已经存在
	 */
	public void doAdd(){
		//地市名称
		String name = getPara("name");
		//地市代码
		String code = getPara("code");
		//类型，1-省份，2-地市，3-县/区
		int type = getParaToInt("type",1);
		//上级地市ID
		String parentid = getPara("parentid","0");
		if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(name.length()>20){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(code)){
			renderResult(3);
			return;
		}else if(code.length()>6){
			renderResult(4);
			return;
		}else if(dictCityService.findByCode(code)!=null){
			renderResult(5);
			return;
		}else{
			//保存
			City city = new City()
				.set("type", type)
				.set("name", name)
				.set("code", code)
				.set("parentid", parentid);
			if(dictCityService.addCity(city)){
				renderSuccess(dictCityService.findById(city.getInt("id")));
				return;
			}
		}
		renderFailed();
	}
	
	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			City city = dictCityService.findById(id);
			int parentid = city.getInt("parentid");
			if(parentid!=0){
				setAttr("parent", dictCityService.findById(parentid));
			}
			setAttr("object", city);
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：地市名称为空，2：地市名称过长，3：地市代码为空，4：地市代码过长，5：地市代码已经存在
	 */
	public void doEdit(){
		//地市ID
		String cityid = getPara("cityid");
		//地市名称
		String name = getPara("name");
		//地市对应的代码
		String code = getPara("code");
		if(StrKit.isBlank(cityid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(name.length()>20){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(code)){
			renderResult(3);
			return;
		}else if(code.length()>6){
			renderResult(4);
			return;
		}
		City findByCode = dictCityService.findByCode(code);
		if(findByCode!=null&&findByCode.getInt("id")!=Integer.valueOf(cityid)){
			renderResult(5);
			return;
		}else{
			//更新
			if(dictCityService.editCity(new City()
				.set("id", cityid)
				.set("name", name)
				.set("code", code))){
				renderSuccess(dictCityService.findById(Integer.valueOf(cityid)));
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&dictCityService.delCity(id)){
			logger.info("删除地市【"+id+"】，及其下属子数据");
			renderSuccess();
			return;
		}renderFailed();
	}
}
