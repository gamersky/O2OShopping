package cn.com.dashihui.web.controller;

import org.apache.shiro.authz.annotation.RequiresAuthentication;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.kit.CommonKit;
import cn.com.dashihui.kit.ValidateKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.User;
import cn.com.dashihui.web.service.UserService;

@RequiresAuthentication
public class UserController extends BaseController{
	private static final Logger logger = Logger.getLogger(UserController.class);
	private UserService service = new UserService();

    public void index(){
        render("index.jsp");
    }

	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(pageNum, pageSize));
	}

	public void toAdd(){
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：用户名为空，2：用户名格式不正确，3：用户已经存在，4：密码为空，5：密码格式不正确，6：密码长度不正确，7：昵称为空，8：昵称过长
	 */
	public void doAdd(){
		//用户名
		String username = getPara("username");
		//密码
		String password = getPara("password");
		//昵称
		String nickName = getPara("nickName");
		//性别
		int sex = getParaToInt("sex");
		if(StrKit.isBlank(username)){
			renderResult(1);
			return;
		}else if(!ValidateKit.Mobile(username)){
			renderResult(2);
			return;
		}else if(service.findExistsByUsername(username)){
			renderResult(3);
			return;
		}else if(StrKit.isBlank(password)){
			renderResult(4);
			return;
		}else if(ValidateKit.Password_reg(password)){
			renderResult(5);
    		return;
    	}else if(password.length()<6||password.length()>18){
    		renderResult(6);
    		return;
		}else if(StrKit.isBlank(nickName)){
			renderResult(7);
			return;
		}else if(nickName.length()>50){
			renderResult(8);
			return;
		}else{
			//保存
			if(service.addUser(new User()
				.set("username", username)
				.set("password", CommonKit.encryptPassword(password))
				.set("nickName", nickName)
				.set("sex", sex))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}

	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：用户名为空，2：用户名格式不正确，3：用户已经存在，4：昵称为空，5：昵称过长
	 */
	public void doEdit(){
		//用户ID
		String userid = getPara("userid");
		//用户名
		String username = getPara("username");
		//昵称
		String nickName = getPara("nickName");
		//性别
		int sex = getParaToInt("sex");
		if(StrKit.isBlank(userid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(username)){
			renderResult(1);
			return;
		}else if(!ValidateKit.Mobile(username)){
			renderResult(2);
			return;
		}else if(service.findExistsByUsernameWithout(username,Integer.valueOf(userid))){
			renderResult(3);
			return;
		}else if(StrKit.isBlank(nickName)){
			renderResult(4);
			return;
		}else if(nickName.length()>50){
			renderResult(5);
			return;
		}else{
			//保存
			if(service.editUser(new User()
				.set("id", userid)
				.set("username", username)
				.set("nickName", nickName)
				.set("sex", sex))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}

	public void toEditPwd(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("editPwd.jsp");
	}
	
	/**
	 * 更新密码
	 * @return -1：异常，0：成功，1：密码为空，2：密码格式不正确，3：密码长度不正确
	 */
	public void doEditPwd(){
		//用户ID
		String userid = getPara("userid");
		//密码
		String password = getPara("password");
		if(StrKit.isBlank(userid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(password)){
			renderResult(1);
			return;
		}else if(ValidateKit.Password_reg(password)){
			renderResult(2);
    		return;
    	}else if(password.length()<6||password.length()>18){
    		renderResult(3);
    		return;
		}else{
			//更新
			if(service.editUser(new User()
				.set("id", userid)
				.set("password", CommonKit.encryptPassword(password)))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&service.delUser(id)){
			logger.info("删除会员【"+id+"】");
			renderSuccess();
			return;
		}
		renderFailed();
	}
}