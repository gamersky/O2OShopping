package cn.com.dashihui.web.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;

public class SessionHandler extends Handler{
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        int index1 = target.lastIndexOf(";jsessionid");
        target = index1==-1?target:target.substring(0, index1);

        int index2 = target.lastIndexOf(";JSESSIONID");
        target = index2==-1?target:target.substring(0, index2);
        nextHandler.handle(target, request, response, isHandled);
    }
}
