package cn.com.dashihui.web.dao;

import com.jfinal.plugin.activerecord.Model;

public class Brand extends Model<Brand>{
	private static final long serialVersionUID = 1L;
	private static Brand me = new Brand();
	public static Brand me(){
		return me;
	}
	
	public String getTrueName(){
		return getStr("trueName");
	}
}
