package cn.com.dashihui.web.controller;

import org.apache.shiro.authz.annotation.RequiresAuthentication;

import com.jfinal.kit.PropKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.service.FeedbackService;

@RequiresAuthentication
public class FeedbackController extends BaseController{
	private static final Logger logger = Logger.getLogger(FeedbackController.class);
	private FeedbackService service = new FeedbackService();
    
	//@RequiresPermissions("sys:feedback:index")
    public void index(){
        render("index.jsp");
    }
    
    public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(pageNum, pageSize));
	}
    
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&service.delFeedback(id)){
			logger.info("删除意见反馈内容【"+id+"】");
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
