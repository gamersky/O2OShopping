package cn.com.dashihui.web.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresAuthentication;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.Community;
import cn.com.dashihui.web.dao.Store;
import cn.com.dashihui.web.service.CommunityService;

@RequiresAuthentication
public class CommunityController extends BaseController{
	private static final Logger logger = Logger.getLogger(CommunityController.class);
	private CommunityService communityService = new CommunityService();
    
	/**
	 * 转到社区管理主页面
	 */
	//@RequiresPermissions("dict:community:index")
    public void index(){
        render("index.jsp");
    }
    /**
     * 社区管理主页面的分页数据
     */
    public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		int province = getParaToInt("p",0);
		int city = getParaToInt("c",0);
		int area = getParaToInt("a",0);
		String title = getPara("t");
		renderResult(0,communityService.findByPage(pageNum, pageSize, province, city, area, title));
	}
    public void toSetStore(){
    	//查出指定省、市、县/区的便利店列表，供用户选择
    	int province = getParaToInt("p"),city = getParaToInt("c"),area = getParaToInt("a");
    	List<Store> storeList = communityService.findStoreByCity(province,city,area);
    	//指定社区ID
    	int communityid = getParaToInt(0);
    	Community comm = communityService.findById(communityid);
    	if(comm!=null){
    		for(Store item : storeList){
    			if(item.getInt("id").intValue()==comm.getInt("storeid").intValue()){
    				item.put("checked", true);
    				break;
    			}
    		}
    	}
    	setAttr("communityid", communityid);
    	setAttr("storeList", storeList);
		render("setStore.jsp");
    }
    
    public void doSetStore(){
    	//要保存的社区ID及便利店ID
    	int storeid = getParaToInt("storeid",0);
    	int communityid = getParaToInt("communityid",0);
    	if(storeid!=0&&communityid!=0){
	    	//更新保存
	    	if(communityService.setStore(storeid, communityid)){
	    		renderSuccess(communityService.findById(communityid));
	    		return;
	    	}
    	}
		renderFailed();
    }
    
    /**
     * 转到社区管理增加页面
     */
	public void toAdd(){
		render("add.jsp");
	}
	
	/**
	 * 添加
	 */
	public void doAdd(){
		//名称
		String title = getPara("title");
		//百度定位KEY
		String baidukey = getPara("baidukey");
		//省、市、县/区
		int province = getParaToInt("province",0),city = getParaToInt("city",0),area = getParaToInt("area",0);
		//详细地址
		String address = getPara("address");
		if(StrKit.isBlank(title)){
			renderResult(1);
			return;
		}else if(title.length()>50){
			renderResult(2);
			return;
		}else if(province==0||city==0||area==0){
			renderResult(3);
			return;
		}else if(StrKit.isBlank(address)){
			renderResult(4);
			return;
		}else if(address.length()>100){
			renderResult(5);
			return;
		}else if(StrKit.isBlank(baidukey)){
			renderResult(6);
			return;
		}else{
			//保存
			Community comm = new Community()
				.set("title", title)
				.set("baidukey", baidukey)
				.set("province", province)
				.set("city", city)
				.set("area", area)
				.set("address", address);
			if(communityService.addCommunity(comm)){
				renderSuccess(communityService.findById(comm.getInt("id")));
				return;
			}
		}
		renderFailed();
	}
	
	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", communityService.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return 
	 */
	public void doEdit(){
		//ID
		String communityid = getPara("communityid");
		//名称
		String title = getPara("title");
		//省、市、县/区
	    int province = getParaToInt("province",0),city = getParaToInt("city",0),area = getParaToInt("area",0);
		//百度定位KEY
		String baidukey = getPara("baidukey");
		//详细地址
		String address = getPara("address");
		if(StrKit.isBlank(communityid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(title)){
			renderResult(1);
			return;
		}else if(title.length()>50){
			renderResult(2);
			return;
		}else if(province==0||city==0||area==0){
			renderResult(3);
			return;
		}else if(StrKit.isBlank(address)){
			renderResult(4);
			return;
		}else if(address.length()>100){
			renderResult(5);
			return;
		}else if(StrKit.isBlank(baidukey)){
			renderResult(6);
			return;
		}else{
			//更新
			Community comm = new Community()
				.set("id", communityid)
				.set("title", title)
				.set("province", province)
				.set("city", city)
				.set("area", area)
				.set("baidukey", baidukey)
				.set("address", address);
			if(communityService.editCommunity(comm)){
				renderSuccess(communityService.findById(Integer.valueOf(communityid)));
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0){
			Community comm = communityService.findById(id);
			comm.delete();
			logger.info("删除社区【"+id+"】");
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
