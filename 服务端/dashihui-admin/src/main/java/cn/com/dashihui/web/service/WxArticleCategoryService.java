package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;

import cn.com.dashihui.web.dao.WxArticleCategory;

public class WxArticleCategoryService {
	
	public boolean addCategory(WxArticleCategory newObject){
		return newObject.save();
	}
	
	public boolean hasArticle(int categoryid){
		return Db.queryLong("SELECT COUNT(*) FROM t_wx_article WHERE categoryid=?",categoryid)!=0;
	}
	
	public boolean delCategory(int id){
		return WxArticleCategory.me().deleteById(id);
	}
	
	public boolean editCategory(WxArticleCategory object){
		return object.update();
	}
	
	public WxArticleCategory findById(int id){
		return WxArticleCategory.me().findFirst("SELECT * FROM t_wx_article_category WHERE id=?",id);
	}
	
	public List<WxArticleCategory> findAllWithCount(int mp){
		return WxArticleCategory.me().find("SELECT A.*,(SELECT COUNT(*) FROM t_wx_article WHERE categoryid=A.id) articleNum FROM t_wx_article_category A WHERE A.mp=? ORDER BY A.orderNo",mp);
	}
	
	public List<WxArticleCategory> findAll(int mp){
		return WxArticleCategory.me().find("SELECT A.* FROM t_wx_article_category A WHERE A.mp=? ORDER BY A.orderNo DESC",mp);
	}
	
	/**
	 * 保存分类排序
	 */
	public void sortCategory(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_wx_article_category SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		Db.batch(sqlList,batchSize);
	}
}
