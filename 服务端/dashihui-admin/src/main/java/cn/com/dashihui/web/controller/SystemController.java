package cn.com.dashihui.web.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.session.SessionException;
import org.apache.shiro.subject.Subject;

import com.jfinal.aop.Duang;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.Admin;
import cn.com.dashihui.web.service.AdminService;
import cn.com.dashihui.web.service.ResourceService;

public class SystemController extends BaseController{
	private static final Logger LOG = Logger.getLogger(SystemController.class);
	private ResourceService resourceService = new ResourceService();
	private AdminService adminService = Duang.duang(new AdminService());
//    private static final int DEFAULT_CAPTCHA_LEN = 4;
    
    @RequiresAuthentication
    public void index(){
		this.render("index.jsp");
    }
    
    public void login(){
        this.render("login.jsp");
    }
    
//    @ActionKey("/img")
//    public void img(){
//        CaptchaRender img = new CaptchaRender(DEFAULT_CAPTCHA_LEN);
//        this.setSessionAttr(CaptchaRender.DEFAULT_CAPTCHA_MD5_CODE_KEY, img.getMd5RandomCode());
//        this.render(img);
//    }
    
    /**
     * 登录操作
     * @return 1：用户名为空，2：密码为空，3：用户名或密码错误，4：系统异常
     */
    public void doLogin(){
        try {
        	String username = getPara("username");
        	String password = getPara("password");
        	if(StrKit.isBlank(username)){
        		//用户名为空
                this.renderResult(1);
        	}else if(StrKit.isBlank(password)){
        		//密码为空
                this.renderResult(2);
        	}else{
        		//根据入参创建token
        		boolean rememberMe = getParaToBoolean("rememberMe",false);
        		String host = getRequest().getRemoteHost();
        		UsernamePasswordToken authenticationToken = new UsernamePasswordToken(username, password, rememberMe, host);
        		//认证
        		Subject currentUser = SecurityUtils.getSubject();
        		currentUser.login(authenticationToken);
        		Admin currUser = (Admin)currentUser.getPrincipal();
        		//查询登录用户可操作的菜单
        		this.setSessionAttr("navMenus", resourceService.findMenuResourcesByUser(currUser.getInt("id")));
        		//登录成功
        		this.renderSuccess();
        	}
        } catch (AuthenticationException e) {
        	//e.printStackTrace();
        	LOG.error("登录失败");
        	//用户名或者密码错误
            this.renderResult(3);
        } catch (Exception e) {
        	e.printStackTrace();
            //系统异常
            this.renderResult(4);
        }
    }
    
	public void toEditMinePwd(){
		render("editMinePwd.jsp");
	}
	
	/**
	 * 更新当前登录用户的密码
	 * @return -1：异常，0：成功，1：密码为空，2：密码过长
	 */
	public void doEditMinePwd(){
		//原密码
		String passwordOld = getPara("passwordOld");
		//新设密码
		String passwordNew = getPara("passwordNew");
		if(StrKit.isBlank(passwordOld)){
			renderResult(1);
			return;
		}else if(StrKit.isBlank(passwordNew)){
			renderResult(2);
			return;
		}else if(passwordNew.length()>100){
			renderResult(3);
			return;
		}else{
			//更新
			try {
				if(adminService.editMinePwd(getCurrentUser(),passwordOld,passwordNew)){
					renderSuccess();
					return;
				}
			} catch (Exception e) {
				e.printStackTrace();
				//原密码输入错误
				renderResult(4);
				return;
			}
		}
		renderFailed();
	}

    public void logout() {
        Subject currentUser = SecurityUtils.getSubject();
        try {
        	if(currentUser.isAuthenticated())currentUser.logout();
            this.redirect("/login");
        } catch (SessionException ise) {
            LOG.debug("Encountered session exception during logout.  This can generally safely be ignored.", ise);
        } catch (Exception e) {
            LOG.debug("登出发生错误", e);
        }
    }
    
    public void error401(){
    	render("401.jsp");
    }
    
    public void error403(){
    	render("403.jsp");
    }
    
    public void error404(){
    	render("404.jsp");
    }
    
    public void error500(){
    	render("500.jsp");
    }
}
