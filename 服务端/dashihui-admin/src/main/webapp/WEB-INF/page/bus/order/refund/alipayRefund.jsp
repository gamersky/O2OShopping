<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.web.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>退款单管理</title>
</head>
<body>
<c:choose>
<c:when test="${flag==1}">
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<script type="text/javascript">
Kit.alert("退款单号为空！");
setTimeout(function(){
	//先得到当前iframe层的索引
	var index = parent.layer.getFrameIndex(window.name);
	//再执行关闭
	parent.layer.close(index);
},2000);
</script>
</c:when>
<c:when test="${flag==2}">
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<script type="text/javascript">
Kit.alert("退款单不存在！");
setTimeout(function(){
	//先得到当前iframe层的索引
	var index = parent.layer.getFrameIndex(window.name);
	//再执行关闭
	parent.layer.close(index);
},2000);
</script>
</c:when>
<c:when test="${flag==3}">
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<script type="text/javascript">
Kit.alert("退款单状态不正确！");
setTimeout(function(){
	//先得到当前iframe层的索引
	var index = parent.layer.getFrameIndex(window.name);
	//再执行关闭
	parent.layer.close(index);
},2000);
</script>
</c:when>
<c:when test="${flag==4}">
${formHtml}
</c:when>
</c:choose>
</body>
</html>