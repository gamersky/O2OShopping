<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#accreditForm").validate({
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						sRoleDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="accreditForm" action="${BASE_PATH}/auth/admin/doAccredit" method="post" class="form-horizontal">
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>角色名称</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${roles}" var="role">
				<tr>
					<td><input type="checkbox" name="roleid" value="${role.id}" <c:if test="${role.checked}">checked</c:if>/> ${role.name}</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:sRoleDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="userid" value="${userid}">
</form>