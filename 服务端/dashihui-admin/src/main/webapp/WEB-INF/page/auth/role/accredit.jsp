<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!-- jquery tree table -->
<link href='${BASE_PATH}/static/plugins/jquery-treetable/css/jquery.treetable.css' media='all' rel='stylesheet' type='text/css' />
<!-- jquery tree table -->
<script src='${BASE_PATH}/static/plugins/jquery-treetable/js/jquery.treetable.js' type='text/javascript'></script>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#accreditForm").validate({
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 1:
						Kit.alert("请选择权限");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						window.location.reload();
						sResourceDialog.close();
					}
				}
			});
		}
	});
	$("#accreditDataTable").treetable({expandable:true,nodeIdAttr:"nodeId",parentIdAttr:"nodeParentId"});
	$(".resourceItem").on("change",function(){
		var nodeParentId = $(this).parents("tr").data("nodeParentId");
		var nodeId = $(this).parents("tr").data("nodeId");
		onResourceItemChoosedUp(nodeId,nodeParentId,$(this).is(":checked"));
		onResourceItemChoosedDown(nodeId,$(this).is(":checked"));
	});
});
function onResourceItemChoosedUp(nodeId,nodeParentId,isChoosed){
	if(isChoosed){
		var parentNode = $("#accreditTr"+nodeParentId);
		$("#accreditChk"+nodeParentId).prop("checked",true);
		if(parentNode.data("nodeParentId")){
			onResourceItemChoosedUp(parentNode.data("nodeId"),parentNode.data("nodeParentId"),true);
		}
	}else{
		var siblingsChecked = $("tr[data-node-parent-id="+nodeParentId+"] input[type=checkbox]:checked","#accreditDataList");
		if(siblingsChecked.length>0){
			return;
		}
		var parentNode = $("#accreditTr"+nodeParentId);
		$("#accreditChk"+nodeParentId).prop("checked",false);
		if(parentNode.data("nodeParentId")){
			onResourceItemChoosedUp(parentNode.data("nodeId"),parentNode.data("nodeParentId"),false);
		}
	}
}
function onResourceItemChoosedDown(nodeId,isChoosed){
	var childrenNode = $("tr[data-node-parent-id="+nodeId+"]","#accreditDataList");
	$.each(childrenNode,function(indx,child){
		var childNodeId = $(child).data("nodeId");
		$("#accreditChk"+childNodeId).prop("checked",isChoosed);
		onResourceItemChoosedDown(childNodeId,isChoosed);
	});
}
//-->
</script>
<form id="accreditForm" action="${BASE_PATH}/auth/role/doAccredit" method="post" class="form-horizontal">
	<div class="table-responsive">
		<table id="accreditDataTable" class="table table-hover">
			<thead>
				<tr>
					<th>权限名称</th>
				</tr>
			</thead>
			<tbody id="accreditDataList">
			<!-- level1 -->
			<c:if test="${resourceList!=null&&fn:length(resourceList)!=0}">
				<c:forEach items="${resourceList}" var="level1">
				<tr id="accreditTr${level1.id}" data-id="${level1.id}" data-node-id="${level1.id}" data-node-parent-id="${level1.parentid}">
					<td><input type="checkbox" id="accreditChk${level1.id}" class="resourceItem" name="resourceid" value="${level1.id}" <c:if test="${level1.checked==1}">checked</c:if>/> ${level1.name}</td>
				</tr>
				<!-- level2 -->
                <c:if test="${level1.children!=null&&fn:length(level1.children)!=0}">
                	<c:forEach items="${level1.children}" var="level2">
                	<tr id="accreditTr${level2.id}" data-id="${level2.id}" data-node-id="${level2.id}" data-node-parent-id="${level2.parentid}">
						<td><input type="checkbox" id="accreditChk${level2.id}" class="resourceItem" name="resourceid" value="${level2.id}" <c:if test="${level2.checked==1}">checked</c:if>/> ${level2.name}</td>
					</tr>
					<!-- level3 -->
					<c:if test="${level2.children!=null&&fn:length(level2.children)!=0}">
	                	<c:forEach items="${level2.children}" var="level3">
	                	<tr id="accreditTr${level3.id}" data-id="${level3.id}" data-node-id="${level3.id}" data-node-parent-id="${level3.parentid}">
							<td><input type="checkbox" id="accreditChk${level3.id}" class="resourceItem" name="resourceid" value="${level3.id}" <c:if test="${level3.checked==1}">checked</c:if>/> ${level3.name}</td>
						</tr>
	                	</c:forEach>
	                </c:if>
                	</c:forEach>
                </c:if>
	        	</c:forEach>
        	</c:if>
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:sResourceDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="roleid" value="${roleid}">
</form>