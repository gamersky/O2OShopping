<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>分类管理</title>
    <jsp:include page="../../include/header.jsp"></jsp:include>
	<!-- jquery tree table -->
	<link href='${BASE_PATH}/static/plugins/jquery-treetable/css/jquery.treetable.css' media='all' rel='stylesheet' type='text/css' />
</head>
<body>
<div id="wrapper">
	<jsp:include page="../../include/nav.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">分类</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<div class="col-lg-6 col-lg-offset-6">
						<div class="pull-right">
							<button type="button" class="btn btn-link" onclick="toAdd(0,0)"><span class="fa fa-plus"></span> 添加</button>
							<button type="button" class="btn btn-link" onclick="doSort()"><span class="fa fa-save"></span> 保存排序</button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<form id="sortForm" action="${BASE_PATH}/dict/category/doSort" method="post">
					<input type="hidden" name="sortKey" value="orderNo">
					<table id="dataTable" class="table table-hover">
						<thead>
							<tr>
								<th width="25%">分类名称</th>
								<th width="30%">分类编码</th>
								<th width="5%">分类排序 <a href="#" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="排序操作仅作用于同级之间"><span class="fa fa-warning"></span></a></th>
								<th width="10%">操作</th>
							</tr>
						</thead>
						<tbody>
						<!-- level1 -->
						<c:if test="${categoryList!=null&&fn:length(categoryList)!=0}">
							<c:forEach items="${categoryList}" var="level1">
								<tr id="item${level1.categoryId}" data-id="${level1.categoryId}" data-node-id="${level1.categoryId}" data-node-parent-id="">
								
									<td>${level1.categoryName}</td>
									<td>${level1.categoryNum}</td>
									<td><input name="orderNo${level1.categoryId}" value="${level1.categoryNo}" size="3" class="order form-control width50" required ></td>
									<td><button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toAdd('${level1.categoryId}',1)" title="添加"><span class="fa fa-plus fa-lg"></span></button> 
										<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('${level1.categoryId}',1)" title="编辑"><span class="fa fa-edit fa-lg"></span></button>
										<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('${level1.categoryId}',1)" title="删除"><span class="fa fa-remove fa-lg"></span></button>
									</td>
								</tr>
								<!-- level2 -->
				                <c:if test="${level1.children!=null&&fn:length(level1.children)!=0}">
				                	<c:forEach items="${level1.children}" var="level2">
					                	<tr id="item${level2.categoryId}" data-id="${level2.categoryId}" data-node-id="${level2.categoryId}" data-node-parent-id="${level2.categoryFatherId}">
											<td>${level2.categoryName}</td>
											<td>${level2.categoryNum}</td>
											<td><input name="orderNo${level2.categoryId}" value="${level2.categoryNo}" size="3" class="order form-control width50" required></td>
											<td><button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toAdd('${level2.categoryId}',2)" title="添加"><span class="fa fa-plus fa-lg"></span></button> 
												<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('${level2.categoryId}',2)" title="编辑"><span class="fa fa-edit fa-lg"></span></button> 
												<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('${level2.categoryId}',2)" title="删除"><span class="fa fa-remove fa-lg"></span></button>
											</td>
										</tr>
										<!-- level3 -->
										<c:if test="${level2.children!=null&&fn:length(level2.children)!=0}">
						                	<c:forEach items="${level2.children}" var="level3">
							                	<tr id="item${level3.categoryId}" data-id="${level3.categoryId}" data-node-id="${level3.categoryId}" data-node-parent-id="${level3.categoryFatherId}">
													<td>${level3.categoryName}</td>
													<td>${level3.categoryNum}</td>
													<td><input name="orderNo${level3.categoryId}" value="${level3.categoryNo}" size="3" class="order form-control width50" required></td>
													<td><button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toAdd('${level3.categoryId}',3)" title="添加"><span class="fa fa-plus fa-lg"></span></button> 
													    <button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('${level3.categoryId}',3)" title="编辑"><span class="fa fa-edit fa-lg"></span></button> 
														<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('${level3.categoryId}',3)" title="删除"><span class="fa fa-remove fa-lg"></span></button>
													</td>
												</tr>
												<!-- level4 -->
												<c:if test="${level3.children!=null&&fn:length(level3.children)!=0}">
								                	<c:forEach items="${level3.children}" var="level4">
									                	<tr id="item${level4.categoryId}" data-id="${level3.categoryId}" data-node-id="${level4.categoryId}" data-node-parent-id="${level4.categoryFatherId}">
															<td>${level4.categoryName}</td>
															<td>${level4.categoryNum}</td>
															<td><input name="orderNo${level4.categoryId}" value="${level4.categoryNo}" size="3" class="order form-control width50" required></td>
															<td><button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('${level4.categoryId}',4)" title="编辑"><span class="fa fa-edit fa-lg"></span></button> 
																<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('${level4.categoryId}',4)" title="删除"><span class="fa fa-remove fa-lg"></span></button>
															</td>
														</tr>
								                	</c:forEach>
								                </c:if>
						                	</c:forEach>
						                </c:if>
			                	</c:forEach>
			                </c:if>
				        </c:forEach>
			        </c:if>
				</tbody>
			</table>
		</form>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../../include/footer.jsp"></jsp:include>
</div>
<jsp:include page="../../include/javascripts.jsp"></jsp:include>
<!-- 异步加载下一页数据后，用模板渲染 -->
<script type="text/html" id="dataTpl">
	<tr id="item{{categoryId}}" data-id="{{categoryId}}" data-node-id="{{categoryId}}" {{if categoryFatherId}}data-node-parent-id="{{categoryFatherId}}"{{/if}}>
        <td>{{categoryName}}</td>		
        <td>{{categoryNum}}</td>
		<td><input name="orderNo{{categoryId}}" value="{{categoryNo}}" size="3" class="order form-control width50" required></td>
		<td>{{if categoryType!=4}}
				<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toAdd('{{categoryId}}',{{categoryType}})" title="添加"><span class="fa fa-plus fa-lg"></span></button>  
			{{/if}}
			<button type="button" class="btn btn-sm btn-primary btn-simple" onclick="toEdit('{{categoryId}}',{{categoryType}})" title="编辑"><span class="fa fa-edit fa-lg"></span></button> 
			<button type="button" class="btn btn-sm btn-danger btn-simple" onclick="toDelete('{{categoryId}}',{{categoryType}})" title="删除"><span class="fa fa-remove fa-lg"></span></button>
		</td>
	</tr>
</script>
<!-- jquery tree table -->
<script src='${BASE_PATH}/static/plugins/jquery-treetable/js/jquery.treetable.js' type='text/javascript'></script>
<script type="text/javascript">
$(function(){
	$("#dataTable").treetable({expandable:true,clickableNodeNames:true,nodeIdAttr:"nodeId",parentIdAttr:"nodeParentId"}).treetable("collapseAll");
});
function onCategoryAdded(parentid,category){
	$("#dataTable").treetable("loadBranch",(parentid!=0?$("#dataTable").treetable("node",parentid):null),template("dataTpl",category));
}
function onCategoryEdited(category){
	$("#dataTable").treetable("updateNode",$("#dataTable").treetable("node",category.categoryId),template("dataTpl",category));
}
var addDialog;
function toAdd(parentid,type){
	addDialog = Kit.dialog("添加","${BASE_PATH}/dict/category/toAdd/"+parentid+"?type="+(type+1)).open();
	
}
var editDialog;
function toEdit(id,type){
	editDialog = Kit.dialog("修改","${BASE_PATH}/dict/category/toEdit/"+id+"?type="+(type+1)).open();
}
function toDelete(id,type){
	Kit.confirm("提示",(type!=4?"您确定要删除这条数据吗？":"将同时层级下所有的产品分类，您确定要删除这条数据吗？"),function(){
		$.post("${BASE_PATH}/dict/category/doDelete/"+id,function(result){
			$("#dataTable").treetable("removeNode",id);
		});
	});
}
function doSort(){
	$("#sortForm").ajaxSubmit({
		success:function(data){
			switch(data.flag){
			case -1:
				Kit.alert("系统异常，请重试");return;
			case 0:
				window.location.reload();
			}
		}
	});
}
</script>
</body>
</html>