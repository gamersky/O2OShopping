<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div id="menu-wrapper">
	<div class="navbar-default sidebar" role="navigation">
	    <div class="sidebar-nav navbar-collapse">
	        <ul class="nav" id="side-menu">
	        	<c:forEach items="${navMenus}" var="level1">
	            <li>
	                <c:if test="${level1.children!=null&&fn:length(level1.children)!=0}">
	                <a href="#">${level1.name}<span class="fa arrow"></span></a>
	                <ul class="nav nav-second-level">
	                	<c:forEach items="${level1.children}" var="level2">
	                    <li><a href="${BASE_PATH}${level2.url}">${level2.name}</a></li>
	                	</c:forEach>
	                </ul>
	                </c:if>
	                <c:if test="${level1.children==null||fn:length(level1.children)==0}">
	                <a href="#">${level1.name}</a>
	                </c:if>
	            </li>
	        	</c:forEach>
	        </ul>
		</div>
	</div>
</div>