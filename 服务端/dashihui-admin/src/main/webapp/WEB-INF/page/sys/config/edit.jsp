<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#editForm").validate({
		messages:{
			value: {required: "请输入值"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						editDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="editForm" action="${BASE_PATH}/sys/config/doEdit" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">代码</label>
	    <div class="col-lg-9">
        	<input type="text" name="code" value="${object.code}" class="form-control" placeholder="请输入代码" readonly="readonly">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">值</label>
	    <div class="col-lg-9">
        	<input type="text" name="value" value="${object.value}" class="form-control" placeholder="请输入值" required maxlength="10">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">描述</label>
	    <div class="col-lg-9">
        	<textarea rows="" cols="" name="describe" class="form-control" placeholder="请输入描述信息" maxlength="100">${object.describe}</textarea>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:editDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="configid" value="${object.id}"/>
</form>