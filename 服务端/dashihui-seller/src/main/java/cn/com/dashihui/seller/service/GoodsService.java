package cn.com.dashihui.seller.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.seller.dao.Category;
import cn.com.dashihui.seller.dao.Goods;
import cn.com.dashihui.seller.dao.GoodsTag;

public class GoodsService {
	Logger log = Logger.getLogger(this.getClass());
	
	/**
	 * 获取所有的分类
	 */
	public List<Category> findAllCategory() {
		return Category.me().find("select * from t_dict_category");
	}
	
	/**
	 * 查询商品列表
	 * @param storeid 店铺ID
	 * @param categoryonCode 一级分类代码
	 * @param categorytwCode 二级分类代码
	 * @param pageNum 页码
	 * @param pageSize 数量
	 * @param ORDERBY 排序，0：默认排序，1：销量从高到低，2：价格从低到高，3：价格从高到低
	 */
	public Page<Record> findByPage(int storeid,String goodsType,String categoryonid,String categorytwid,String categorythid,
			String categoryfoid,String state,String keyword,int pageNum,int pageSize,String tagsid){
		String sqlSelect = "SELECT g.id,g.name,g.thumb,g.spec,g.marketPrice,g.sellPrice,g.urv,g.state ";
		StringBuffer sqlFromSelect = new StringBuffer("FROM t_bus_goods g");
		sqlFromSelect.append(" LEFT JOIN t_bus_goods_tag_rel a ON g.id = a.goodsid ");
		
		StringBuffer sqlWhereSelect = new StringBuffer(" WHERE g.storeid=" + storeid);
		//判断如果分类不为空，则添加条件
		if(!StrKit.isBlank(goodsType)){
			sqlWhereSelect.append(" AND g.type in (" + goodsType + ")");
		}
		if(!StrKit.isBlank(categoryfoid)) {
			sqlWhereSelect.append(" AND g.categoryfoid = " + categoryfoid);
		} else if(!StrKit.isBlank(categorythid) && StrKit.isBlank(categoryfoid)) {
			sqlWhereSelect.append(" AND g.categorythid = " + categorythid);
		} else if(!StrKit.isBlank(categorytwid) && StrKit.isBlank(categorythid) && StrKit.isBlank(categoryfoid)) {
			sqlWhereSelect.append(" AND g.categorytwid = " + categorytwid);
		} else if(!StrKit.isBlank(categoryonid) && StrKit.isBlank(categorytwid) && StrKit.isBlank(categorythid) && StrKit.isBlank(categoryfoid)){
			sqlWhereSelect.append(" AND g.categoryonid = " + categoryonid);
		}
		if(!StrKit.isBlank(state)) {
			sqlWhereSelect.append(" AND g.state = " + state);
		}
		if(!StrKit.isBlank(keyword)) {
			sqlWhereSelect.append(" AND g.name like '%" + keyword + "%'");
		}
		if(!StrKit.isBlank(tagsid)) {
			sqlWhereSelect.append(" AND a.tagid in (" + tagsid + ")");
		}
		sqlWhereSelect.append(" ORDER BY g.createDate DESC");
		return Db.paginate(pageNum, pageSize, sqlSelect, sqlFromSelect.append(sqlWhereSelect).toString());
	}
	
	/**
	 * 通过商品ID查找出商品的信息
	 * @param goodsid 商品ID
	 * @return
	 */
	public Goods findById(int goodsid) {
		return Goods.me().findFirst("SELECT * FROM t_bus_goods g WHERE g.id=?", goodsid);
	}
	
	/**
	 * 查询所有可用的商品标签，并判断指定商品有哪些已经选中
	 * @param goodsid
	 */
	public List<GoodsTag> getAllEnabledTag(int goodsid){
		if(goodsid==0){
			return GoodsTag.me().find("SELECT A.id,A.tagName,A.code,0 checked FROM t_bus_goods_tag A WHERE A.enabled=1 ORDER BY A.orderNo");
		}else{
			return GoodsTag.me().find("SELECT A.id,A.tagName,A.code,(CASE WHEN B.goodsid IS NULL THEN 0 ELSE 1 END) checked FROM t_bus_goods_tag A LEFT JOIN t_bus_goods_tag_rel B ON A.id=B.tagid AND B.goodsid=? WHERE A.enabled=1 ORDER BY A.orderNo",goodsid);
		}
	}
	
	/**
	 *修改便利店商品
	 */
	@Before(Tx.class)
	public boolean update(Goods goods,String[] tags){
		//更新商品信息
		goods.update();
		List<String> sqlList = new ArrayList<String>();
		//删除所有商品标签关联
		int batchSize = 1;
		sqlList.add("DELETE FROM t_bus_goods_tag_rel WHERE goodsid="+goods.getStr("id"));
		//保存相应的商品标签关联
		if(tags!=null&&tags.length!=0){
			batchSize = batchSize + tags.length;
			for(String tagid : tags){
				sqlList.add("INSERT INTO t_bus_goods_tag_rel(goodsid,tagid) VALUES("+goods.getStr("id")+","+tagid+")");
			}
		}
		Db.batch(sqlList,batchSize);
		return true;
	}
	
}
