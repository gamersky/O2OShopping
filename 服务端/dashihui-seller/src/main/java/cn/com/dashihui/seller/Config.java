package cn.com.dashihui.seller;

import java.util.Map;

import com.google.common.collect.Maps;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;

import cn.com.dashihui.seller.controller.AppApiController;
import cn.com.dashihui.seller.controller.GoodsController;
import cn.com.dashihui.seller.controller.OrderController;
import cn.com.dashihui.seller.controller.SystemController;
import cn.com.dashihui.seller.dao.Category;
import cn.com.dashihui.seller.dao.Goods;
import cn.com.dashihui.seller.dao.GoodsTag;
import cn.com.dashihui.seller.dao.Order;
import cn.com.dashihui.seller.dao.OrderList;
import cn.com.dashihui.seller.dao.OrderRefund;
import cn.com.dashihui.seller.dao.Seller;
import cn.com.dashihui.seller.dao.User;
import cn.com.dashihui.seller.handler.ContextParamsHandler;
import cn.com.dashihui.seller.interceptor.AuthLoginInterceptor;

public class Config extends JFinalConfig {
	public void configConstant(Constants me) {
		String config = "config.properties";
		// 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
		loadPropertyFile(config);
		//装载进PropKit中
		PropKit.use(config);
		//设置视图根目录
		me.setBaseViewPath("/WEB-INF/page");
	    //设置字符集
	    me.setEncoding("UTF-8");
	    me.setViewType(ViewType.JSP);
	    //调试模式，会打印详细日志
		me.setDevMode(getPropertyToBoolean("constants.devMode", false));
		//出错跳转页面
		me.setError401View("/WEB-INF/page/401.jsp");
		me.setError403View("/WEB-INF/page/403.jsp");
		me.setError404View("/WEB-INF/page/404.jsp");
		me.setError500View("/WEB-INF/page/500.jsp");
	}
	
	public void configRoute(Routes me) {
		me.add("/", SystemController.class);
		me.add("/order", OrderController.class);
		me.add("/goods", GoodsController.class);
		me.add("/api", AppApiController.class);
	}
	
	public void configPlugin(Plugins me) {
		//缓存
		EhCachePlugin ecp = new EhCachePlugin();
		me.add(ecp);
		// 配置C3p0数据库连接池插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("db.jdbcUrl"), getProperty("db.username"), getProperty("db.password"), getProperty("db.jdbcDriver"));
		c3p0Plugin.setMaxPoolSize(getPropertyToInt("db.maxPoolSize"));
		c3p0Plugin.setMinPoolSize(getPropertyToInt("db.minPoolSize"));
		c3p0Plugin.setInitialPoolSize(getPropertyToInt("db.initialPoolSize"));
		c3p0Plugin.setMaxIdleTime(getPropertyToInt("db.maxIdleTime"));
		c3p0Plugin.setAcquireIncrement(getPropertyToInt("db.acquireIncrement"));
		me.add(c3p0Plugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.setDialect(new MysqlDialect());
		//忽略大小写
		//arp.setContainerFactory(new CaseInsensitiveContainerFactory());
		arp.setShowSql(true);
		me.add(arp);
		//添加model映射
		//用户表
		arp.addMapping("t_dict_store_seller", Seller.class);
		//商品订单系列
		arp.addMapping("t_bus_goods", Goods.class);
		arp.addMapping("t_bus_order", Order.class);
		arp.addMapping("t_bus_order_list", OrderList.class);
		arp.addMapping("t_dict_category", Category.class);
		arp.addMapping("t_bus_goods_tag", GoodsTag.class);
		arp.addMapping("t_bus_order_refund", OrderRefund.class);
		arp.addMapping("t_bus_user", User.class);
	}
	
	public void configInterceptor(Interceptors me) {
		me.add(new SessionInViewInterceptor());
		//登录状态拦截器
		me.add(new AuthLoginInterceptor());
	}
	
	public void configHandler(Handlers me) {
		//可在此设置context_path，解决http://ip:port/context_path的问题
		//因为测试时是在jetty下，所以默认没有context_path，如果部署在tomcat下，会自动加上项目名，所以会用到该配置
		//可自定义context_path，默认下是CONTEXT_PATH，使用如：${CONTEXT_PATH}
		me.add(new ContextPathHandler("BASE_PATH"));
		//需要在页面传递的常量
		Map<String,Object> params = Maps.newHashMap();
		params.put("FTP_PATH", PropKit.get("constants.ftppath"));
		params.put("URL_PATH", PropKit.get("constants.urlpath"));
		me.add(new ContextParamsHandler(params));
	}
}
