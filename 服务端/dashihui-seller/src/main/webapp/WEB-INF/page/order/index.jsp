<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.seller.common.OrderCode,cn.com.dashihui.seller.common.SellerState.DispatchState" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<c:choose>
		<c:when test="${type==0}">
		<title>待接单</title>
		</c:when>
		<c:when test="${type==1}">
		<title>未完成</title>
		</c:when>
		<c:when test="${type==2}">
		<title>已完成</title>
		</c:when>
		</c:choose>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
	</head>
	<body class="app-navbar-body">
		<header id="top" class="am-header app-header">
			<c:choose>
			<c:when test="${type==0}">
			<h1 class="am-header-title app-title">待接单</h1>
			</c:when>
			<c:when test="${type==1}">
			<h1 class="am-header-title app-title">未完成</h1>
			</c:when>
			<c:when test="${type==2}">
			<h1 class="am-header-title app-title">已完成</h1>
			</c:when>
			</c:choose>
			<c:if test="${type==0}">
			<div id="refreshBtn" class="am-header-right am-header-nav app-title">
				<span>刷新</span>
			</div>
			</c:if>
		</header>
		<div data-am-sticky>
			<ul class="am-avg-sm-3 app-tab">
				<li <c:if test="${type==0}">class="active"</c:if>>
					<a href="${BASE_PATH}/order/index" <c:if test="${type!=0}">class="rdriver"</c:if>>
						<span>待接单</span>
					</a>
				</li>
				<li <c:if test="${type==1}">class="active"</c:if>>
					<a href="${BASE_PATH}/order/index?type=1" <c:if test="${type==0}">class="l-driver r-driver"</c:if><c:if test="${type==2}">class="r-driver"</c:if>>
						<span>未完成</span>
					</a>
				</li>
				<li <c:if test="${type==2}">class="active"</c:if>>
					<a href="${BASE_PATH}/order/index?type=2" <c:if test="${type==1}">class="l-driver"</c:if>>
						<span>已完成</span>
					</a>
				</li>
			</ul>
		</div>
		
		<div id="list"></div>
		<div id="listLoadBtnWrapper" class="app-p-10">
			<button id="listLoadBtn" type="button" class="am-btn am-btn-warning am-btn-block" style="display:none;">点击加载更多</button>
		</div>
		<div id="listEmpty" style="display:none;margin-top:15%;">
			<div>
				<img src="${BASE_PATH}/static/app/img/order_empty.png" style="margin:0 auto;display:block;">
				<div class="am-text-center app-m-t-10">没有订单</div>
			</div>
		</div>
		
		<div class="am-navbar am-cf am-no-layout app-navbar">
			<ul class="am-navbar-nav am-cf am-avg-sm-3">
				<li>
					<a href="javascript:void(0);" class="app-active">
						<img src="${BASE_PATH}/static/app/img/menu_order11.png"/>
						<span class="am-navbar-label">订单管理</span>
					</a>
				</li>
				<li class="app-navbar-cart">
					<a href="${BASE_PATH }/goods">
						<img src="${BASE_PATH}/static/app/img/menu_goods02.png"/>
						<span class="am-navbar-label">商品管理</span>
					</a>
				</li>
			</ul>
		</div>
		
		<div data-am-smooth-scroll class="am-gotop am-gotop-fixed" >
			<a href="#top"><span class="am-gotop-title">回到顶部</span><i class="am-gotop-icon am-icon-chevron-up"></i></a>
		</div>
				
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
			{{each list as order}}
			<div id="order{{order.orderNum}}" class="app-list app-list-highlight app-m-t-10">
				<div onclick="toDetail('{{order.orderNum}}')">
					<div class="app-list-item">
						<label>下单时间：</label><span>{{order.createDate}}</span>
					</div>
					<hr/>
					<div class="app-list-item app-m-t-10">
						<ul class="am-avg-sm-3">
							<li class="am-text-center r-driver">
								<h2><span class="am-navbar-label"><i class="am-icon-rmb"></i> {{order.amount}}</span></h2>
								{{if order.payType==<%=OrderCode.OrderPayType.ON_LINE%>}}
								<span class="am-navbar-label app-text-gray">在线支付</span>
								{{else}}
								<span class="am-navbar-label app-text-gray">货到付款</span>
								{{/if}}
							</li>
							<li class="am-text-center r-driver">
								{{if order.takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
								<h2><span class="am-navbar-label">门店配送</span></h2>
								{{else}}
								<h2><span class="am-navbar-label">上门自取</span></h2>
								{{/if}}
								<span class="am-navbar-label app-text-gray">配送方式</span>
							</li>
							<li class="am-text-center">
								{{if order.orderState==<%=OrderCode.OrderState.CANCEL%>}}
								<!-- 状态0：已取消 -->
								<h2><span class="app-state am-navbar-label">已取消</span></h2>
								{{else if order.state==<%=DispatchState.UNACCEPT%>}}
								<!-- 状态1：未接单 -->
								<h2><span class="app-state am-navbar-label">待接单</span></h2>
								{{else if order.state==<%=DispatchState.WAIT_PACK%>}}
								<!-- 状态2：待配货 -->
								<h2><span class="app-state am-navbar-label">待配货</span></h2>
								{{else if order.state==<%=DispatchState.PACKING%>}}
								<!-- 状态3：配货中 -->
								<h2><span class="app-state am-navbar-label">配货中</span></h2>
								{{else if order.state==<%=DispatchState.PACK_FINISH%> && order.takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
								<!-- 状态4：配货完成 且 订单要求门店配送 -->
								<h2><span class="app-state am-navbar-label">配货完成</span></h2>
								{{else if order.state==<%=DispatchState.DISPATCHING%>}}
								<!-- 状态5：配送中 -->
								<h2><span class="app-state am-navbar-label">配送中</span></h2>
								{{else if order.state==<%=DispatchState.WAIT_GET%>}}
								<!-- 状态6：待取货 -->
								<h2><span class="app-state am-navbar-label">待取货</span></h2>
								{{else if order.state==<%=DispatchState.FINISH%>}}
								<!-- 状态7：已完成 -->
								<h2><span class="app-state-finish am-navbar-label">已完成</span></h2>
								{{/if}}
								<span class="am-navbar-label app-text-gray">当前状态</span>
							</li>
						</ul>
					</div>
					<div class="am-cf"></div>
					<hr/>
					<div class="app-list-item app-m-t-10">
						<div class="am-u-sm-8 am-u-md-8 am-u-lg-8">商品</div>
						<div class="am-u-sm-2 am-u-md-2 am-u-lg-2">单价</div>
						<div class="am-u-sm-2 am-u-md-2 am-u-lg-2">数量</div>
					</div>
					{{each order.goodsList as goods}}
					<hr/>
					<div class="app-list-item app-m-t-10">
						<div class="am-u-sm-8 am-u-md-8 am-u-lg-8 am-text-truncate">{{goods.name}}</div>
						<div class="am-u-sm-2 am-u-md-2 am-u-lg-2"><i class="am-icon-rmb"></i> {{goods.price}}</div>
						<div class="am-u-sm-2 am-u-md-2 am-u-lg-2">{{goods.count}}</div>
						<div class="am-cf"></div>
					</div>
					{{/each}}
				</div>
				<hr/>
				{{if order.orderState==<%=OrderCode.OrderState.CANCEL%>}}
				<a href="javascript:doHide('{{order.orderNum}}',true);" class="am-btn am-btn-primary am-radius" style="width:100%;">隐藏</a>
				{{else if order.state==<%=DispatchState.UNACCEPT%>}}
				<!-- 状态1：未接单，显示接单按钮 -->
				<a href="javascript:doAccept('{{order.orderNum}}');" class="am-btn am-btn-primary am-radius" style="width:100%;">接单</a>
				{{else if order.state==<%=DispatchState.WAIT_PACK%>}}
				<!-- 状态2：待配货，显示开始配货按钮 -->
				<a href="javascript:doDeal('doPack','{{order.orderNum}}');" class="am-btn am-btn-secondary am-radius" style="width:45%;">开始配货</a>
				<a href="${BASE_PATH}/order/packTicket/{{order.orderNum}}" class="am-btn am-btn-secondary am-radius am-fr app-m-b-10" style="width:45%;">配货单</a>
				{{else if order.state==<%=DispatchState.PACKING%>}}
				<!-- 状态3：配货中，显示配货完成按钮 -->
				<a href="javascript:doDeal('doPackFinish','{{order.orderNum}}');" class="am-btn am-btn-secondary am-radius" style="width:100%;">配货完成</a>
				{{else if order.state==<%=DispatchState.PACK_FINISH%> && order.takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
				<!-- 状态4：配货完成 且 订单要求门店配送，显示送货按钮 -->
				<a href="javascript:doDeal('doSend','{{order.orderNum}}');" class="am-btn am-btn-warning am-radius" style="width:45%;">送货</a>
				<a href="${BASE_PATH}/order/deliverTicket/{{order.orderNum}}" class="am-btn am-btn-warning am-radius am-fr app-m-b-10" style="width:45%;">出货单</a>
				{{else if order.state==<%=DispatchState.DISPATCHING%> || order.state==<%=DispatchState.WAIT_GET%>}}
				<!-- 状态5：配送中 或 待取货，显示确认收货按钮 -->
				<a href="javascript:doFinish('{{order.orderNum}}');" class="am-btn am-btn-success am-radius" style="width:100%;">确认收货</a>
				{{/if}}
			</div>
			{{/each}}
		</script>
		<script type="text/javascript">
			var type = parseInt('${type}'),pageNum=0,totalPage=1,webSocket,interval = 60000;
			//加载标识，表示当前是否有请求未完成，防止同时多个请求
			var loading = false;
			$(function(){
				if(type==0){
					//加载最新订单
					$("#refreshBtn").show().on("click",function(){
						if(!loading){
							loading = true;
							Kit.ajax.post("${BASE_PATH}/order/page",function(result){
								if(result.flag==0){
									if(result.object.length!=0){
										//有新订单，遍历展示
										$("#list").empty().append(template("dataTpl",{"list":result.object})).show();
									}else{
										//没有新订单
										Kit.ui.toggle("#listEmpty","#list");
									}
									loading = false;
								}
								//开启下一次刷新任务
								setTimeout(function(){
									$("#refreshBtn").trigger("click");
								}, interval);
							});
						}
					}).trigger("click");
				}else{
					Kit.util.onPageEnd(function(){
		               	if(pageNum < totalPage && !loading){
		               		loading = true;
		               		$("#list").append("<div class=\"app-loading\">正在加载</div>");
		               		Kit.ajax.post("${BASE_PATH}/order/page",{pageNum:pageNum+1,pageSize:10,type:type},function(result){
								$("#list").append(template("dataTpl",result.object));
								$(".app-loading","#list").remove();
								pageNum = result.object.pageNumber;
								totalPage = result.object.totalPage;
								//重置加载标识
								loading = false;
							});
		               	}
					});
				}
			});
			function doAccept(orderNum){
				Kit.ajax.post("${BASE_PATH}/order/doAccept",{orderNum:orderNum},function(result){
					if(result.flag==1){
						Kit.ui.confirm("该订单已取消，是否隐藏?",function(){
							doHide(orderNum,false);
						},function(){
							$("#order"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
						});
					}else if(result.flag==0){
						$("#order"+orderNum).remove();
						Kit.render.redirect("${BASE_PATH}/order/index?type=1");
					}else{
						Kit.ui.toast(result.message);
					}
				});
			}
			function doDeal(action,orderNum){
				Kit.ajax.post("${BASE_PATH}/order/"+action,{orderNum:orderNum},function(result){
					if(result.flag==1){
						Kit.ui.confirm("该订单已取消，是否隐藏?",function(){
							doHide(orderNum,false);
						},function(){
							$("#order"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
						});
					}else if(result.flag==0){
						$("#order"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
					}else{
						Kit.ui.toast(result.message);
					}
				});
			}
			function doFinish(orderNum){
				Kit.ajax.post("${BASE_PATH}/order/doFinish",{orderNum:orderNum},function(result){
					if(result.flag==1){
						Kit.ui.confirm("该订单已取消，是否隐藏?",function(){
							doHide(orderNum,false);
						},function(){
							$("#order"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
						});
					}else if(result.flag==0){
						$("#order"+orderNum).remove();
						Kit.render.redirect("${BASE_PATH}/order/index?type=2");
					}else{
						Kit.ui.toast(result.message);
					}
				});
			}
			function toDetail(orderNum){
				Kit.render.redirect("${BASE_PATH}/order/detail/"+orderNum);
			}
			//隐藏订单
			function doHide(orderNum,isNeedConfirm) {
				var doHideCallback = function(){
					Kit.ajax.post("${BASE_PATH}/order/doHide",{orderNum:orderNum}, function(result){
						if(result.flag == 0) {
							$("#order"+orderNum).remove();
						}else{
							Kit.ui.alert(result.message);
						}
					});
				}
				if(isNeedConfirm){
					Kit.ui.confirm("确定要隐藏该订单吗？",doHideCallback);
				}else{
					doHideCallback();
				}
			}
		</script>
	</body>
</html>