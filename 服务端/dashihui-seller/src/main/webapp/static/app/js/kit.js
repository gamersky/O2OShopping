/**
 * 封装一些常用方法<br/>
 * 将mui和jquery等第三方类库的方法进行一个浅封装，目的是减少耦合，方便替换ui和js框架
 */
(function(window){
	window.Kit = {};
	//数据库操作类
	Kit.dao = {
		/**
		 * 保存数据
		 * @param {Object} key
		 * @param {Object} value
		 * @param {Object} objToJsonStr 是否需要转换为json字符串，true或false
		 */
		save : function(key,value,objToJsonStr){
			if(objToJsonStr){
				$.AMUI.store.set(key, Kit.util.toJson(value));
			}else{
				$.AMUI.store.set(key, value);
			}
		},
		/**
		 * 取出数据
		 * @param {Object} key
		 * @param {Object} jsonStrToObj 是否需要将数据转换为对象取出，true或false
		 */
		get : function(key,jsonStrToObj){
			var value = $.AMUI.store.get(key);
			if(jsonStrToObj&&value){
				return Kit.util.fromJson(value);
			}
			return value;
		},
		/**
		 * 删除
		 * @param {Object} key
		 */
		del : function(key){
			$.AMUI.store.remove(key);
		},
		/**
		 * 清空
		 */
		clear : function(){
			$.AMUI.store.clear();
		},
		/**
		 * 验证数据是否存在
		 * @param {Object} key
		 */
		has : function(key){
			var value = Kit.dao.get(key);
			if(value!=null&&typeof(value)!="undefined"){
				return true;
			}
			return false;
		}
	};
	//异步类
	Kit.ajax = {
		/**
		 * post异步请求
		 * @param {Object} url
		 * @param {Object} params
		 * @param {Object} callback
		 */
		post : function(url,params,callback){
			//请求
			$.post(url,params,function(data){
				if(callback)callback(data);
			},"json");
		},
		/**
		 * get异步请求
		 * @param {Object} url
		 * @param {Object} callback
		 */
		get : function(url,callback){
			$.get(url,function(data){
				if(callback)callback(data);
			},"json");
		}
	};
	//界面操作类
	Kit.ui = {
		/**
		 * 显示加载中提示框
		 */
		showLoading : function(){
			window._gvLayerLoading = layer.open({type:2,shadeClose:false});
		},
		/**
		 * 关闭加载中提示框
		 */
		closeLoading : function(){
			if(window._gvLayerLoading>=0){
				layer.close(window._gvLayerLoading);
			}
		},
		/**
		 * alert提示框
		 * @param {Object} message
		 */
		toast : function(message){
			layer.open({
			    content: message,
			    time: 2
			});
		},
		/**
		 * alert提示框，带确定按钮
		 * @param {Object} message
		 */
		alert : function(message,callback){
			layer.open({
			    content: message,
			    btn: ["确定"],
			    yes: function(index){
			    	if(callback){
			    		callback();
			    	}
			    	layer.close(index);
			    }
			});
		},
		/**
		 * 确定提示框
		 * @param {Object} message
		 * @param {Object} onYesCallback 用户确认时回调事件
		 * @param {Object} onNoCallback 用户取消时回调事件
		 */
		confirm : function(message,onYesCallback,onNoCallback){
			layer.open({
			    content: message,
			    btn: ['确认', '取消'],
			    shadeClose: false,
			    yes: function(index){
			    	onYesCallback();
			    	layer.close(index);
			    }, no: function(){
			    	if(onNoCallback)onNoCallback();
			    }
			});
		},
		/**
		 * 切换两个控件显示或隐藏
		 * @param toShow 要显示的控件
		 * @param toHide 要隐藏的控件
		 */
		toggle : function(toShow,toHide){
			if(toShow&&toHide){
				$(toShow).removeClass("app-hide").show();
				$(toHide).addClass("app-hide").hide();
			}
		},
		/**
		 * 以底部弹出的效果展示一段html内容，可自定义样式
		 * @param html 要显示的html内容
		 * @param style 自定义样式
		 */
		popup : function(html,style){
			window._gvLayerPopupWindow = layer.open({
			    type: 1,
			    content: html,
			    style: (Kit.validate.isBlank(style)?"position:fixed; bottom:0; left:0; width:100%; padding:0; border:none; padding:5px;":style)
			});
		},
		/**
		 * 关闭询问弹出层
		 */
		popdown : function(){
			if(window._gvLayerPopupWindow>=0){
				layer.close(window._gvLayerPopupWindow);
			}
		}
	};
	//页面跳转类
	Kit.render = {
		/**
		 * 重定向
		 * @param {Object} href
		 */
		redirect : function(href){
			window.location.href = href;
		},
		/**
		 * 刷新
		 */
		refresh : function(){
			window.location.reload();
		}
	};
	//验证类
	Kit.validate = {
		/**
		 * 验证是否为空，可接收对象或字符串，对象时只验证是否undefined，字符串时再验证是否为空串
		 * @param {Object} obj
		 */
		isBlank : function (obj) {
			if(typeof obj === "undefined"){
				return true;
			}
			if(typeof obj === "string"){
				return obj==="";
			}
			return true;
		}
	};
	//工具类
	Kit.util = {
		/**
		 * 将js对象转换为json格式字符串
		 * @param obj 要转换的JS对象
		 */
		toJson : function(obj){
			return JSON.stringify(obj);
		},
		/**
		 * 将json格式字符串转换为js对象
		 * @param str 要转换的json格式字符串
		 */
		fromJson : function(str){
			return JSON.parse(str);
		},
		/** 
		 * 对日期进行格式化，2015/10/11添加
		 * @param date 要格式化的日期 
		 * @param format 进行格式化的模式字符串
		 *     支持的模式字母有： 
		 *     y:年, 
		 *     M:年中的月份(1-12), 
		 *     d:月份中的天(1-31), 
		 *     h:小时(0-23), 
		 *     m:分(0-59), 
		 *     s:秒(0-59), 
		 *     S:毫秒(0-999),
		 *     q:季度(1-4)
		 * @return String
		 * @author yanis.wang
		 * @see	http://yaniswang.com/frontend/2013/02/16/dateformat-performance/
		 */
		dateFormat : function (dateStr, format){
			var date = new Date(Date.parse(dateStr.replace(/-/g,   "/")));
		    var map = {
		        "M": date.getMonth() + 1, //月份 
		        "d": date.getDate(), //日 
		        "h": date.getHours(), //小时 
		        "m": date.getMinutes(), //分 
		        "s": date.getSeconds(), //秒 
		        "q": Math.floor((date.getMonth() + 3) / 3), //季度 
		        "S": date.getMilliseconds() //毫秒 
		    };
		    format = format.replace(/([yMdhmsqS])+/g, function(all, t){
		        var v = map[t];
		        if(v !== undefined){
		            if(all.length > 1){
		                v = '0' + v;
		                v = v.substr(v.length-2);
		            }
		            return v;
		        }
		        else if(t === 'y'){
		            return (date.getFullYear() + '').substr(4 - all.length);
		        }
		        return all;
		    });
		    return format;
		},
		/** 
		 * 对标识进行转换，2015/10/11添加
		 * @param flag 要转换的标识 
		 * @param 相应可选值1
		 * @param 相应可选值1所对应的中文释义
		 * @param 相应可选值2
		 * @param 相应可选值2所对应的中文释义
		 * @param ......
		 * @return 相应值对应的中文释义
		 * 举例：
		 * var flag = 2;
		 * flagTransform(flag,1,'类型1',2,'类型2',3,'类型3'......);
		 * 返回值为：'类型2'
		 */
		flagTransform : function(flag){
			var argLen = arguments.length;
			if(argLen<1){
				return "NaN";
			}else if(argLen==1||(argLen-1)%2!=0){
				return flag;
			}else{
				for(var i=0;i<argLen-1;){
					if(flag==arguments[i+1]){
						return arguments[i+2];
					}
					i=i+2;
				}
			}
		},
		/**
		 * 页面滚动到底部的事件监听
		 * @param callback 回调
		 * @param offset 要监听的距离底部的距离
		 */
		onPageEnd : function(callback,offset){
			var innerHeight = window.innerHeight, offset = offset || 400;
			$(window).on("scroll",function() {
				var topOffset = document.body.offsetHeight - (document.documentElement.scrollTop||window.pageYOffset||document.body.scrollTop) - innerHeight;
                if(topOffset < offset){
                	if(callback)callback();
				}
            });
			//判断如果页面高度初始化时就比要监听的距离小，或者远高于页面内容时，则立即执行
			var topOffset = document.body.offsetHeight - (document.documentElement.scrollTop||window.pageYOffset||document.body.scrollTop) - innerHeight;
			if(topOffset<=0||innerHeight < offset){
				if(callback)callback();
			}
		},
		/**
		 * 移除滚动到底部的事件监听
		 */
		offPageEnd : function(){
			$(window).off("scroll");
		},
		/**
		 * 空格加字符串拼接
		 * @param spaceCount 空格数量
		 * @param string 字符串
		 */
		splice : function(space, string){
			var content = '';
			var spaceCount = space - string.length;
			for(var a = 0; a < spaceCount; a++){
				content += ' ';
			}
			return content + string;
		}
	};
	//调试类
	Kit.debug = {
		/**
		 * 调试日志
		 * @param {Object} message
		 */
		log : function(message){
			console.log(message);
		}
	}
	//即时执行的页面初始化代码
	//1.设置jQuery初始配置
	if(window.$||window.jQuery){
		//设置jQuery默认配置
		$.ajaxSetup({
			//不开启缓存
			cache:false,
			//默认数据类型为JSON
			dataType:"json",
			//异步发出前统一执行的动作
			beforeSend:function(){
				//记录异步进行中的状态
				window.ajaxing = true;
				//设置1秒后执行
				setTimeout(function(){
					//判断还在异步中时，显示进度条
					if(window.ajaxing){
						Kit.ui.showLoading();
					}
				}, 1000);
			},
			//异步结束后统一执行的动作
			complete:function(response,status){
				//异步结束后，统一判断登录状态
				if(status=="success"){
					if(response.responseJSON&&response.responseJSON.flag==-2){
						//dataType为json时
						window.location.href=BASE_PATH+"/login";
					}else if(response.responseText&&response.responseText.indexOf("\"flag\":-2")==1){
						//dataType不是json时
						window.location.href=BASE_PATH+"/login";
					}else if(response.responseJSON&&response.responseJSON.flag==-3){
						//dataType为json时
						window.location.href=response.responseJSON.object.authUrl;
					}else if(response.responseText&&response.responseText.indexOf("\"flag\":-3")==1){
						//dataType不是json时
						window.location.reload();
					}
				}
				//修改异步状态为false，这样如果异步加载的快的话，将会在beforeSend的判断前修改异步状态为false，这样就不会显示进度条了
				window.ajaxing = false;
				Kit.ui.closeLoading();
			}
		});
	}
	//2.为页面中ID为backBtn的元素绑定后退事件
	if($("#backBtn").length!=0){
		$("#backBtn").on("click",function(){
			window.history.go(-1);
		});
	}
})(window);