var Ticket = function(attrs){
	var size = 1;
	var position = 1;
	var content;
	
	var setSize = function(aSize){
		size = aSize;
	}
	var setPosition = function(aPosition){
		position = aPosition;
	}
	var setContent = function(aContent){
		content = aContent;
	}
	var getContent = function(){
		return content;
	}
	var toString = function(){
		return {size:size,position:position,content:content};
	}
	
	if(attrs.size){
		size = attrs.size;
	}
	if(attrs.position){
		position = attrs.position;
	}
	if(attrs.content){
		content = attrs.content;
	}else{
		throw "content is null";
	}
	
	return {
		setSize : setSize,
		setPosition : setPosition,
		setContent : setContent,
		getContent : getContent,
		toString : toString
	}
}