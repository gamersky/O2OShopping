package cn.com.dashihui.web.controller;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.CommunityDetail;
import cn.com.dashihui.web.service.Community;
import cn.com.dashihui.web.service.CommunityDetailService;
import cn.com.dashihui.web.service.StoreService;

public class CommunityDetailController extends BaseController{
	private static final Logger logger = Logger.getLogger(CommunityDetailController.class);
	private CommunityDetailService communityDetailService = new CommunityDetailService();
	private StoreService storeService = new StoreService();
    
    public void index(){
    	Community community = storeService.findCommunity(getStoreid());
    	setAttr("detailList", communityDetailService.findAllCommunityDetails(community.getInt("id")));
        render("index.jsp");
    }
    
    public void doSort(){
    	Map<String,String[]> params = getParaMap();
    	if(params!=null&&params.size()!=0&&params.containsKey("sortKey")){
    		String sortKey = params.get("sortKey")[0];
    		Map<String,String> sortMap = new HashMap<String,String>();
    		for(String key : params.keySet()){
    			if(!key.equals("sortKey")){
    				String id = key.replace(sortKey, "");
    				String no = params.get(key)[0];
    				if(StrKit.isBlank(no)||no.length()>3){
    					no = "0";
    				}
    				sortMap.put(id, no);
    			}
    		}
    		communityDetailService.sortCommunityDetail(sortMap);
    		renderSuccess();
    		return;
    	}
    	renderFailed();
    }
    
	public void toAdd(){
		int parentid = getParaToInt(0,0);
		if(parentid!=0){
			setAttr("parent", communityDetailService.findById(parentid));
		}
		keepPara("type");
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：名称为空，2：名称过长，3：代码为空，4：代码过长，5：代码已经存在
	 */
	public void doAdd(){
		//名称
		String name = getPara("name");
		//代码
		String code = getPara("code");
		//类型，1-楼号，2-单元号，3-房间门牌号
		int type = getParaToInt("type",1);
		//上级ID
		String parentid = getPara("parentid","0");
		if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(name.length()>20){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(code)){
			renderResult(3);
			return;
		}else if(code.length()>7){
			renderResult(4);
			return;
		}else if(communityDetailService.findByCode(getStoreid(),code)!=null){
			renderResult(5);
			return;
		}else{
	    	Community community = storeService.findCommunity(getStoreid());
			//保存
			CommunityDetail detail = new CommunityDetail()
				.set("communityid", community.getInt("id"))
				.set("type", type)
				.set("name", name)
				.set("code", code)
				.set("parentid", parentid);
			if(communityDetailService.addCommunityDetail(detail)){
				renderSuccess(communityDetailService.findById(detail.getInt("id")));
				return;
			}
		}
		renderFailed();
	}
	
	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			CommunityDetail detail = communityDetailService.findById(id);
			int parentid = detail.getInt("parentid");
			if(parentid!=0){
				setAttr("parent", communityDetailService.findById(parentid));
			}
			setAttr("object", detail);
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：名称为空，2：名称过长，3：代码为空，4：代码过长，5：代码已经存在
	 */
	public void doEdit(){
		//ID
		String detailid = getPara("detailid");
		//名称
		String name = getPara("name");
		//对应的代码
		String code = getPara("code");
		if(StrKit.isBlank(detailid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(name.length()>20){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(code)){
			renderResult(3);
			return;
		}else if(code.length()>7){
			renderResult(4);
			return;
		}
		CommunityDetail findByCode = communityDetailService.findByCode(getStoreid(),code);
		if(findByCode!=null&&findByCode.getInt("id")!=Integer.valueOf(detailid)){
			renderResult(5);
			return;
		}else{
			//更新
			if(communityDetailService.editCommunityDetail(new CommunityDetail()
				.set("id", detailid)
				.set("name", name)
				.set("code", code))){
				renderSuccess(communityDetailService.findById(Integer.valueOf(detailid)));
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&communityDetailService.delCommunityDetail(id)){
			logger.info("删除楼房信息【"+id+"】，及其下属子数据");
			renderSuccess();
			return;
		}renderFailed();
	}
}
