package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.SerShop;

public class SerShopService {
	
	public boolean findExistsByUsername(String username){
		return SerShop.me().findFirst("SELECT * FROM t_bus_ser_shop WHERE username=?",username)!=null;
	}
	
	/**
	 * 分页查找
	 */
	public Page<Record> findByPage(int pageNum, int pageSize, String name){
		StringBuffer sBuffer = new StringBuffer(" FROM t_bus_ser_shop T WHERE 1=1");
		List<Object> params = new ArrayList<Object>();
		if(StrKit.notBlank(name)){
			sBuffer.append(" AND T.name LIKE ?");
			params.add("%"+name+"%");
		}
		sBuffer.append(" ORDER BY T.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT T.*", sBuffer.toString(), params.toArray());
	}
	
	public boolean addShop(SerShop newObject){
		return newObject.save();
	}
	
	public boolean delShop(int id){
		return SerShop.me().deleteById(id);
	}
	
	public boolean editShop(SerShop object){
		return object.update();
	}

	public SerShop findById(int id){
		return SerShop.me().findFirst("SELECT * FROM t_bus_ser_shop WHERE id=?",id);
	}
}
