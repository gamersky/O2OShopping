package cn.com.dashihui.web.controller;

import java.util.List;

import com.jfinal.aop.Duang;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.kit.DatetimeKit;
import cn.com.dashihui.kit.SMSKit;
import cn.com.dashihui.kit.ValidateKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.common.ServiceOrderCode;
import cn.com.dashihui.web.dao.ServiceOrder;
import cn.com.dashihui.web.service.ServiceOrderService;

/**
 * （其他）服务订单管理<br/>
 * 查询的是t_bus_service_order表
 */
public class ServiceOrderController extends BaseController {
	//使用Duang.duang进行封装，使普通类具有事务的功能
	private ServiceOrderService service = Duang.duang(ServiceOrderService.class);

	/**
	 * 订单列表页面
	 */
	public void index(){
		render("index.jsp");
	}

	/**
	 * 获得订单列表数据
	 */
	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		//订单编号
		String orderNum = getPara("orderNum");
		//下单时间范围
		String beginDate = getPara("beginDate");
		String endDate = getPara("endDate");
		//订单状态
		int state = getParaToInt("state",0);
		//买家地址
		String address = getPara("address");
		//买家电话
		String tel = getPara("tel");
		//支付方式
		int payType=getParaToInt("payType",0);
		renderResult(0,service.findByPage(pageNum,pageSize,getStoreid(),orderNum,beginDate,endDate,state,address,tel,payType));
	}

	/**
	 * 查询订单详情
	 */
	public void detail(){
		String orderNum = getPara("orderNum");
		ServiceOrder order = service.getOrderByOrderNum(orderNum);
		if(StrKit.notNull(order)){
			setAttr("obj", order);
			setAttr("serviceList", service.getServiceListByOrderNum(orderNum));
			setAttr("logList", service.getLogListByOrderNum(orderNum));
		}
		render("detail.jsp");
	}

	/**
	 * 为订单派单
	 */
	public void doDeliver(){
		String orderNum = getPara("orderNum");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else{
			ServiceOrder order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(2);
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=ServiceOrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许派单");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“已付款”，派单状态为“未派单”时可派单
				if(payType==ServiceOrderCode.OrderPayType.ON_LINE){
					if(payState!=ServiceOrderCode.OrderPayState.HAD_PAY||deliverState!=ServiceOrderCode.OrderDispatchState.NO_DISPATCH){
						renderFailed("此订单当前不允许派单");
						return;
					}
				}else
				//2.3.服务后付款，派单状态为“未派单”时可派单
				if(payType==ServiceOrderCode.OrderPayType.AFTER_SERVICE){
					if(deliverState!=ServiceOrderCode.OrderDispatchState.NO_DISPATCH){
						renderFailed("此订单当前不允许派单");
						return;
					}
				}
				//3.订单派单操作，并记录日志
				order.set("deliverState", ServiceOrderCode.OrderDispatchState.HAD_DISPATCH).set("deliverDate", DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss"));
				if(service.update(order)){
					//查询对应的服务项清单
					List<Record> serviceItemList = service.getServiceListByOrderNum(orderNum);
					order.put("serviceList", serviceItemList);
					//记录操作日志
					service.log(orderNum, "管理员："+getCurrentUser().getStr("username"), "订单派单", "订单号："+orderNum+"，派单操作成功！");
					//遍历服务项列表（其实只会有一个服务项，但是是按商品清单列表作的，所以是集合）
					for(Record serItem : serviceItemList){
						//向用户发送短信通知，需要判断如果用户的电话是手机号码
						if(ValidateKit.Mobile(order.getStr("tel"))){
							SMSKit.onStoreDispatchToCustomer(order.getStr("tel"), serItem.getStr("shopName"), serItem.getStr("shopTel"));
						}
						//向商家发送短信通知，需要判断如果商家的电话是手机号码
						if(!StrKit.isBlank(serItem.getStr("shopTel"))&&ValidateKit.Mobile(serItem.getStr("shopTel"))){
							SMSKit.onStoreDispatchToShop(serItem.getStr("shopTel"), order.getStr("linkName"), order.getStr("tel"), serItem.getStr("serviceTitle"), DatetimeKit.getFormatDate(order.getDate("serTime"), "yyyy年MM月dd日HH时mm分"), order.getStr("address"));
						}
					}
					renderSuccess(order);
					return;
				}
				//4.其他情况
				renderFailed("订单派单失败");
				return;
			}
		}
	}

	/**
	 * 确认完成订单
	 */
	public void doSign(){
		String orderNum = getPara("orderNum");
		if(StrKit.isBlank(orderNum)){
			renderResult(1);
			return;
		}else{
			ServiceOrder order = service.getOrderByOrderNum(orderNum);
			if(order==null){
				renderResult(2);
				return;
			}else{
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=ServiceOrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许确认");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int deliverState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“已付款”，派单状态为“已派单”时可确认
				if(payType==ServiceOrderCode.OrderPayType.ON_LINE){
					if(payState!=ServiceOrderCode.OrderPayState.HAD_PAY||deliverState!=ServiceOrderCode.OrderDispatchState.HAD_DISPATCH){
						renderFailed("此订单当前不允许确认");
						return;
					}
				}else
				//2.3.服务后付款，派单状态为“已派单”时可派单
				if(payType==ServiceOrderCode.OrderPayType.AFTER_SERVICE){
					if(deliverState!=ServiceOrderCode.OrderDispatchState.HAD_DISPATCH){
						renderFailed("此订单当前不允许确认");
						return;
					}
				}
				//3.订单收货操作，并记录日志
				String datetime = DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss");
				order.set("orderState", ServiceOrderCode.OrderState.FINISH).set("signDate", datetime);
				if(service.update(order)){
					//查询对应的服务项清单
					order.put("serviceList", service.getServiceListByOrderNum(orderNum));
					//记录操作日志
					service.log(orderNum, "管理员："+getCurrentUser().getStr("username"), "订单确认服务完成", "订单号："+orderNum+"，确认服务完成操作成功！");
					renderSuccess(order);
					return;
				}
				//4.其他情况
				renderFailed("确认失败");
				return;
			}
		}
	}
}