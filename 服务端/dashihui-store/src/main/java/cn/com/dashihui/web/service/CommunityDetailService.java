package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;

import cn.com.dashihui.web.dao.CommunityDetail;

public class CommunityDetailService {
	
	public boolean sortCommunityDetail(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_dict_community_detail SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	public boolean addCommunityDetail(CommunityDetail newObject){
		return newObject.save();
	}
	
	public boolean delCommunityDetail(int id){
		CommunityDetail curr = findById(id);
		List<String> sqlList = new ArrayList<String>();
		//删除相应记录
		sqlList.add("DELETE FROM t_dict_community_detail WHERE id="+id);
		if(curr.getInt("type")==2){
			//当前要删除记录如果是单元，则需要将其下属所有房间
			sqlList.add("DELETE FROM t_dict_community_detail WHERE parentid="+id);
		}else if(curr.getInt("type")==1){
			//当前要删除记录如果是楼幢号，则需要将其下属单元
			sqlList.add("DELETE FROM t_dict_community_detail WHERE parentid="+id);
			//及各单元下属房间
			List<CommunityDetail> floorList = CommunityDetail.me().find("SELECT * FROM t_dict_community_detail WHERE parentid=?",id);
			if(floorList!=null){
				for(CommunityDetail item : floorList){
					sqlList.add("DELETE FROM t_dict_community_detail WHERE parentid="+item.getInt("id"));
				}
			}
		}
		Db.batch(sqlList, sqlList.size());
		return true;
	}
	
	public boolean editCommunityDetail(CommunityDetail object){
		return object.update();
	}
	
	public CommunityDetail findById(int id){
		return CommunityDetail.me().findFirst("SELECT * FROM t_dict_community_detail WHERE id=?",id);
	}
	
	public CommunityDetail findByCode(int storeid,String code){
		return CommunityDetail.me().findFirst("SELECT * FROM t_dict_community_detail WHERE storeid=? AND code=?",storeid,code);
	}
	
	public List<CommunityDetail> findAllCommunityDetails(int communityid){
		return trim(CommunityDetail.me().find("SELECT * FROM t_dict_community_detail c WHERE communityid=? ORDER BY c.parentid,c.orderNo",communityid));
	}
	
	/**
	 * 将查询出来的小区楼幢、单元、房间信息列表，整理成嵌套形式
	 */
	private List<CommunityDetail> trim(List<CommunityDetail> allList){
		if(allList!=null){
			List<CommunityDetail> type1List = new ArrayList<CommunityDetail>(), type2List = new ArrayList<CommunityDetail>(), type3List = new ArrayList<CommunityDetail>();
			for(CommunityDetail item : allList){
				if(item.getInt("type")==1){
					type1List.add(item);
				}else if(item.getInt("type")==2){
					type2List.add(item);
				}else if(item.getInt("type")==3){
					type3List.add(item);
				}
			}
			if(type3List.size()!=0){
				for(CommunityDetail r3 : type3List){
					for(CommunityDetail r2 : type2List){
						if(r3.getInt("parentid").intValue()==r2.getInt("id").intValue()){
							r2.addChild(r3);
							break;
						}
					}
				}
			}
			if(type2List.size()!=0){
				for(CommunityDetail r2 : type2List){
					for(CommunityDetail r1 : type1List){
						if(r2.getInt("parentid").intValue()==r1.getInt("id").intValue()){
							r1.addChild(r2);
							break;
						}
					}
				}
			}
			return type1List;
		}
		return null;
	}
}
