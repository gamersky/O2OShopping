package cn.com.dashihui.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.jfinal.aop.Duang;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.jfinal.upload.UploadFile;

import cn.com.dashihui.kit.DirKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.ServiceCategory;
import cn.com.dashihui.web.service.ServiceCategoryService;

public class ServiceCategoryController extends BaseController{
	private static final Logger logger = Logger.getLogger(ServiceCategoryController.class);
	private ServiceCategoryService service = Duang.duang(ServiceCategoryService.class);

    public void index(){
        render("index.jsp");
    }

	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,service.findByPage(pageNum, pageSize, getStoreid()));
	}
	
	public void doSort(){
    	Map<String,String[]> params = getParaMap();
    	if(params!=null&&params.size()!=0&&params.containsKey("sortKey")){
    		String sortKey = params.get("sortKey")[0];
    		Map<String,String> sortMap = new HashMap<String,String>();
    		for(String key : params.keySet()){
    			if(!key.equals("sortKey")){
    				String id = key.replace(sortKey, "");
    				String no = params.get(key)[0];
    				if(StrKit.isBlank(no)||no.length()>3){
    					no = "0";
    				}
    				sortMap.put(id, no);
    			}
    		}
    		service.sortCategory(sortMap);
    		renderSuccess();
    		return;
    	}
    	renderFailed();
    }

	public void toAdd(){
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：分类为空，2：代码为空，3：代码已存在，4：图标为空，5：图标上传失败
	 */
	public void doAdd(){
		UploadFile icon = getFile("icon");
		//分类名
		String name = getPara("name");
		//标签代码
		String code = getPara("code");
		//是否显示
		int isShow = getParaToInt("isShow",1);
		//是否可用
		int enabled = getParaToInt("enabled",1);
		if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(StrKit.isBlank(code)){
			renderResult(2);
			return;
		}else if(service.findByCode(getStoreid(),code)!=null){
			renderResult(3);
			return;
		}else if(icon==null){
			renderResult(4);
			return;
		}else{
			ServiceCategory category = new ServiceCategory()
				.set("name", name)
				.set("code", code)
				.set("isShow", isShow)
				.set("storeid", getStoreid())
				.set("enabled", enabled);
			//分类的图标，则上传至FTP，并记录图片文件名
			String dir = DirKit.getDir(DirKit.ICON);
			String thumbFileName;
			try {
				thumbFileName = uploadToFtp(dir,icon);
			} catch (IOException e) {
				e.printStackTrace();
				logger.debug("图片上传失败");
				renderResult(5);
				return;
			}
			category.set("icon", dir.concat(thumbFileName));
			//保存
			if(service.addCategory(category)){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}

	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：分类为空，2：代码为空，3：代码已存在
	 */
	public void doEdit(){
		UploadFile icon = getFile("icon");
		//分类ID
		String categoryid = getPara("categoryid");
		//分类名
		String name = getPara("name");
		//标签代码
		String code = getPara("code");
		//是否显示
		int isShow = getParaToInt("isShow",1);
		//是否可用
		int enabled = getParaToInt("enabled",1);
		if(StrKit.isBlank(categoryid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(StrKit.isBlank(code)){
			renderResult(2);
			return;
		}
		ServiceCategory findByCode = service.findByCode(getStoreid(),code);
		if(findByCode!=null&&findByCode.getInt("id")!=Integer.valueOf(categoryid)){
			renderResult(3);
			return;
		}else{
			ServiceCategory category = new ServiceCategory()
				.set("id", categoryid)
				.set("name", name)
				.set("code", code)
				.set("isShow", isShow)
				.set("enabled", enabled);
			//如果新上传了分类图标，则上传至FTP，并记录图片文件名
			if(icon!=null){
				String dir = DirKit.getDir(DirKit.ICON);
				String thumbFileName;
				try {
					thumbFileName = uploadToFtp(dir,icon);
				} catch (IOException e) {
					e.printStackTrace();
					logger.debug("图片上传失败");
					renderResult(5);
					return;
				}
				category.set("icon", dir.concat(thumbFileName));
			}
			//更新
			if(service.editCategory(category)){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		String code = getPara(0);
		if(!StrKit.isBlank(code)&&service.delCategory(code)){
			logger.info("删除分类【"+code+"】");
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
