<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#addForm").validate({
		messages:{
			name: {required: "请输入标签名"},
			code: {required: "请输入标签代码"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 3:
						Kit.alert("代码已经存在");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						addDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="addForm" action="${BASE_PATH}/goods/tag/doAdd" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">标签名</label>
	    <div class="col-lg-9">
        	<input type="text" name="tagName" value="" class="form-control" placeholder="请输入标签名" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">代码</label>
	    <div class="col-lg-9">
        	<input type="text" name="code" value="" class="form-control" placeholder="请输入标签代码" required maxlength="3">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">显示</label>
	    <div class="col-lg-9">
	        <ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="isShow" value="1" checked> 是</li>
	    		<li><input type="radio" class="iCheck" name="isShow" value="0"> 否</li>
	    	</ul>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">可用</label>
	    <div class="col-lg-9">
	        <ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="enabled" value="1" checked> 是</li>
	    		<li><input type="radio" class="iCheck" name="enabled" value="0"> 否</li>
	    	</ul>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>