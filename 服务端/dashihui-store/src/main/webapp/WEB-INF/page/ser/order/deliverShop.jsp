<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<form class="form-horizontal">
   <div class="table-responsive" style="min-height:300px;">
		<table  class="table table-hover">
			<thead>
				<tr>
					<th>商家名称</th>
					<th>详细地址</th>
				</tr>
			</thead>
			<tbody>
			<c:if test="${shopList!=null&&fn:length(shopList)!=0}">
				<c:forEach items="${shopList}" var="shop">
					<tr>
						<td><input type="radio" class="iCheck" name="shopid" data-text="${shop.name}" value="${shop.id}" <c:if test="${shop.checked==1}">checked</c:if> <c:if test="${shop.isWork==0}">disabled</c:if>> ${shop.name}<c:if test="${shop.isWork==0}"><span class="text-danger">（休息中）</span></c:if></td>
						<td>${shop.address}</td>
					</tr>
	            </c:forEach>
               </c:if>
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="button" onclick="onSumbit()" autocomplete="off">确定</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:deliverDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>
<script type="text/javascript">
function onSumbit(){
	var shop = $("input[name='shopid']:checked");
	var shopid = shop.val();
	var shopName= shop.data("text");
	$.post("${BASE_PATH}/ser/order/doDeliver",{'orderNum':'${orderNum}','proSerid':shopid,'proSerName':shopName},function(result){
		if(result.flag == 0){
			onDeliverSuccess(result.object);
		}else{
			Kit.alert("派单失败");
		}
	});
}
</script>