<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#addForm").validate({
		rules:{
			tel: {isTelOrMobile: true}
		},
		messages:{
			name: {required: "请输入商家名称",maxLength:"商家名称最多只能填写100个字符"},
			comment:{maxLength:"商家简介最多只能填写500个字符"},
			address:{required: "请输入商家具体地址"},
			tel: {isTelOrMobile: "请输入座机或手机号码"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
						case -1:
							Kit.alert("系统异常，请重试");return;
						case 0:
							dataPaginator.loadPage(1);
							addDialog.close();
					}
				}
			});
		}
	});
});
</script>
<form id="addForm" action="${BASE_PATH}/service/shop/doAdd" method="post" class="form-horizontal" enctype="multipart/form-data">
	<div class="form-group">
	    <label class="col-lg-2 control-label">商家名称</label>
	    <div class="col-lg-9">
	       	<input type="text" name="name"  class="form-control" placeholder="请输入商家名称" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
		<label class="col-lg-2 control-label">商家照片</label>
		<div class="col-lg-9">
			<input type="file" id="thumb" name="thumb" accept="image/jpg" title="选择文件">
		</div>
		<div class="col-lg-offset-2 p-t-5 col-lg-9"><span class="text-success">建议640px*300px,大小60K以内，支持JPEG、PNG格式</span></div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">地址</label>
	    <div class="col-lg-9">
    		<input type="text" name="address" value="" class="form-control" placeholder="请输入商家地址" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">电话</label>
	    <div class="col-lg-9">
        	<input type="text" name="tel" value="" class="form-control" placeholder="请输入电话" maxlength="13">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">是否上架</label>
	    <div class="col-lg-9">
	    	<ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="state" value="1" checked>&nbsp;上架</li>
	    		<li><input type="radio" class="iCheck" name="state" value="2" >&nbsp;下架</li>
	    	</ul>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">商家简介</label>
	    <div class="col-lg-9">
	    	<textarea name="comment" class="form-control" placeholder="商家简介" maxlength="500" rows="4"></textarea>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>