<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>管理平台</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'/>
	<!--[if lt IE 9]>
	<script src='${BASE_PATH}/static/js/html5shiv.js' type='text/javascript'></script>
	<script src='${BASE_PATH}/static/js/respond.min.js' type='text/javascript'></script>
	<![endif]-->
	<link href='${BASE_PATH}/static/plugins/bootstrap/css/bootstrap.min.css' media='all' rel='stylesheet' type='text/css' />
	<!-- ui -->
	<link href='${BASE_PATH}/static/css/ui.css' media='all' rel='stylesheet' type='text/css' />
	<!-- 自定义 -->
	<link href='${BASE_PATH}/static/css/custom.css' media='all' rel='stylesheet' type='text/css' />
</head>
<body class='login-wrap'>
<div class="login-wrap-box text-center">
	<div>
		<div><img src="${BASE_PATH}/static/images/logo_big.png" width="300"/></div>
		<form id="loginForm" role="form" action="<c:url value="/doLogin"/>" method="post">
			<div class="form-group">
				<input type="text" class="form-control" name="username" placeholder="用户名" required maxlength="20" value="${username}">
			</div>
			<div class="form-group">
				<input type="password" class="form-control" name="password" placeholder="密码" required maxlength="50">
			</div>
			<button type="submit" class="btn btn-primary login-wrap-btn-submit">登 录</button>
			<p class="pull-right"> <a href="javascript:void(0);"><small>忘记密码了？</small></a></p>
			${token}
		</form>
	</div>
</div>
<!-- jQuery -->
<script src='${BASE_PATH}/static/plugins/jquery/jquery.min.js' type='text/javascript'></script>
<!-- jQuery验证 -->
<script src='${BASE_PATH}/static/plugins/jquery-validate/jquery.validate.js' type='text/javascript'></script>
<script src='${BASE_PATH}/static/plugins/jquery-validate/messages_zh.min.js' type='text/javascript'></script>
<!-- jQuery form -->
<script src='${BASE_PATH}/static/plugins/jquery-form/jquery.form.js' type='text/javascript'></script>
<!-- layer弹出工具 -->
<script src='${BASE_PATH}/static/plugins/layer/layer.js' type='text/javascript'></script>
<!-- 自定义 -->
<script src='${BASE_PATH}/static/js/custom-kit.js' type='text/javascript'></script>
<script src='${BASE_PATH}/static/js/custom.js' type='text/javascript'></script>
<script type="text/javascript">
$("#loginForm").validate({
	messages:{
		username: {required: "请输入用户名"},
		password: {required: "请输入密码"}
	},
	submitHandler:function(form){
		$(form).ajaxSubmit({
			success:function(data){
				switch(data.flag){
				case 4:
					Kit.alert("用户名或密码错误，请重新输入");return;
				case -1:
					Kit.alert("系统异常，请刷新后重新登录");return;
				case 0:
					window.location.href = "<c:url value="/index"/>";
				}
			}
		});
	}
});
</script>
</body>
</html>