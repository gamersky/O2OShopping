(function($){
	//初始化左侧菜单，并读取当前location，并与左侧菜单进行一一匹配，并设置相应的菜单为active的样式
	if($('#side-menu').length==1&&$.fn["metisMenu"]){
		//store平台左侧菜单固定，不再进行左侧菜单初始化
		//$('#side-menu').metisMenu();
		//读取当前location，并与左侧菜单进行一一匹配，并设置相应的菜单为active的样式
		var url = window.location.href;
		if(url.substring(url.length-1)=='#'){
			url = url.substring(0,url.length-1);
		}
		var element = $('ul.nav a').filter(function() {
			return this.href == url || url.indexOf(this.href) == 0;
		}).addClass('active').parent().parent().addClass('in').parent();
		if (element.is('li')) {
			element.addClass('active');
		}
	};
    //控制当页面宽度变化时，隐藏或显示左侧菜单
	if($("#menu-wrapper").length!=0){
		$(window).bind("load resize", function() {
			topOffset = 50, leftOffset = 250, bottomOffset = 35;
			width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
			height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
			if (width < 768) {
				topOffset = 100; // 2-row-menu
				var contentHeight = height-topOffset-bottomOffset;
				$('div.navbar-collapse').addClass('collapse');
				//屏幕变窄时，设置左菜单隐藏，并隐藏菜单容器的高度
				$("#menu-wrapper").css({"height":"auto"});
				$("#page-wrapper").css({"width":"100%"});
			} else {
				var contentHeight = height-topOffset-bottomOffset;
				$('div.navbar-collapse').removeClass('collapse');
				//屏幕变窄时，设置左菜单显示，并设置菜单容器的高度
				$("#menu-wrapper").css({"height":contentHeight+"px","overflowY":"scroll"});
				$("#page-wrapper").css({"width":(width-leftOffset)+"px"});
			}
			//页面变化时，设置右内容容器的高度
			$("#page-wrapper").height(contentHeight).css("overflowY","scroll");
		});
	}
	//设置jQuery Validation插件初始化方法
	$.validator.setDefaults({
		highlight: function (element) {
			$(element).closest('.form-group').find(".control-label:first").addClass("jqerror-control-label");
			$(element).addClass('jqerror-form-control');
		},
		unhighlight: function (element) {
			$(element).closest('.form-group').find(".control-label:first").removeClass("jqerror-control-label");
			$(element).removeClass('jqerror-form-control');
		},
		errorElement: "span",
		errorClass: "help-block m-b-none jqerror-span",
		errorPlacement: function(error, element) {
			if(element.data("errorPlace")){
				error.appendTo($(element.data("errorPlace")));
			}else{
				error.insertAfter(element);
			}
		}
	});
	//设置jQuery默认值
	$.ajaxSetup({
		//不开启缓存
		cache:false,
		//默认数据类型为JSON
		dataType:"json",
		//异步发出前统一执行的动作
		beforeSend:function(){
			Kit.showLoading();
		},
		//异步结束后统一执行的动作
		complete:function(response,status){
			//异步结束后，统一判断登录状态
			if(status=="success"){
				if(response.responseJSON&&response.responseJSON.flag==-2){
					//dataType为json时
					window.location.href=BASE_PATH+"/login";
				}else if(response.responseText&&response.responseText.indexOf("\"flag\":-2")==1){
					//dataType不是json时
					window.location.href=BASE_PATH+"/login";
				}
			}
			Kit.closeLoading();
		}
	});
	//初始化UI
	Kit.initUI();
})(jQuery,window,document);