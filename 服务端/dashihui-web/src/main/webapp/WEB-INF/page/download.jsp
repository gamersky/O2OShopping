<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mtag" uri="mytag" %>
<!doctype html>
<html>
<head>
	<title>大实惠云商-中国领先的智慧社区服务平台</title>
	<meta name="Keywords" content="河南大实惠,大实惠云商,社区O2O,O2O创业，智慧社区,社区便利店,便利店加盟,连锁便利店,河南便利店"/>
	<meta name="Description" content="河南大实惠电子商务有限公司是一家突破型电商平台，通过社区O2O模式，为小区业主提供更便捷、更安全、更舒适的生活服务体验，大实惠云商用户通过大实惠云商APP其他社区业主沟通和分享、发起交易、组织社区活动等，全面成为社区居民的智慧生活管家！"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'/>
	<script type="text/javascript">window.BASE_PATH = '${BASE_PATH}';</script>
	<script type="text/javascript" src="${BASE_PATH}/static/js/jquery.min.js"></script>
	<style type="text/css">
	.tip_wrap{position:absolute; width:100%; height:100%; top:0; left:0; background:#FFFFFF;}
	.wxtip{position:absolute; width:100%; top:0; left:0;}
	</style>
</head>
<body>
<div id="content">
<script type="text/javascript">
    //智能机浏览器版本信息:
	$(function(){
        var browser = {
            versions: function() {
                var u = navigator.userAgent, app = navigator.appVersion;
                return {//移动终端浏览器版本信息
                    trident: u.indexOf('Trident') > -1, //IE内核
                    presto: u.indexOf('Presto') > -1, //opera内核
                    webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
                    gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
                    mobile: !!u.match(/AppleWebKit.*Mobile.*/) || !!u.match(/AppleWebKit/), //是否为移动终端
                    ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
                    android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
                    iPhone: u.indexOf('iPhone') > -1 || u.indexOf('Mac') > -1, //是否为iPhone或者QQHD浏览器
                    iPad: u.indexOf('iPad') > -1, //是否iPad
                    webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
                };
            }(),
            language: (navigator.browserLanguage || navigator.language).toLowerCase()
        }
        if(isWeiXin()){
            if(browser.versions.ios || browser.versions.iPhone || browser.versions.iPad) {
                $("#ios_tips").show();
            }else{
                $("#android_tips").show();
            }
        }else if(browser.versions.ios || browser.versions.iPhone || browser.versions.iPad) {
            window.location.href="https://itunes.apple.com/us/app/da-shi-hui-yun-shang-ping-tai/id1071747512?mt=8";
        }else if(browser.versions.android) {
            window.location.href="${androidDownurl}";
        }else{
            window.location.href="${androidDownurl}";
        }
    });
    function isWeiXin(){
        var ua = window.navigator.userAgent.toLowerCase();
        if(ua.match(/MicroMessenger/i) == 'micromessenger'){
            return true;
        }else{
            return false;
        }
    }
</script>
<div class="tip_wrap" >
    <img id="ios_tips" src="${BASE_PATH}/static/img/weixin_ios.jpg" class="wxtip tip_ios" style="display:none">
    <img id="android_tips" src="${BASE_PATH}/static/img/weixin_android.jpg" class="wxtip tip_android" style="display:none">
</div>
</div>
</body>
</html>