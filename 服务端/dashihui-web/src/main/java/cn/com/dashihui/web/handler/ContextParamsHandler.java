package cn.com.dashihui.web.handler;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;

public class ContextParamsHandler extends Handler {
	private Map<String,Object> params;
	
	public ContextParamsHandler(Map<String,Object> params) {
		this.params = params;
	}
	
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		if(params!=null&&params.size()!=0){
			for(String key : params.keySet()){
				request.setAttribute(key, params.get(key));
			}
		}
		nextHandler.handle(target, request, response, isHandled);
	}
}
