package cn.com.dashihui.web.dao;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class Join extends Model<Join>{
	private static final long serialVersionUID = 1L;
	private static Join me = new Join();
	public static Join me(){
		return me;
	}
	
	private boolean hasChildren = false;

	public List<Join> getChildren() {
		return get("children");
	}

	public void setChildren(List<Join> children) {
		put("children", children);
		hasChildren = true;
	}
	
	public boolean hasChildren(){
		return hasChildren;
	}
	
	public void addChild(Join child){
		List<Join> children = getChildren();
		if(children==null){
			children = new ArrayList<Join>();
		}
		children.add(child);
		setChildren(children);
		hasChildren = true;
	}
}
