<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.wx.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>我的订单</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_order.css" />
	</head>
	<body>
		<header class="am-header am-header-default app-header">
			<div class="am-header-left am-header-nav">
          		<a href="javascript:history.go(-1);" class="app-title">
          			<i class="am-icon-angle-left am-icon-md"></i>
          		</a>
          	</div>
			<h1 class="am-header-title">退款</h1>
		</header>
		<div id="emptyPart" style="display:none;">
		</div>
		<div id="notEmptyPart" style="display:none;">
			<!-- 订单列表 -->
		    <div class="app-order-wrapper">
				<div id="orderList"></div>
			</div>
			
			<div data-am-widget="gotop" class="am-gotop am-gotop-fixed" style="bottom:50px;">
				<a href="#top"><span class="am-gotop-title">回到顶部</span><i class="am-gotop-icon am-icon-chevron-up"></i></a>
			</div>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template-ext.js"></script>
		<script type="text/javascript">
			var pageNum = 0,totalPage = 1,loading = false,orderNum,state = '${empty state?0:state}';
			$(function(){
				//绑定“加载中”进入加载事件
				Kit.util.onPageEnd(function(){
	               	if(pageNum < totalPage && !loading){
	               		loading = true;
	               		$("#orderList").append("<div class=\"app-loading\">正在加载</div>");
	               		Kit.ajax.post("${BASE_PATH}/order/pageRefund",{pageNum:pageNum+1,pageSize:15},function(result){
							if(pageNum==0){
								if(result.object.totalRow>0){
									Kit.ui.toggle("#notEmptyPart","#emptyPart");
								}else{
									Kit.ui.toggle("#emptyPart","#notEmptyPart");
								}
							}
							$("#orderList").append(template("dataTpl",result.object));
							//图片延迟加载
							$("img.lazyload","#orderList").not(".lazyload-binded").lazyload({
								failurelimit : 100,
								effect : "show"
							}).addClass("lazyload-binded");
							$(".app-loading","#orderList").remove();
							pageNum = result.object.pageNumber;
							totalPage = result.object.totalPage;
							//重置加载标识
							loading = false;
						});
	               	}
				});
			});
		</script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
			{{each list as refund index}}
			<div id="order{{refund.orderNum}}" class="app-order-item app-m-t-10">
				<div class="app-text-13px">
					<div class="am-fl">
						<span>退款单号：{{refund.refundNum}}</span><br/>
						<span>申请时间：{{refund.createDate}}</span>
					</div>
					<div class="am-fr" style="line-height:3.2;">
						{{if refund.state == <%=OrderCode.RefundState.VERIFYING%>}}
						<span class="app-text-red">审核中</span>
						{{else if refund.state == <%=OrderCode.RefundState.VERIFIED%>}}
						<span class="app-text-red">审核通过</span>
						{{else if refund.state == <%=OrderCode.RefundState.VERIFY_FAILED%>}}
						<span class="app-text-red">审核不通过</span>
						{{else if refund.state == <%=OrderCode.RefundState.REFUNDED%>}}
						<span class="app-text-red">已退款</span>
						{{/if}}
					</div>
					<span class="am-cf"></span>
				</div>
				{{each refund.goodsList as goods}}
				<hr/>
				<div class="am-g app-text-14px">
					<div class="am-u-sm-2 am-u-md-3 am-u-lg-3" style="padding:2px;">
						<img data-original="${FTP_PATH}{{goods.thumb}}" width="100%" class="lazyload">
					</div>
					<div class="am-u-sm-10 am-u-md-9 am-u-lg-9" style="padding:2px;">
						<div class="am-u-sm-12 am-u-md-12 am-u-lg-12">{{goods.name}}</div>
					</div>
					<span class="am-cf"></span>
				</div>
				{{/each}}
				<hr/>
				<div class="app-m-t-10 app-text-13px">
					<div class="am-fl">
						<span style="color:#999;">交易金额：</span>
						<i class="am-icon-rmb"></i>
						<span>{{refund.orderAmount}}</span>
					</div>
					<div class="am-fr">
						<span style="color:#999;">退款金额：</span>
						<i class="am-icon-rmb app-text-red"></i>
						<span class="app-text-red">{{refund.amount}}</span>
					</div>
					<div class="am-cf"></div>
					<div class="am-fl"><span style="color:#999;">{{refund.refuseReason}}</span></div>
					<div class="am-cf"></div>
				</div>
				{{if refund.state == <%=OrderCode.RefundState.REFUNDED%>}}
					<hr/>
					<div class="app-m-t-10 app-text-12px" style="color:#999;">
						{{refund.statement}}
					</div>
				{{/if}}
			</div>
			{{/each}}
		</script>
	</body>
</html>