<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>确认订单</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_order_confirm.css" />
	</head>
	<body>
		<div class="app-part">
			<div class="app-part-item">
				<label>订单号：</label>
				<span>${orderNum}</span>
			</div>
			<hr/>
			<div class="app-part-item">
				<label>总金额：</label>
				<span><i class="am-icon-rmb"></i> ${amount}</span>
			</div>
		</div>
		
		<div style="width:100%;padding:5px;">
			<a href="javascript:doPrePay();" class="am-btn am-btn-success app-m-t-10 am-radius" style="width:100%;">微信支付</a>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
		<script type="text/javascript">
		wx.config({appId: '${appId}',timestamp: '${timestamp}',nonceStr: '${nonceStr}',signature: '${signature}',jsApiList: ['chooseWXPay']});
		function doPrePay(){
			Kit.ajax.post("${BASE_PATH}/order/doPrePay",{orderNum:"${orderNum}",flag:1},function(result){
				if(result.flag==0){
					wx.chooseWXPay({
						appId: '${appId}',
					    timestamp: result.object.timeStamp,
					    nonceStr: result.object.nonceStr,
					    package: result.object.package,
					    signType: result.object.signType,
					    paySign: result.object.paySign,
					    success: function (res) {
					    	if(res.errMsg == "chooseWXPay:ok"){
		    					var popup = '<div class="app-result-wrapper am-text-center"><i class="am-icon-check-circle am-text-success" style="font-size:100px;"></i><div>支付成功！</div></div><a href="javascript:turnToOrderList();" class="am-btn am-btn-success am-radius am-btn-block app-m-t-10">确认</a>';
		    					Kit.ui.popup(popup,"padding:10px;width:90%;");
					    	}else if(res.errMsg == "chooseWXPay:cancel"){
					    		Kit.ui.alert("支付被取消！");
					    	}else if(res.errMsg == "chooseWXPay:fail"){
					    		var popup = '<div class="app-result-wrapper am-text-center"><i class="am-icon-close am-text-danger" style="font-size:100px;"></i><div>支付失败！</div></div><a href="javascript:turnToOrderList();" class="am-btn am-btn-danger am-btn-block am-radius app-m-t-10">关闭</a>';
								Kit.ui.popup(popup,"padding:10px;width:90%;");
					    	}
					    }
					});
				}else{
					Kit.ui.alert("支付失败！",function(){
						turnToOrderList();
					});
				}
			});
		}
		function turnToOrderList(){
			Kit.render.redirect("${BASE_PATH}/order/list?state=1");
		}
		</script>
	</body>
</html>