<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.wx.common.UserCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>推荐的好友</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/app/css/m_my_money.css">
		<style type="text/css">
			button{
				font-size: 15px;
				color: black;
				margin-top: 20px;
			}
		</style>
	</head>
	<body>
		<header class="am-header app-header">
			<div class="am-header-left am-header-nav">
          		<a href="javascript:history.go(-1);" class="app-title">
          			<i class="am-icon-angle-left am-icon-md"></i>
          		</a>
          	</div>
			<h1 class="am-header-title app-title">推荐的好友</h1>
		</header>
		
		<div data-am-sticky>
			<div class="app-part app-part-money">
				<div class="app-part-top">
					<div class="app-part-title">${invitedUserCount.inviteCount }个
						<span style="font-size:15px;">好友</span>
						<a href="${BASE_PATH}/my/share"><button class="am-fr">分享好友</button></a>
					</div>
					<div style="color:white;">推荐的好友越多，奖励越多，轻松理财</div>
				</div>
			</div>
			<div class="app-part" style="background-color: #fff;padding: 0;">
				<div class="app-part-header">
					<div class="am-fl">推荐好友</div>
					<a class="am-fr">如何推荐好友</a>
					<div class="am-cf"></div>
				</div>
			</div>
		</div>
		<div class="app-part" style="background-color: #fff;padding: 0;">
			<div class="app-part-main" id="dataList"></div>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template-ext.js"></script>
		<script type="text/html" id="myIncome">
		{{each list as myIncome index}}
			{{if index == 0}}<div>获得好友{{myIncome.nickName}}购物收益</div>
			{{else}}<div class="app-m-t-10">获得好友{{myIncome.nickName}}购物收益</div>
			{{/if}}
			<div class="am-fl app-m-t-5" style="color:#bbb;font-size:13px;">{{myIncome.createDate | dateFormat:'yyyy年MM月dd日 hh:mm:ss'}}</div>
			<div class="am-fr" style="color:#e63f17;font-size:18px;">+{{myIncome.amount}}<span style="font-size:13px;"> 实惠币</span></div>
			<div class="am-cf"></div>
			<hr class="app-m-t-10">
		{{/each}}
		</script>
		<script type="text/javascript">
			var pageNum = 0,totalPage = 1,loading = false;
			$(function(){
				Kit.util.onPageEnd(function(){
					if(pageNum < totalPage && !loading){
						loading = true;
						$("#dataList").append("<div class=\"app-loading\">正在加载</div>");
						Kit.ajax.post("${BASE_PATH}/my/expenseRecord",{pageSize:10,pageNum:pageNum+1,searchType:4},function(result){
							$("#dataList").append(template("myIncome",result.object));
							pageNum = result.object.pageNumber;
							totalPage = result.object.totalPage;
						});
						$(".app-loading","#dataList").remove();
						loading = false;
					};
				});
			})
		</script>
	</body>
</html>