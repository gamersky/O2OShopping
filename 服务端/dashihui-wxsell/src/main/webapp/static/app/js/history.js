/**
 * 商品浏览历史本地数据操作(存储、获取、删除)
 */
(function(window){
	var key_name = "history";
	window.History = {
		/**
		 * 保存商品浏览记录
		 * 逻辑：
		 * 遍历现有浏览记录，移至新的数组中保存，当判断存在本次要保存的相同ID商品时，不移动，
		 * 在最后将商品保存至队尾，在浏览历史页面展示时，使用数组的reverse方法，将数组倒置，
		 * 以达到最近浏览的商品在最上边的效果
		 * @param {Object} goods 商品对象json格式信息
		 */
		save : function(goods){
			var list = Kit.dao.get(key_name,true);
			var array = [];
			if(list != null || list != undefined) {
				for(var i = 0; i < list.length; i++) {
					if(list[i].id != goods.id) {
						array.push(list[i]);
					}
				}
			}
			array.push(goods);
			Kit.dao.save(key_name,array,true);
		},
		
		/**
		 * 以json格式取出浏览记录
		 */
		get : function(){
			var list = Kit.dao.get(key_name,true);
			if(list != null && list!= undefined){
				return list;
			}else{
				return [];
			}
		},
		
		/**
		 * 清空浏览记录
		 */
		clear : function(){
			Kit.dao.del(key_name);
		}
	};
})(window);