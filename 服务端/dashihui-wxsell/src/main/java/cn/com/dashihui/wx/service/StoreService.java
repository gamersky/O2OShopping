package cn.com.dashihui.wx.service;

import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.wx.dao.Store;
import cn.com.dashihui.wx.dao.StoreAd;

public class StoreService {
	/**
	 * 查询指定店铺的广告位（只显示在微信商城显示的）
	 */
	public List<StoreAd> findAd(int storeid){
		return StoreAd.me().find("SELECT id,thumb,link,type,goodsid FROM t_bus_store_ad WHERE storeid=? AND isShowOnWX=1 ORDER BY orderNo",storeid);
	}
	
	/**
	 * 查询指定店铺的详细信息
	 */
	public Store findById(int storeid){
		return Store.me().findFirst("SELECT store.*,p.name provinceName,c.name cityName,a.name areaName FROM t_dict_store store LEFT JOIN t_dict_city p ON store.province=p.id LEFT JOIN t_dict_city c ON store.city=c.id LEFT JOIN t_dict_city a ON store.area=a.id WHERE store.id=?",storeid);
	}
	
	/**
	 * 查询指定店铺的最新一条通知
	 */
	public Record findLastTip(int storeid){
		return Db.findFirst("SELECT content FROM t_dict_store_tip WHERE storeid=? ORDER BY orderNo",storeid);
	}
}
