package cn.com.dashihui.wx.service;

import cn.com.dashihui.wx.dao.Community;

public class CommunityService {
	/**
	 * 根据百度地图的社区KEY查询相应的社区信息
	 */
	public Community findByBaidukey(String baidukey){
		return Community.me().findFirst("SELECT comm.*,p.name provinceName,c.name cityName,a.name areaName FROM t_dict_community comm LEFT JOIN t_dict_city p ON comm.province=p.id LEFT JOIN t_dict_city c ON comm.city=c.id LEFT JOIN t_dict_city a ON comm.area=a.id WHERE comm.baidukey=?",baidukey);
	}
	
	/**
	 * 根据社区ID查询相应的社区信息
	 */
	public Community findById(int communityid){
		return Community.me().findFirst("SELECT comm.*,p.name provinceName,c.name cityName,a.name areaName FROM t_dict_community comm LEFT JOIN t_dict_city p ON comm.province=p.id LEFT JOIN t_dict_city c ON comm.city=c.id LEFT JOIN t_dict_city a ON comm.area=a.id WHERE comm.id=?",communityid);
	}
}
