package cn.com.dashihui.wx.interceptor;

import cn.com.dashihui.wx.common.Constants;
import cn.com.dashihui.wx.controller.BaseController;
import cn.com.dashihui.wx.dao.User;
import cn.com.dashihui.wx.service.UserService;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

/**
 * 判断用户当前已经登录， 再查询一次最新信息并放在session中
 */
public class RefreshLoginInterceptor implements Interceptor{
	private UserService userService = new UserService();

	@Override
	public void intercept(Invocation inv) {
		BaseController c = (BaseController)inv.getController();
		//如果用户当前已经登录， 再查询一次最新信息并放在session中
		User currentUser = c.getCurrentUser();
		if(currentUser!=null){
			c.setSessionAttr(Constants.USER, userService.findByUserid(c.getCurrentUserid()));
		}
		inv.invoke();
	}
}
