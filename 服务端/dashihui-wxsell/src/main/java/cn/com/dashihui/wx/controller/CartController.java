package cn.com.dashihui.wx.controller;

import cn.com.dashihui.wx.common.CartKit;
import cn.com.dashihui.wx.common.CartKit.Cart;
import cn.com.dashihui.wx.common.CartKit.CartGoods;
import cn.com.dashihui.wx.kit.DoubleKit;
import cn.com.dashihui.wx.kit.MapKit;

/**
 * 用户购物车操作<br/>
 * 未登录时，保存在session中
 */
public class CartController extends BaseController{
	
	/**
	 * 跳转至购物车列表页
	 */
	public void index(){
		setAttr("currentStore", getCurrentStore());
		render("/WEB-INF/page/cart.jsp");
	}
	
	/**
	 * 异步查询购物车商品数量，多用于页面底部大菜单中“购物车”的角标显示
	 */
	public void state(){
		//返回当前购物车商品数量及总金额
		Cart cart1 = CartKit.getCart(getSession(),0);
		Cart cart2 = CartKit.getCart(getSession(),getCurrentStoreid());
		renderSuccess(MapKit.one()
				.put("CART_COUNTER", cart1.getCounter()+cart2.getCounter())
				.put("CART_TOTAL", DoubleKit.add(cart1.getTotal(),cart2.getTotal()))
				.getAttrs());
	}
	
	/**
	 * 异步查询购物车清单
	 */
	public void detail(){
		//返回当前自营商品购物车及店铺商品购物车
		Cart cart1 = CartKit.getCart(getSession(),0);
		Cart cart2 = CartKit.getCart(getSession(),getCurrentStoreid());
		renderSuccess(MapKit.one()
				.put("cart1", cart1)
				.put("cart2", cart2)
				.getAttrs());
	}
	
	/**
	 * 加入购物车
	 * @param goodsid 商品ID
	 * @param goodsName 商品标题
	 * @param goodsSpec 商品规格
	 * @param goodsThumb 商品图片
	 * @param goodsSellPrice 商品销售价
	 * @param goodsMarketPrice 商品市场价
	 */
	public void add(){
		int storeid = getCurrentStoreid();
		String goodsid = getPara("id");
		int isSelf = getParaToInt("isSelf");
		String goodsName = getPara("name");
		String goodsSpec = getPara("spec");
		String goodsThumb = getPara("thumb");
		double goodsSellPrice = Double.parseDouble(getPara("sellPrice"));
		double goodsMarketPrice = Double.parseDouble(getPara("marketPrice"));
		//购物车商品对象
		CartGoods cartGoods = new CartGoods(goodsid, isSelf, goodsName, goodsSpec, goodsThumb, goodsSellPrice, goodsMarketPrice);
		//判断商品是否是自营，放在不同购物车中
		if(isSelf==1){
			//自营商品
			CartKit.add(getSession(), 0, cartGoods);
		}else{
			//店铺商品
			CartKit.add(getSession(), storeid, cartGoods);
		}
		//返回当前购物车商品数量及总金额
		Cart cart1 = CartKit.getCart(getSession(),0);
		Cart cart2 = CartKit.getCart(getSession(),getCurrentStoreid());
		renderSuccess(MapKit.one()
				.put("counter", cart1.getCounter()+cart2.getCounter())
				.put("total", DoubleKit.add(cart1.getTotal(),cart2.getTotal()))
				.getAttrs());
	}
	
	/**
	 * 修改商品数量
	 * @param goodsid 要修改数量的商品ID
	 * @param operate 1：增加数量，0：减少数量
	 * @param num 要修改的数量
	 */
	public void update(){
		int storeid = getCurrentStoreid();
		String goodsid = getPara("goodsid");
		int operate = getParaToInt("operate");
		int num = getParaToInt("num");
		//判断商品如果是在自营商品购物车中，则更新自营商品购物车
		Cart cart1 = CartKit.getCart(getSession(), 0);
		if(cart1.hasGoods(goodsid)){
			cart1.update(goodsid, operate, num);
		}
		//判断商品如果是在店铺商品购物车中，则更新店铺商品购物车
		Cart cart2 = CartKit.getCart(getSession(), storeid);
		if(cart2.hasGoods(goodsid)){
			cart2.update(goodsid, operate, num);
		}
		//返回当前购物车商品数量及总金额
		renderSuccess();
	}
	
	/**
	 * 从购物车中移除某些商品
	 * @param goodsid 要移除的商品ID数组，以英文逗号分隔
	 */
	public void delete(){
		int storeid = getCurrentStoreid();
		String goodsid = getPara("goodsid");
		String[] goodsids = goodsid.split(",");
		//从自营商品购物车中删除商品
		Cart cart1 = CartKit.getCart(getSession(), 0);
		cart1.delete(goodsids);
		//从店铺商品购物车中删除商品
		Cart cart2 = CartKit.getCart(getSession(), storeid);
		cart2.delete(goodsids);
		//返回当前购物车商品数量及总金额
		renderSuccess();
	}
}
