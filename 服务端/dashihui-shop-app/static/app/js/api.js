/**
 * 封装所有与服务器间的接口请求
 */
(function(window){
	window.API = {};

	//接口地址前缀
	var base = "http://192.168.1.113:8086";
	
	//错误代码
	var code = {
		SUCCESS : 0,
		FAILED : -1
	};

	//发起异步请求统一方法
	var doPost = function(suffix,params,onSuccess,onFailed){
		//请求
		$.post(base + suffix, params, function (result) {
			
			if(result.STATE==code.SUCCESS){
				//接口返回成功时
				if(onSuccess)onSuccess(result.OBJECT);
			}else{
				//接口返回失败时
				if(result.MSG){
					Kit.ui.alert(result.MSG);
				}
				if(onFailed)onFailed(result.FLAG,result.MSG,result.OBJECT);
			}
		}, "json");
	};
	
	//与服务器间的来往接口实现开始
	/**
	 * 检测版本更新  
	 * 检查版本更新<br/>检查更新成功后，会根据终端类型进行相应处理（IOS会提示去appstore更新，安卓会提示下载）<br/>
	 * 另外还需要注意客户端是否要求强制更新，如果是，则弹出框不可关闭
	 * @param {Object} isLogin 当首页加载检测版本时 当前版本为最新版本时不提醒  
	 */
	API.checkVersion  = function(isLogin){
		var showMask = Kit.ui.showLoading("",{back:"none"});
		var osType = mui.os.android?"A":"B";
		var cversionCode = Prop.appVersion;
		doPost("/common/checkVersion",{"TYPE":osType,"VERSION":cversionCode},function(data){
			var obj =  data;
			var updateFlg = obj.UPDATEFLG;
			var downUrl = obj.DOWNURL;
			if(updateFlg == "A"){
				if(!isLogin){
					Kit.ui.alert("已经是最新版本");
				}
				Kit.ui.closeLoading();
				return;
			}else{
				
				if(osType == "A"){//安卓
					//安卓时，提示下载
					if(updateFlg=="C"){
						showMask.setTitle("正在下载最新版");
						//强制更新时，弹出框不可关闭
						Kit.download.create(downUrl,true);
					}else{
						//非强制更新时，弹出框可关闭
						Kit.ui.confirm("更新","发现新版本了",function(){
							showMask.setTitle("正在下载最新版");
							Kit.download.create(downUrl,false);
						},function(){
							Kit.ui.closeLoading();
						});
					}
				}else{//IOS无法
					if(updateFlg=="C"){
						//强制更新时，弹出框不可关闭
						Kit.ui.modal("立即更新");
						plus.runtime.openURL(downUrl);
					}else{
						//非强制更新时，弹出框可关闭
						Kit.ui.confirm("更新","发现新版本了",function(){
							plus.runtime.openURL(downUrl);
						},function(){
							Kit.ui.closeLoading();
						});
					} 
				}
			}
		});
	}
	/**
	 * 修改密码 ||找回密码 发送验证码到手机上
	 * @param {Object} phone
	 */
	API.sendResetPwdCode = function(phone, onSuccess, onFailed){
		doPost("/common/sendResetPwdCode",{"PHONE":phone},onSuccess,onFailed);
	}
	/**
	 * 注册设备信息
	 * @param {Object} phone
	 */
	API.registerDevice = function(shopid, clientid, deviceid,onSuccess, onFailed){
		doPost("/common/registerDevice",{"SHOPID":shopid,"CLIENTID":clientid,"DEVICEID":deviceid},onSuccess,onFailed);
	}
	/**
	 * 删除设备信息
	 * @param {Object} phone
	 */
	API.clearDevice = function(shopid, clientid, deviceid,onSuccess, onFailed){
		doPost("/common/clearDevice",{"SHOPID":shopid,"CLIENTID":clientid,"DEVICEID":deviceid},onSuccess,onFailed);
	}
	/**
	 * 用户反馈
	 * @param {Object} shopid
	 * @param {Object} context
	 * @param {Object} onSuccess
	 * @param {Object} onFailed
	 */
	API.feedback = function(shopid, context, onSuccess, onFailed){
		doPost("/shop/feedback",{"SHOPID":shopid,"CONTEXT":context},onSuccess,onFailed);
	};
	
	/**
	 * 修改密码 
	 * @param {Object} phone
	 * @param {Object} code
	 * @param {Object} pwd
	 * @param {Object} confirmPwd
	 */
	API.resetPwd = function(phone,code,pwd,confirmPwd, onSuccess, onFailed){
		doPost("/shop/resetPwd",{"PHONE":phone,"CODE":code,"PWD":pwd,"CONFIRMPWD":confirmPwd},onSuccess,onFailed);
	}
	/**
	 * 登录操作
	 * @param {Object} account
	 * @param {Object} pwd
	 * @param {Object} onSuccess
	 * @param {Object} onFailed
	 */
	API.doLogin = function(account, pwd, onSuccess, onFailed){
		doPost("/shop/login",{"PHONE":account,"PASSWORD":pwd},onSuccess,onFailed);
	};
	/**
	 * 查询指定商家的状态信息（如订单数量等）
	 * @param {Object} shopid
	 * @param {Object} onSuccess
	 * @param {Object} onFailed
	 */
	API.getShopState = function(shopid, onSuccess, onFailed){
		doPost("/order/orderCount",{"ID":shopid},onSuccess,onFailed);
	};
	/**
	 * 上拉查询订单  上拉刷新加载更多订单
	 * @param {Object} shopid 商家ID
	 * @param {Object} pageSize 一页显示的订单数
	 * @param {Object} pageNum  页数
	 * @param {Object} flag 0：全部 1：待接单 2：待服务 3：已完成
	 * @param {Object} startDate 开始时间
	 * @param {Object} onSuccess 成功后处理方法
	 * @param {Object} onFailed 失败处理方法
	 * 
	 */
	API.getOrderUp = function(shopid,pageSize,pageNum,flag,startDate, onSuccess, onFailed){
		doPost("/order/page",{"ID":shopid,"PAGENUM":pageNum,"PAGESIZE":pageSize,"FLAG":flag,"DIRECTION":1,"STARTDATE":startDate},onSuccess,onFailed);
	}
	/**
	 * 下拉查询订单  下拉刷新加载最新订单
	 * @param {Object} shopid 商家ID
	 * @param {Object} pageSize 一页显示的订单数
	 * @param {Object} pageNum  页数
	 * @param {Object} flag 0：全部 1：待接单 2：待服务 3：已完成
	 * @param {Object} startDate 开始时间
	 * @param {Object} onSuccess 成功后处理方法
	 * @param {Object} onFailed 失败处理方法
	 */
	API.getOrderDown = function(shopid,pageSize,pageNum,flag,startDate, onSuccess, onFailed){
		doPost("/order/page",{"ID":shopid,"PAGENUM":pageNum,"PAGESIZE":pageSize,"FLAG":flag,"DIRECTION":2,"STARTDATE":startDate},onSuccess,onFailed);
	}
	/**
	 * 商家接单
	 * @param {Object} shopid 商家ID
	 * @param {Object} ORDERNUM  订单ID
	 * @param {Object} onSuccess 成功后处理方法
	 * @param {Object} onFailed 失败处理方法
	 * 
	 */
	API.accept = function(shopid,orderNum,onSuccess,onFailed){
		doPost("/order/accept",{"ID":shopid,"ORDERNUM":orderNum},onSuccess,onFailed);
	}
	/**
	 * 商家拒单
	 * @param {Object} shopid 商家ID
	 * @param {Object} ORDERNUM  订单ID
	 * @param {Object} onSuccess 成功后处理方法
	 * @param {Object} onFailed 失败处理方法
	 * 
	 */
	API.refuse = function(shopid,orderNum,onSuccess,onFailed){
		doPost("/order/refuse",{"ID":shopid,"ORDERNUM":orderNum},onSuccess,onFailed);
	}
	/**
	 * 标记订单完成
	 * @param {Object} shopid 商家ID
	 * @param {Object} ORDERNUM  订单号
	 * @param {Object} onSuccess 成功后处理方法
	 * @param {Object} onFailed 失败处理方法
	 * 
	 */
	API.finish = function(shopid,orderNum,onSuccess,onFailed){
		doPost("/order/finish",{"ID":shopid,"ORDERNUM":orderNum},onSuccess,onFailed);
	}
	/**
	 * 改变商家的营业状态
	 * @param {Object} shopid 商家ID
	 * @param {Object} state  状态
	 * @param {Object} onSuccess 成功后处理方法
	 * @param {Object} onFailed 失败处理方法
	 * 
	 */
	API.changeWork = function(shopid,state,onSuccess,onFailed){
		doPost("/shop/changeWork",{"ID":shopid,"FLAG":state},onSuccess,onFailed);
	}
	/**
	 * 订单详情
	 * @param {Object} shopid 商家ID
	 * @param {Object} ORDERNUM  订单号
	 * @param {Object} onSuccess 成功后处理方法
	 * @param {Object} onFailed 失败处理方法
	 * 
	 */
	API.orderDetail = function(shopid,orderNum,onSuccess,onFailed){
		doPost("/order/detail",{"ID":shopid,"ORDERNUM":orderNum},onSuccess,onFailed);
	}
	
	
})(window);