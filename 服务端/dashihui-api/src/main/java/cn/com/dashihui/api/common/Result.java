package cn.com.dashihui.api.common;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;

public class Result {
	private String state;
	private String message;
	private Object object;
	
	public Result(String state) {
		this.state = state;
	}
	
	public Result(String state, Object object) {
		this.state = state;
		this.object = object;
	}
	
	public Result(String state, String message) {
		this.state = state;
		this.message = message;
	}
	
	public Result(String state, String message, Object object) {
		this.state = state;
		this.message = message;
		this.object = object;
	}
	
	public Result(String state, Map<String,Object> map){
		this.state = state;
		this.object = map;
	}
	
	public Result(String state, String message, Map<String,Object> map) {
		this.state = state;
		this.message = message;
		this.object = map;
	}
	
	public void setState(String state) {
		this.state = state;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setObject(Object object){
		this.object = object;
	}
	
	@Override
	public String toString() {
		Map<String,Object> result = new HashMap<String,Object>();
		result.put(ResultField.FIELD_STATE, state);
		result.put(ResultField.FIELD_MSG, StrKit.isBlank(message)?"":message);
		if(object!=null){
			if(object instanceof ResultMap){
				result.put(ResultField.FIELD_OBJECT, ((ResultMap)object).getAttrs());
			}else{
				result.put(ResultField.FIELD_OBJECT, object);
			}
		}else{
			result.put(ResultField.FIELD_OBJECT, "");
		}
		return JsonKit.toJson(result);
	}
}
