package cn.com.dashihui.api.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.aop.Before;
import com.jfinal.aop.Duang;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.common.ResultMap;
import cn.com.dashihui.api.common.ServiceOrderCode;
import cn.com.dashihui.api.dao.ServiceOrder;
import cn.com.dashihui.api.dao.ServiceOrderPayAPIRecord;
import cn.com.dashihui.api.interceptor.AuthLoginInterceptor;
import cn.com.dashihui.api.service.ServiceOrderService;
import cn.com.dashihui.kit.CommonKit;
import cn.com.dashihui.kit.DatetimeKit;
import cn.com.dashihui.pay.alipay.sign.SignUtils;
import cn.com.dashihui.pay.wx.kit.HttpsRequest;
import cn.com.dashihui.pay.wx.kit.Signature;
import cn.com.dashihui.pay.wx.kit.UtilKit;
import cn.com.dashihui.pay.wx.request.PreOrderReqData;
import cn.com.dashihui.pay.wx.request.QueryOrderReqData;
import cn.com.dashihui.pay.wx.response.PreOrderResData;
import cn.com.dashihui.pay.wx.response.QueryOrderResData;

/**
 * 服务类订单处理
 */
@Before(AuthLoginInterceptor.class)
public class ServiceOrderController extends BaseController {

	//使用Duang.duang进行封装，使普通类具有事务的功能
	private ServiceOrderService service = Duang.duang(ServiceOrderService.class);

	/**
	 * 保存订单<br/>
	 * 在保存订单时，需要验证<br/>
	 * 1.验证所购买商品是否是“上架”状态
	 * 2.对商品数量是否超过限购数量作验证，如果不符合则保存不在成功
	 * 3.验证客户端提交过来的订单总金额和服务端计算出来的总金额是否一致
	 * @param STOREID 商铺ID
	 * @param SERVICESID 服务项ID
	 * @param AMOUNT 总金额
	 * @param SERTIME 服务时间
	 * @param PAYTYPE 支付方式
	 * @param LINKNAME 联系人姓名
	 * @param SEX 性别
	 * @param TEL 电话
	 * @param ADDRESS 地址
	 * @param DESCRIBE 订单备注
	 */
	@Before(Tx.class)
	public void save(){
		String storeid = getPara("STOREID");
 		String servicesidArray = getPara("SERVICESID");
		String amount = getPara("AMOUNT");
		String serTime = getPara("SERTIME");
		String payType = getPara("PAYTYPE");
		String linkName = getPara("LINKNAME");
		String sex = getPara("SEX");
		String tel = getPara("TEL");
		String address = getPara("ADDRESS");
		String describe = getPara("DESCRIBE");
		if(StrKit.isBlank(storeid)){
			renderFailed("参数STOREID不能为空");
    		return;
		}else if(StrKit.isBlank(servicesidArray)){
			renderFailed("参数SERVICESID不能为空");
    		return;
		}else if(StrKit.isBlank(amount)){
			renderFailed("参数AMOUNT不能为空");
    		return;
		}else if(StrKit.isBlank(payType)){
			renderFailed("参数PAYTYPE不能为空");
    		return;
		}else if(StrKit.isBlank(linkName)){
			renderFailed("参数LINKNAME不能为空");
    		return;
		}else if(StrKit.isBlank(sex)){
			renderFailed("参数SEX不能为空");
    		return;
		}else if(StrKit.isBlank(tel)){
			renderFailed("参数TEL不能为空");
    		return;
		}else if(StrKit.isBlank(address)){
			renderFailed("参数ADDRESS不能为空");
    		return;
		}else{
			//验证服务 下架和删除
			if(service.hasDownOrDelGoods(servicesidArray.split(","))){
				renderFailed("此订单中有已下架或者删除的服务");
				return;
			}
			//验证价格
			double totalPrice = service.getAmount(servicesidArray.split(","));
			if(totalPrice != Double.valueOf(amount).doubleValue()){
				renderFailed("订单价格与后台总价格有误");
				return; 
			}
			//生成服务类订单的订单编号，以大写字母S（O表示其他，与家政订单区分开）开头
			String orderNum = "SO"+DatetimeKit.getFormatDate("yyyyMMddHHmmssSSS")+CommonKit.randomNum(2);
			//保存订单
			ServiceOrder order = new ServiceOrder()
					.set("orderNum", orderNum)
					.set("storeid", storeid)
					.set("userid", getCurrentUser().getInt("id"))
					.set("amount", totalPrice)
					.set("payType", Integer.valueOf(payType).intValue())
					.set("payState", ServiceOrderCode.OrderPayState.NO_PAY)
					.set("deliverState", ServiceOrderCode.OrderDispatchState.NO_DISPATCH)
					.set("orderState", ServiceOrderCode.OrderState.NORMAL)
					.set("linkName", linkName)
					.set("sex", sex)
					.set("tel", tel)
					.set("address", address)
					.set("startDate", DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss"))
					.set("expireDate",  DatetimeKit.getIntervalFormatDate(PropKit.getInt("constants.order.interval"), "yyyy-MM-dd HH:mm:ss"));
			if(StrKit.notBlank(serTime)){
				order.set("serTime", serTime);
			}
			if(StrKit.notBlank(describe)){
				order.set("describe", describe);
			}
			try {
				service.saveOrderAndGoods(order, servicesidArray.split(","));
				service.log(order.getStr("orderNum"),"用户", "保存订单", "订单号："+order.getStr("orderNum"));
				renderSuccess(ResultMap.newInstance().put("ORDERNUM",orderNum).put("AMOUNT", totalPrice));
				return;
			} catch (Exception e) {
				e.printStackTrace();
				renderFailed("出错了");
				return;
			}
		}
	}
	
	/**
	 * 客户端确认去支付<br/>
	 * 首先验证支付方式参数，并验证根据订单号查询订单记录是否存在，以此验证订单号是否正确<br/>
	 * 然后根据所选择的支付方式，向不同平台发起支付请求<br/>
	 * 目前只申请了微信支付平台，2015/12/02
	 * @param ORDERNUM 要支付的订单号
	 * @param FLAG 用户所选择的支付方式，1:微信支付
	 */
	public void pay(){
		String orderNum = getPara("ORDERNUM");
		String flagStr = getPara("FLAG");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
    		return;
		}else if(StrKit.isBlank(flagStr)){
			renderFailed("参数FLAG不能为空");
    		return;
		}else if(!flagStr.equals(String.valueOf(ServiceOrderCode.OrderPayMethod.WEIXIN))&&!flagStr.equals(String.valueOf(ServiceOrderCode.OrderPayMethod.ALIPAY))){
			renderFailed("参数FLAG不正确");
    		return;
		}
		//查询订单详情并以此验证订单号
		ServiceOrder order = service.getOrderByOrderNum(getCurrentUser().getInt("id"),orderNum);
		if(order==null){
			renderFailed("订单不存在");
    		return;
		}
		//验证订单是否有效
		String startDate = DatetimeKit.getFormatDate(order.getDate("startDate"),"yyyyMMddHHmmss");
		String expireDate = DatetimeKit.getFormatDate(order.getDate("expireDate"),"yyyyMMddHHmmss");
		if(DatetimeKit.compareDate(order.getDate("expireDate")) == -1){
			renderFailed("您的订单已过期");
			return;
		}
		int payMethod = Integer.valueOf(flagStr);
		if(payMethod == ServiceOrderCode.OrderPayMethod.WEIXIN){
			//1：微信支付
			//获取客户端IP
			String remoteip = CommonKit.getClientIp(getRequest());
			//获取订单总金额，并转换为以“分”为单位
			String amount = String.valueOf(Double.valueOf(order.getDouble("amount")*100).intValue()).toString();
			//向微信发起预支付请求
			PreOrderReqData preOrderReqData = new PreOrderReqData("大实惠云商超市",orderNum,amount,remoteip,PropKit.get("weixinpay.serviceOrderNotifyUrl"),startDate,expireDate);
			PreOrderResData preOrderRspData = null;
			try {
				preOrderRspData = (PreOrderResData)UtilKit.getObjectFromXML(new HttpsRequest().post(PropKit.get("weixinpay.unifiedorder"), preOrderReqData), PreOrderResData.class);
			}catch(Exception e){
				e.printStackTrace();
				renderFailed("支付错误");
	    		return;
			}
			//预支付完成
			if(preOrderRspData!=null){
				//保存接口请求记录
				String sendContent = UtilKit.getXMLFromObject(preOrderReqData);
				String returnContent = UtilKit.getXMLFromObject(preOrderRspData);
				logger.info(sendContent);
				logger.info(returnContent);
				new ServiceOrderPayAPIRecord().set("orderNum", orderNum)
					.set("sendContent", sendContent)
					.set("returnContent", returnContent)
					.set("payMethod", payMethod)
					.set("flag", 1)
					.save();
				if(preOrderRspData.getReturn_code().equals("SUCCESS")&&preOrderRspData.getResult_code().equals("SUCCESS")){
					//更新订单支付方式为微信
					order.set("payMethod", ServiceOrderCode.OrderPayMethod.WEIXIN).update();
					//向客户端返回结果
					String noncestr = CommonKit.getRandomStringByLength(32);
					String timestamp = DatetimeKit.getTimestamp();
					renderSuccess(ResultMap.newInstance()
							.put("APPID", preOrderReqData.getAppid())
							.put("PARTNERID", preOrderReqData.getMch_id())
							.put("PREPAYID", preOrderRspData.getPrepay_id())
							.put("PACKAGE", "Sign=WXPay")
							.put("NONCESTR", noncestr)
							.put("TIMESTAMP", timestamp)
							.put("SIGN", Signature.getSign(ResultMap.newInstance()
									.put("appid", preOrderReqData.getAppid())
									.put("partnerid", preOrderReqData.getMch_id())
									.put("prepayid", preOrderRspData.getPrepay_id())
									.put("package", "Sign=WXPay")
									.put("noncestr", noncestr)
									.put("timestamp", timestamp).getAttrs())));
					return;
				}
			}
		}else if(payMethod == ServiceOrderCode.OrderPayMethod.ALIPAY){
			//2：支付宝支付
			//接收客户端入参
			StringBuffer params = new StringBuffer();
			// 签约合作者身份ID
			params.append("partner=\"").append(PropKit.get("alipay.partner")).append("\"");
			// 签约卖家支付宝账号
			params.append("&seller_id=\"").append(PropKit.get("alipay.seller_id")).append("\"");
			// 商户网站唯一订单号
			params.append("&out_trade_no=\"").append(orderNum).append("\"");
			// 商品名称
			params.append("&subject=\"大实惠云商超市\"");
			// 商品详情
			params.append("&body=\"大实惠云商超市\"");
			// 商品金额
			params.append("&total_fee=\"").append(order.getDouble("amount")).append("\"");
			// 服务器异步通知页面路径
			params.append("&notify_url=\"").append(PropKit.get("alipay.serviceOrderNotifyUrl")).append("\"");
			// 服务接口名称， 固定值
			params.append("&service=\"mobile.securitypay.pay\"");
			// 支付类型， 固定值
			params.append("&payment_type=\"1\"");
			// 参数编码， 固定值
			params.append("&_input_charset=\"utf-8\"");
			// 设置未付款交易的超时时间，可空
			// 默认30分钟，一旦超时，该笔交易就会自动被关闭。
			// 取值范围：1m～15d。
			// m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
			// 该参数数值不接受小数点，如1.5h，可转换为90m。
			// 或者使用绝对时间（示例格式：2014-06-13 16:00:00）。
			// m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）。
			// 该参数数值不接受小数点，如1.5h，可转换为90m。
			params.append("&it_b_pay=\"").append(DatetimeKit.getFormatDate(order.getDate("expireDate"),"yyyy-MM-dd HH:mm:ss")).append("\"");
			//签名并作URL编码
			String sign = SignUtils.sign(params.toString(),PropKit.get("alipay.privateKey"));
			try {
				sign = URLEncoder.encode(sign, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			params.append("&sign=\"").append(sign).append("\"");
			params.append("&sign_type=\"RSA\"");
			//更新订单支付方式为支付宝
			order.set("payMethod", ServiceOrderCode.OrderPayMethod.ALIPAY).update();
			//将结果返回给客户端
			renderSuccess(ResultMap.newInstance().put("PARAMS", params.toString()).getAttrs());
			return;
		}
		renderFailed("支付错误");
		return;
	}

	/**
	 * 查询指定订单支付结果<br/>
	 * 客户端跳转向微信或其他第三方支付平台，在支付完成后，第三方平台会返回客户端界面，并告知支付结果，此时客户端需要再次向服务端发起支付结果查询请求
	 * @param ORDERNUM 订单号
	 * @param PAYSTATE 支付结果，1:已支付，0:未支付
	 */
	public void query(){
		String orderNum = getPara("ORDERNUM");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
    		return;
		}
		//查询订单详情，并验证订单是否存在，以及订单支付状态
		ServiceOrder order = service.getOrderByOrderNum(getCurrentUser().getInt("id"),orderNum);
		if(order==null){
			renderFailed("订单不存在");
    		return;
		}else if(order.getInt("payState")==2){
			renderSuccess(ResultMap.newInstance().put("PAYSTATE", 1));  
			return;
		}else if(order.getInt("payState")==1
				//未支付状态，且支付方式不为空时，需要根据不同支付方式去相应支付平台查询订单的最新状态
				&&!StrKit.isBlank(order.get("payMethod"))){
			if(order.getInt("payMethod")==ServiceOrderCode.OrderPayMethod.WEIXIN){
				//1：微信支付
				try {
					QueryOrderReqData queryOrderReqData = new QueryOrderReqData(order.getStr("tradeNo"));
					String responseStr = new HttpsRequest().post(PropKit.get("weixinpay.orderquery"), queryOrderReqData);
					QueryOrderResData queryOrderResData = (QueryOrderResData) UtilKit.getObjectFromXML(responseStr, QueryOrderResData.class);
					if(queryOrderResData!=null){
						//保存接口请求记录
						new ServiceOrderPayAPIRecord().set("orderNum", orderNum)
							.set("sendContent", UtilKit.getXMLFromObject(queryOrderReqData))
							.set("returnContent", UtilKit.getXMLFromObject(queryOrderResData))
							.set("payMethod", ServiceOrderCode.OrderPayMethod.WEIXIN)
							.set("flag", 3)
							.save();
						if(queryOrderResData.getReturn_code().equals("SUCCESS")&&queryOrderResData.getResult_code().equals("SUCCESS")){
							order.set("payState", 2).update();
							renderSuccess(ResultMap.newInstance().put("PAYSTATE", 1));  
							return;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}else if(order.getInt("payMethod")==ServiceOrderCode.OrderPayMethod.ALIPAY){
			//2：支付宝支付不提供相应的查询状态功能
		}
		renderSuccess(ResultMap.newInstance().put("PAYSTATE", 0));  
		return;
	}
	
	/**
	 * <red>注：2016/02/25，停止该方法在客户端的调用，家政类服务订单列表与其他类服务订单列表，统一调用UserController的serviceOrderList，本方法不删除只是为了适应旧版本客户端</red>
	 * 查询订单列表
	 * @param FLAG 订单状态，1:待付款，2：待发货，3：待收货，4：已完成
	 * @param PAGENUM 页码
	 * @param PAGESIZE 数量
	 */
	public void list(){
		int pageNum = getParaToInt("PAGENUM",1);
		int pageSize = getParaToInt("PAGESIZE",PropKit.getInt("constants.pageSize"));
		int flag = getParaToInt("FLAG",0);
		Page<Record> page = service.findByPage(getCurrentUser().getInt("id"),flag,pageNum,pageSize);
		List<Record> orderList = page.getList();
		for(Record record : orderList){
			record.set("SERVICELIST", service.getServiceListByOrderNum(record.getStr("orderNum")));
		}
		renderSuccess(ResultMap.newInstance()
				.put("PAGENUMBER", page.getPageNumber())
				.put("PAGESIZE", page.getPageSize())
				.put("TOTALPAGE", page.getTotalPage())
				.put("TOTALROW", page.getTotalRow())
				.put("LIST", page.getList()));
		return;
	}
	
	/**
	 * 取消订单<br/>
	 * @param ORDERNUM 订单号
	 * @param REASON 取消订单理由
	 */
	@Before(Tx.class)
	public void cancel(){
		String orderNum = getPara("ORDERNUM");
		String reason = getPara("REASON");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
    		return;
		}else{
			ServiceOrder order = service.getOrderByOrderNum(getCurrentUser().getInt("id"),orderNum);
			if(order!=null){
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=ServiceOrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许取消");
					return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int dispatchState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“未付款”时可取消订单
				if(payType==ServiceOrderCode.OrderPayType.ON_LINE){
					if(payState!=ServiceOrderCode.OrderPayState.NO_PAY){
						renderFailed("此订单当前不允许取消");
						return;
					}
				}else
				//2.2.服务后付款，派单状态为“未派单”时可取消
				if(payType==ServiceOrderCode.OrderPayType.AFTER_SERVICE){
					if(dispatchState==ServiceOrderCode.OrderDispatchState.HAD_DISPATCH){
						renderFailed("此订单当前不允许取消");
						return;
					}
				}
				//3.取消订单操作，并记录日志
				order.set("orderState", ServiceOrderCode.OrderState.CANCEL).set("reason", reason);
				if(order.update()){
					service.log(order.getStr("orderNum"),"用户", "取消订单", "订单号："+order.getStr("orderNum")+"，取消成功！");
					renderSuccess();
					return;
				}
				//4.其他情况
				renderFailed("取消订单失败");
				return;
			}else{
				renderFailed("订单不存在！");
				return;
			}
		}
	}
	
	/**
	 * 删除订单
	 * @param ORDERNUM 订单号
	 */
	public void delete(){
		String orderNum = getPara("ORDERNUM");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
    		return;
		}else{
			ServiceOrder order = service.getOrderByOrderNum(getCurrentUser().getInt("id"),orderNum);
			if(order!=null){
				//1.判断订单状态是否为“取消”、“过期”、“完成”状态
				int orderState = order.getInt("orderState");
				if(orderState!=ServiceOrderCode.OrderState.CANCEL&&orderState!=ServiceOrderCode.OrderState.EXPIRE&&orderState!=ServiceOrderCode.OrderState.FINISH){
					renderFailed("此订单当前不允许删除");
		    		return;
				}
				//2.删除订单操作，并记录日志
				order.set("orderState", ServiceOrderCode.OrderState.DELETE);
				if(order.update()){
					service.log(order.getStr("orderNum"),"用户", "删除订单", "订单号："+order.getStr("orderNum")+"，删除成功！");
					renderSuccess();
					return;
				}
				//3.其他情况
				renderFailed("删除订单失败");
				return;
			}else{
				renderFailed("订单不存在！");
				return;
			}
		}
	}

	/**
	 * 确认服务完成
	 * @param ORDERNUM 订单号
	 */
	public void sign(){
		String orderNum = getPara("ORDERNUM");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
    		return;
		}else{
			ServiceOrder order = service.getOrderByOrderNum(getCurrentUser().getInt("id"),orderNum);
			if(order!=null){
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=ServiceOrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许签收");
		    		return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int dispatchState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“已付款”，派单状态为“已派单”时可确认服务完成
				if(payType==ServiceOrderCode.OrderPayType.ON_LINE){
					if(payState!=ServiceOrderCode.OrderPayState.HAD_PAY||dispatchState!=ServiceOrderCode.OrderDispatchState.HAD_DISPATCH){
						renderFailed("此订单当前不允许完成");
						return;
					}
				}else
				//2.2.服务后付款，派单状态为“已派单”时可确认服务完成
				if(payType==ServiceOrderCode.OrderPayType.AFTER_SERVICE){
					if(dispatchState!=ServiceOrderCode.OrderDispatchState.HAD_DISPATCH){
						renderFailed("此订单当前不允许完成");
						return;
					}
				}
				//3.签收完成订单操作，并记录日志
				order.set("orderState", ServiceOrderCode.OrderState.FINISH).set("signDate",DatetimeKit.getFormatDate("yyyy-MM-dd HH:mm:ss"));
				if(order.update()){
					service.log(order.getStr("orderNum"),"用户", "确认服务完成", "订单号："+order.getStr("orderNum")+"，确认完成！");
					renderSuccess();
					return;
				}
				//4.其他情况
				renderFailed("签收完成订单失败");
				return;
			}else{
				renderFailed("订单不存在！");
				return;
			}
		}
	}

	/**
	 * 用户催单
	 * @param ORDERNUM 订单号
	 */
	public void urge(){
		String orderNum = getPara("ORDERNUM");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
    		return;
		}else{
			ServiceOrder order = service.getOrderByOrderNum(getCurrentUser().getInt("id"),orderNum);
			if(order!=null){
				//1.判断订单状态是否为“正常”状态
				int orderState = order.getInt("orderState");
				if(orderState!=ServiceOrderCode.OrderState.NORMAL){
					renderFailed("此订单当前不允许催单");
		    		return;
				}
				//2.根据订单不同状态进行不同操作
				int payType = order.getInt("payType");
				int payState = order.getInt("payState");
				int dispatchState = order.getInt("deliverState");
				//2.1.在线支付，支付状态为“已付款”，派单状态为“未派单”时可催单
				if(payType==ServiceOrderCode.OrderPayType.ON_LINE){
					if(payState!=ServiceOrderCode.OrderPayState.HAD_PAY||dispatchState!=ServiceOrderCode.OrderDispatchState.NO_DISPATCH){
						renderFailed("此订单当前不允许催单");
						return;
					}
				}else
				//2.2.服务后付款，派单状态为“未派单”时可确认服务完成
				if(payType==ServiceOrderCode.OrderPayType.AFTER_SERVICE){
					if(dispatchState!=ServiceOrderCode.OrderDispatchState.NO_DISPATCH){
						renderFailed("此订单当前不允许催单");
						return;
					}
				}
				//3.催单操作
				int urgeTimes = order.getInt("urgeTimes");
				if(urgeTimes==0){
					//首次催单需要在下单xx分钟后
					Date orderDate = order.getDate("createDate");
					Date now = new Date();
					int diffMin = (int)(now.getTime()-orderDate.getTime())/(1000*60);
					if(diffMin<=PropKit.getInt("constants.order.urgeFirstTime")){
						renderFailed("刚下单，催啥子催！");
						return;
					}
				}else{
					//第二次以后催单，间隔需要大于xx分钟
					Date urgeLastTimeDate = order.getDate("urgeLastTime");
					Date now = new Date();
					int diffMin = (int)(now.getTime()-urgeLastTimeDate.getTime())/(1000*60);
					if(diffMin<=PropKit.getInt("constants.order.urgePerTime")){
						renderFailed("歇会儿吧，不要老催噻！");
						return;
					}
				}
				try {
					//催单，修改催单累计次数加1，更新最后一次催单记录时间
					order = service.urgeOrder(getCurrentUser().getInt("id"),orderNum);
					urgeTimes = order.getInt("urgeTimes");
					String urgeLastTime = DatetimeKit.getFormatDate(order.getDate("urgeLastTime"),"yyyy-MM-dd HH:mm:ss");
					//记录日志
					service.log(orderNum,"用户","催单","第"+order.getInt("urgeTimes")+"次催单，催单时间："+urgeLastTime+"，成功！");
					//返回客户端
					renderSuccess(ResultMap.newInstance().put("TIMES", urgeTimes).put("DATETIME", urgeLastTime));
					return;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			renderFailed("订单不存在！");
			return;
		}
	}
	
	/**
	 * 查询订单详情，及订单商品列表<br/>
	 * 注意，查询订单时，只查询订单状态“正常”、“已完成”、“已取消”的
	 * @param ORDERNUM 订单号
	 */
	public void detail(){
		String orderNum = getPara("ORDERNUM");
		if(StrKit.isBlank(orderNum)){
			renderFailed("参数ORDERNUM不能为空");
    		return;
		}else{
			ServiceOrder order = service.getOrderByOrderNum(getCurrentUser().getInt("id"),orderNum);
			if(order!=null){
				order.put("SERVICELIST", service.getServiceListByOrderNum(orderNum));
				renderSuccess(order);
				return;
			}else{
				renderFailed("此订单不存在");
				return;
			}
		}
	}
	
	/**
	 * 获取服务可预约的时间列表，供客户端展示给用户选择
	 */
	public void times(){
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		
		String[] weeks = {"周日","周一","周二","周三","周四","周五","周六"};
		int mm = Integer.valueOf(DatetimeKit.getIntervalFormatDate(30*60*1000,"mm")).intValue();//分钟
		int HH = Integer.valueOf(DatetimeKit.getIntervalFormatDate(30*60*1000,"HH")).intValue();//小时
		
		//如果加上半个小时超过整点就会在原来时间上再加1 (15:12 + 30mm = 16hh 15:31+30mm == 17hh )
		if(mm>0){
			HH++;
		}
		for(int i=0;i<7;i++){
			Map<String,Object> map = new HashMap<String,Object>();
			String day = DatetimeKit.getPNtime(i,"yyyy-MM-dd");
			if(i != 0){
				HH = 8;
				if(i==1){
					map.put("TITLE", "明日");
				}else{
					map.put("TITLE", DatetimeKit.getPNDayWeek(i,weeks));
				}
			}else{
				map.put("TITLE",  "今日");
			}
			List<Map<String,Object>> listh = new ArrayList<Map<String,Object>>();
			for(int j=HH;j<=22;j++){
				Map<String,Object> maph= new HashMap<String,Object>();
				String hour = j+"";
				if(j<10){
					 hour = "0"+j;
				}
				maph.put("TITLE", hour+":00");
				maph.put("DATETIME", day+" "+hour+":00:00");
				listh.add(maph);
			}
			map.put("DATE",  DatetimeKit.getPNtime(i,"MM月dd日"));
			map.put("HOURS", listh);
			list.add(map);
		}
		renderSuccess(list);
		return;
	}
}