package cn.com.dashihui.pay.wx.request;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.PropKit;

import cn.com.dashihui.kit.CommonKit;
import cn.com.dashihui.pay.wx.kit.Signature;

public class PreOrderReqData {
	
	//每个字段具体的意思请查看API文档
	private String appid; 		//公众号
	private String mch_id; 		//商户id
	private String nonce_str; 	//随机数字
	private String body;
	private String out_trade_no;
	private String total_fee;
	private String spbill_create_ip;
	private String notify_url;    //支付结果通知
	private String fee_type ;
	private String time_start;
	private String time_expire;
	//private String product_id;
	private String trade_type;
	private String sign; 			//签名
	
    public PreOrderReqData(String body, String out_trade_no, String total_fee,String spbill_create_ip,String notifyUrl,String time_start,String time_expire){

        //--------------------------------------------------------------------
        //设置必须数据
        //--------------------------------------------------------------------

        //微信分配的公众号ID（开通公众号之后可以获取到）
        setAppid(PropKit.get("weixinpay.appid"));

        //微信支付分配的商户号ID（开通公众号的微信支付功能之后可以获取到）
        setMch_id(PropKit.get("weixinpay.mch_id"));

        //随机字符串，不长于32 位
        setNonce_str(CommonKit.getRandomStringByLength(32));
        
        //商品或支付单简要描述
        setBody(body);
        
        //商户系统自己生成的唯一的订单号
        setOut_trade_no(out_trade_no);
        
        //设置订单开始时间
        setTime_start(time_start);
       
        //设置订单结束时间
        setTime_expire(time_expire);
        
        //订单总金额，单位为分，详见支付金额
        setTotal_fee(total_fee);
        
        //APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP
        setSpbill_create_ip(spbill_create_ip);
        
        //接收微信支付异步通知回调地址
        setNotify_url(notifyUrl);
        
        //订单总金额，单位为分，详见支付金额
        setFee_type("CNY");
        
        //取值如下：JSAPI，NATIVE，APP，详细说明见参数规定
        setTrade_type("APP");

        //根据API给的签名规则进行签名
        String sign = Signature.getSign(toMap());
        setSign(sign);//把签名数据设置到Sign这个属性中
    }
    
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getMch_id() {
		return mch_id;
	}
	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}
	public String getNonce_str() {
		return nonce_str;
	}
	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	public String getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(String total_fee) {
		this.total_fee = total_fee;
	}
	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}
	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}
	public String getNotify_url() {
		return notify_url;
	}
	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}
	public String getFee_type() {
		return fee_type;
	}
	public void setFee_type(String fee_type) {
		this.fee_type = fee_type;
	}
	public String getTrade_type() {
		return trade_type;
	}
	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}
	public String getTime_start() {
		return time_start;
	}

	public void setTime_start(String time_start) {
		this.time_start = time_start;
	}

	public String getTime_expire() {
		return time_expire;
	}

	public void setTime_expire(String time_expire) {
		this.time_expire = time_expire;
	}

	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public Map<String,Object> toMap(){
        Map<String,Object> map = new HashMap<String, Object>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            Object obj;
            try {
                obj = field.get(this);
                if(obj!=null){
                    map.put(field.getName(), obj);
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return map;
    }
}
