package com.dashihui.distribution.common.base;

import android.content.Context;
import android.os.RemoteException;

import com.dashihui.distribution.utils.preferences.UtilPreferences;
import com.dashihui.distribution.utils.string.UtilString;
import com.gprinter.aidl.GpService;

/**
 * Created by WYY on 2016/4/5.
 */
public class Contracts {
    /**
     * 版本控制
     */
//    public static final String ADDRESS = "http://192.168.1.100:8086/";//测试地址
    public static final String ADDRESS = "http://seller.91dashihui.com/";//正式地址
    public static final String CHECKVERSION = "api/checkVersion";
    public static final String NO_UPDATE = "A";//没有新版本
    public static final String NEW_UPDATE = "B";//发现新版本
    public static final String FORCE_UPDATE = "C";//强制安装新版本

    /**
     * 蓝牙连接打印机
     */
    public static final String BLUETOOTH_ADDRESS = "bluetoothAddress";//保存蓝牙MAC地址
    public static final int PRINTERID = 0;//打印机ID 第0个
    public static final int PORTTYPE = 4;// 连接类型 蓝牙
    public static final int PORTNUMBER = 0;//
    public static void connectPrinter(Context context,GpService mGpService) {
        String address = UtilPreferences.getString(context, Contracts.BLUETOOTH_ADDRESS, null);
        if(!UtilString.isEmpty(address)) {
            try {
                mGpService.openPort(Contracts.PRINTERID, Contracts.PORTTYPE, address, Contracts.PORTNUMBER);

            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
