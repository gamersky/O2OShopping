package com.codo.exampledemo;


import android.app.Application;
import android.content.Context;

import com.codo.exampledemo.dao.DaoMaster;
import com.codo.exampledemo.dao.DaoSession;

/**
 * Created by Administrator on 2015/11/3.
 */
public class MyApplication extends Application{
    private static MyApplication myApplication;
    private static DaoMaster daoMaster;
    private static DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        if (myApplication == null){
            myApplication = this;
        }
    }

    //取得DaoMaster
    public static DaoMaster getDaoMaster(Context context){
        if (daoMaster == null){
            DaoMaster.OpenHelper helper = new DaoMaster.DevOpenHelper(context, Constants.DB_NAME, null);
            daoMaster = new DaoMaster(helper.getWritableDatabase());
        }
        return daoMaster;
    }

    // 取得DaoSession
    public static DaoSession getDaoSession(Context context){
        if (daoSession == null){
            if (daoMaster == null){
                daoMaster = getDaoMaster(context);
            }
            daoSession =daoMaster.newSession();
        }
        return daoSession;
    }

    //DB常量
    public class Constants{
        public static final String DB_NAME = "note_db";
    }
}
